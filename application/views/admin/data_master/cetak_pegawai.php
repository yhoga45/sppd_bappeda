<!DOCTYPE html>
<html>
<head>
	<title>Daftar Pegawai</title>
	<style type="text/css">
	table, td, th{
		border: 1px solid black;
		border-collapse: collapse;
		font-family: sans-serif;
		font-size: 13px;
	}

	td{
		padding: 5px;
  		text-align: justify;
	}
	h1{
		text-align: center;
	}
	</style>
</head>
<body>
	
	<h1>Daftar Pegawai Badan Perencanaan Pembangunan Daerah</h1><br>
	<table width="100%">
		<thead>
			<tr>
				<th>No</th>
				<th>NIP</th>
				<th>Nama</th>
				<th>Golongan</th>
				<th>Jabatan</th>
				<th>Keterangan Jabatan</th>
			</tr>
		</thead>
		<tbody>
			<?php  
              $no = 1;
              foreach ($data as $tampil):
            ?>
            <tr>
            	<td><?php echo $no++ ?></td>
                <td><?php echo ($tampil->nip)?></td>
                <td><?php echo ($tampil->nama_pegawai)?></td>
                <td><?php echo ($tampil->golongan)?></td>
                <td><?php echo ($tampil->jabatan)?></td>
                <td><?php echo ($tampil->ket_jabatan)?></td>
            </tr>
            <?php endforeach; ?>
		</tbody>
	</table>
</body>
</html>
