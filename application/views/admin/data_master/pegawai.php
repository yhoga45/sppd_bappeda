  <style>
    .tooltip-edit {
      position: relative;
      display: inline-block;
      word-break: normal;
      word-spacing: normal;
      white-space: normal;
      line-break: auto;
      font-size: .875rem;
      word-wrap: break-word;

    }

    .tooltip-edit-text {
      visibility: hidden;
      width: 200px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
      right: 40px;
      top: -20px;
    }

    .tooltip-edit:hover .tooltip-edit-text {
      visibility: visible;
    }

    .tooltip-delete {
      position: relative;
      display: inline-block;
      word-break: normal;
      word-spacing: normal;
      white-space: normal;
      line-break: auto;
      font-size: .875rem;
      word-wrap: break-word;
    }

    .tooltip-delete-text {
      visibility: hidden;
      width: 200px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
      right: 75px;
      top: -20px;
    }

    .tooltip-delete:hover .tooltip-delete-text {
      visibility: visible;
    }
  </style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pegawai</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Data Master</a></li>
            <li class="breadcrumb-item active">Pegawai</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data Master Pegawai</h3><br><br>

            <div class="row">
              <div class="col-lg-6">
                <?php 
                if(isset($_SESSION['flash'])){
                  Flasher::flash();
                } ?>
              </div>  
            </div>

            <button type="button" class="btn btn-success tombolTambahDataPegawai" title="Tambah" data-toggle="modal" data-target="#formModal">
              <i class="fas fa-plus"></i> Tambah Data Pegawai
            </button>
            <a class="btn btn-default" href="<?= base_url('/pegawai/cetak')?>" target="_blank" role="button" title="Cetak Daftar Pegawai (PDF)">
              <i class="fas fa-print"> Cetak</i>
            </a>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Golongan</th>
                  <th>Jabatan</th>
                  <th>Keterangan Jabatan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                $no = 1;
                foreach ($data as $tampil):
                  ?>
                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo ($tampil->nip)?></td>
                    <td><?php echo ($tampil->nama_pegawai)?></td>
                    <td><?php echo ($tampil->golongan)?></td>
                    <td><?php echo ($tampil->jabatan)?></td>
                    <td><?php echo ($tampil->ket_jabatan)?></td>
                    <td class="text-center">
                      <a class="btn btn-warning tampilModalUbahPegawai" href="#" role="button" title="Edit" data-toggle="modal" data-target="#formModal" data-id="<?php echo ($tampil->id_pegawai)?>">
                        <i class="fas fa-edit"></i>
                      </a>
                      <?php 
                      $jml_sama = 0;
                      foreach ($cek_pegawai_pengikut as $cpp) {
                        if ($tampil->id_pegawai == $cpp->id_pegawai) {
                          $jml_sama = $jml_sama + 1;
                        }
                      } 
                      foreach ($cek_ttd as $ctd) {
                        if ($tampil->id_pegawai == $ctd->id_pegawai) {
                          $jml_sama = $jml_sama + 1;
                        }
                      }
                      foreach ($cek_pegawai_stl as $cpstl) {
                        if ($tampil->id_pegawai == $cpstl->id_pegawai) {
                          $jml_sama = $jml_sama + 1;
                        }
                      } 
                      foreach ($cek_pegawai_sppd as $cpspd) {
                        if ($tampil->id_pegawai == $cpspd->id_pegawai) {
                          $jml_sama = $jml_sama + 1;
                        }
                      } 
                      if ($jml_sama > 0) { ?>
                        <button class="btn btn-danger tooltip-delete" disabled=""><i class="fas fa-trash"></i>
                          <span class="tooltip-delete-text">Tidak bisa menghapus data pegawai karna sudah terpakai </span>
                        </button>
                      <?php } else { ?>
                        <a class="btn btn-danger hapus" href="<?= base_url('pegawai/hapusPegawai/'); echo encrypt_url($tampil->id_pegawai); ?>" role="button" title="Hapus" >
                          <i class="fas fa-trash"></i>
                        </a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Golongan</th>
                  <th>Jabatan</th>
                  <th>Keterangan Jabatan</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>


  <!-- Modal -->
  <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="judulModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="judulModal">Tambah Data Pegawai</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" method="post">

            <input type="hidden" name="id" id="id">

            <div class="form-group">
              <label for="nip">NIP:</label>
              <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP">
            </div>

            <div class="form-group">
              <label for="nama">Nama:</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
            </div>

            <div class="form-group mb-3">
              <label for="golongan">Golongan:</label>
              <select class="form-control select2 mb-3" name="golongan" id="golongan">
                <!-- <option value="null" disabled hidden selected>Pilih Golongan</option> -->
                <?php 
                foreach ($golongan as $golongan):?>
                  <option value="<?php echo $golongan->id_golongan ?>"><?php echo $golongan->golongan ?></option>
                <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group mb-3">
              <label for="jabatan">Jabatan:</label>
              <select class="form-control select2 mb-3" name="jabatan" id="jabatan">
                <!-- <option value="null" disabled hidden selected>Pilih Status</option> -->
                <?php 
                foreach ($jabatan as $jabatan):?>
                  <option value="<?php echo $jabatan->id_jabatan ?>"><?php echo $jabatan->jabatan ?></option>
                <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for="nama">Keterangan Jabatan:</label>
              <textarea class="form-control" id="keterangan_jabatan" rows="3" name="keterangan_jabatan" placeholder="Masukkan Keterangan Jabatan..."></textarea>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Tambah Data</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal modal-danger fade" id="formModalHapus" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title text-center" id="myModal">Hapus Data Pegawai</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <form action="" method="post">
              <p class="text-center">
                Apakah Anda Yakin ?
              </p>
              <input type="hidden" name="idHapus" id="idHapus">
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
              <input type="submit" class="btn btn-danger" name="submit" value="Yes">
            </form>
          </div>
        </div>
      </div>
    </div>