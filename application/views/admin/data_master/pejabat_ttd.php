
<style>
  .tooltip-edit {
    position: relative;
    display: inline-block;
    word-break: normal;
    word-spacing: normal;
    white-space: normal;
    line-break: auto;
    font-size: .875rem;
    word-wrap: break-word;

  }

  .tooltip-edit-text {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    right: 40px;
    top: -20px;
  }

  .tooltip-edit:hover .tooltip-edit-text {
    visibility: visible;
  }

  .tooltip-delete {
    position: relative;
    display: inline-block;
    word-break: normal;
    word-spacing: normal;
    white-space: normal;
    line-break: auto;
    font-size: .875rem;
    word-wrap: break-word;
  }

  .tooltip-delete-text {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    right: 75px;
    top: -20px;
  }

  .tooltip-delete:hover .tooltip-delete-text {
    visibility: visible;
  }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pejabat TTD</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Data Master</a></li>
            <li class="breadcrumb-item active">Pejabat TTD</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data Master Pejabat TTD</h3><br><br>

            <div class="row">
              <div class="col-lg-6">
                <?php 
                if(isset($_SESSION['flash'])){
                  Flasher::flash();
                } ?>
              </div>  
            </div>

            <button type="button" class="btn btn-success tombolTambahDataPejabatTTD" title="Tambah" data-toggle="modal" data-target="#formModal">
              <i class="fas fa-plus"></i> Tambah Data Pejabat TTD
            </button>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Golongan</th>
                  <th>Jabatan</th>
                  <th>Keterangan Jabatan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                $no = 1;
                foreach ($data as $tampil):
                  ?>
                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo ($tampil->nip)?></td>
                    <td><?php echo ($tampil->nama_pegawai)?></td>
                    <td><?php echo ($tampil->golongan)?></td>
                    <td><?php echo ($tampil->jabatan)?></td>
                    <td><?php echo ($tampil->ket_jabatan)?></td>
                    <td class="text-center">
                    <!-- <a class="btn btn-warning tampilModalUbahPejabatTTDSPT" href="#" role="button" title="Edit" data-toggle="modal" data-target="#formModal" data-id="id">
                      <i class="fas fa-edit"></i>
                    </a> -->

                    <?php
                    $jml_sama = 0;
                    foreach ($cek_ttd_stl as $cpstl) {
                      if ($tampil->id == $cpstl->id_pejabat_spt) {
                        $jml_sama = $jml_sama + 1;
                      }
                    } 
                    foreach ($cek_ttd_spt as $cpspt) {
                      if ($tampil->id == $cpspt->id_pejabat_ttd_spt) {
                        $jml_sama = $jml_sama + 1;
                      }
                    }
                    foreach ($cek_ttd_spd as $cpspd) {
                      if ($tampil->id == $cpspd->id_pejabat_ttd_sppd) {
                        $jml_sama = $jml_sama + 1;
                      }
                    }
                    foreach ($cek_ttd_disposisi as $cpsdis) {
                      if ($tampil->id == $cpsdis->id_pejabat_ttd_disposisi) {
                        $jml_sama = $jml_sama + 1;
                      }
                    }

                    if ($jml_sama > 0) { ?>
                      <button class="btn btn-danger tooltip-delete" disabled=""><i class="fas fa-trash"></i>
                        <span class="tooltip-delete-text">Tidak bisa menghapus data pejabat ttd karena sudah terpakai </span>
                      </button>
                    <?php } else { ?>
                      <a class="btn btn-danger hapus" href="<?= base_url('pejabatttd/hapusPejabat/'); echo encrypt_url($tampil->id); ?>" role="button" title="Hapus">
                        <i class="fas fa-trash"></i>
                      </a>
                    <?php } ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Golongan</th>
                <th>Jabatan</th>
                <th>Keterangan Jabatan</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>


<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModal">Tambah Data Pejabat TTD</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post">

          <input type="hidden" name="id" id="id">

          <div class="form-group mb-3">
            <label for="nip">Masukkan NIP:</label>
            <select class="form-control select2 mb-3" name="nip" id="nip">
              <!-- <option value="null" disabled hidden selected>Pilih Golongan</option> -->
              <?php 
              foreach ($pegawai as $pegawai):
                $id_pegawai = $pegawai->id_pegawai;
                $save = true;
                foreach ($data as $tampil):
                  $id_pejabat = $tampil->id_pegawai;   
                  if($id_pegawai == $id_pejabat){
                    $save = false;
                  }
                endforeach;
                if($save == true){ ?>
                  <option value="<?php echo $pegawai->id_pegawai ?>"><?php echo $pegawai->nip ?>/<?php echo $pegawai->nama_pegawai ?></option>
                  <?php
                }
              endforeach; 
              ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal modal-danger fade" id="formModalHapus" role="dialog" aria-labelledby="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h5 class="modal-title text-center" id="myModal">Hapus Data Pejabat TTD</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <form action="" method="post">
            <p class="text-center">
              Apakah Anda Yakin ?
            </p>
            <input type="hidden" name="idHapus" id="idHapus">
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
            <input type="submit" class="btn btn-danger" name="submit" value="Yes">
          </form>
        </div>
      </div>
    </div>
  </div>