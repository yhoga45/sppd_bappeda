<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">SPD</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= base_url('/spt/')?>">SPT</a></li>
            <li class="breadcrumb-item active">SPD</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data SPPD</h3><br><br>

            <div class="row">
              <div class="col-lg-6">
                <?php 
                if(isset($_SESSION['flash'])){
                  Flasher::flash();
                } ?>
              </div>  
            </div>
            <?php if(function_exists('check_if_role_is')){
            if (check_if_role_is('1') || check_if_role_is('2')){ 
              $cek = 0;
              foreach($spd as $sp){
                if($sp->id_jabatan == 1){
                  $cek=$cek+1;
                 }  
              }
              if($cek > 0){ ?>
                <button class="btn btn-success tooltip-delete" disabled="">
                  <i class="fas fa-plus"></i> Tambah Data SPPD
                </button>
              <?php } else { ?>
                <a href="<?= base_url('/spd/tambah_data_spd/'); echo $this->uri->segment(3); ?>" class="btn btn-success" title="Tambah" >
                    <i class="fas fa-plus"></i> Tambah Data SPPD
                </a>

              <?php }    
             } } ?>
            
                  
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>NIP</th>
                  <th>Tujuan</th>
                  <th>No SPPD</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 0;
                foreach ($spd as $s) {

                  ?>
                  <tr>
                    <td><?= $i+1; ?></td>
                    <td><?php echo $s->nama_pegawai; ?></td>
                    <td><?php echo $s->nip; ?></td>
                    <td><?php echo $s->maksut_pd; ?></td>
                    <td class="text-center"><?php echo $s->no_sppd; ?></td>
                    <td class="text-center">
                    <?php if(function_exists('check_if_role_is')){
                    if (check_if_role_is('1') || check_if_role_is('2')){ 
                    if($cek > 0){ ?>
                      <button class="btn btn-warning tooltip-delete" disabled="">
                        <i class="fas fa-edit"></i>
                      </button>
                    <?php } else { ?>
                      <a class="btn btn-warning tampilModalUbahSppd" href="<?= base_url('spd/ubah_data_spd/'); echo $this->uri->segment(3).'/'.encrypt_url($s->id_sppd); ?>" role="button" title="Edit">
                        <i class="fas fa-edit"></i>
                      </a>
                    <?php }   ?>               
                      <a href="<?php echo base_url('spd/hapussppd/'); echo $this->uri->segment(3).'/'.encrypt_url($s->id_sppd); ?>" class="btn btn-danger hapus">
                        <i class="fa fa-trash"></i>
                      </a>
                    <?php } } ?>
                      <a class="btn btn-primary" href="<?= base_url('/spd/cetak_hal1/'); echo $this->uri->segment(3).'/'.$s->id_sppd; ?>" target="_blank" role="button" title="Cetak Halaman 1">
                        <i class="fas fa-print"> Hal 1</i>
                      </a>
                      <a class="btn btn-primary" href="<?= base_url('/spd/cetak_hal2/'); echo $this->uri->segment(3).'/'.$s->id_sppd; ?>" target="_blank" role="button" title="Cetak Halaman 2">
                        <i class="fas fa-print"> Hal 2</i>
                      </a>
                    </td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Tujuan</th>
                    <th>No SPPD</th>
                    <th>Aksi</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" id="formModal" tabindex="-1" role="dialog" aria-labelledby="judulModal" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="judulModal">Tambah Data SPPD</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="" method="post">
            <div class="modal-body">
              <?php
              foreach ($spt as $st) {

                ?>
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="id_spt" id="id_spt" value="<?php echo $this->uri->segment(4); ?>">

                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="no_spt">No SPPD:</label>
                    <div class="form-row">
                      <div class="form-group col-md-3">
                        <input type="text" class="form-control" id="no_spd" name="no_spd" value="094/" disabled>
                      </div>

                      <div class="form-group col-md-4">
                        <input type="number" class="form-control" id="no_urut_spd" name="no_urut_spd" title="Nomor SPD" value="" placeholder="No SPD">
                      </div>

                      <div class="form-group col-md-5">
                        <input type="text" class="form-control text-center" id="kode_spd" name="kode_spd" value="/SPD/BAPPEDA/" disabled>
                      </div>

                      <div class="form-group col-md-5">
                        <div class="form-group mb-3">
                          <input type="text" class="form-control text-center" id="bulan_romawi" name="bulan_romawi" value="<?php echo explode("/", $st->no_spt )[4]; ?>" disabled>
                        </div>
                      </div>

                      <div class="form-group col-md-3">
                        <input type="text" class="form-control text-center" id="tahun_spt" name="bulan_spt" value="<?php echo '/'.explode("/", $st->no_spt)[5]; ?>" disabled>
                      </div>
                    </div>

                    <div class="form-group mb-3">
                      <label>Tanggal dikeluarkan:</label>
                      <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_spt" id="tanggal_spt" value="<?php echo $st->tgl_spt; ?>" disabled/>
                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                      </div>
                    </div>

                    <label>Tanggal Berangkat dan Kembali:</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control float-right reservation" name="tgl_berangkat_dan_kembali" id="tgl_berangkat_dan_kembali" disabled>
                    </div>

                    <label for="durasi">Durasi(Hari):</label>
                    <input type="number" class="form-control mb-3" id="durasi" name="durasi" placeholder="Durasi" value="<?php echo $st->durasi; ?>" disabled>

                    <div class="form-group mb-3">
                      <label for="asal">Berangkat Dari:</label>
                      <?php
                      foreach ($kota_brgkt as $kb) {
                        ?>
                        <input type="text" class="form-control" id="kota_brgkt" name="kota_brgkt" value="<?php echo $kb->nama_kota; ?>" disabled>
                      <?php } ?>
                    </div>

                  </div>

                  <div class="form-group col-md-4">
                    <div class="form-group mb-3">
                      <label for="tujuan">Kota Tujuan:</label>
                      <?php
                      foreach ($kota_tujuan as $kt) {
                        ?>
                        <input type="text" class="form-control" id="kota_brgkt" name="kota_brgkt" value="<?php echo $kt->nama_kota; ?>" disabled>
                      <?php } ?>
                    </div>

                    <div class="form-group mb-3">
                      <label for="alat_angkut">Alat Angkut:</label>
                      <?php
                      foreach ($alat_angkut as $aa) {
                        ?>
                        <input type="text" class="form-control" id="kota_brgkt" name="kota_brgkt" value="<?php echo $aa->alat_angkut; ?>" disabled>
                      <?php } ?>
                    </div>

                    <div class="form-group">
                      <label for="mpd">Maksut Perjalanan Dinas:</label>
                      <textarea class="form-control" id="mpd" rows="4" name="mpd" disabled><?php echo $st->maksut_pd; ?></textarea>
                    </div>

                    <div class="form-group mb-3">
                      <label for="nama_pj_pd">Pegawai yang ditugaskan:</label>
                      <select class="form-control select2" name="id_pegawai_pd" id="id_pegawai_pd">
                        <option value="null" disabled hidden selected>Pilih pegawai</option>
                        <?php
                        foreach ($pegawai_pd as $pd) {
                          ?>
                          <option value="<?php echo $pd->id_pegawai; ?>"><?php echo $pd->nama_pegawai; ?></option>

                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Pegawai Pengikut:</label>
                      <div class="select2-blue">
                        <select class="select2" multiple="multiple" data-placeholder="Cari Pegawai Pengikut..." data-dropdown-css-class="select2-blue" id="id_pengikut_pd" name="id_pengikut_pd[]">
                          <?php
                          foreach ($pegawai_pd as $pd) {
                            ?>
                            <option value="<?php echo $pd->id_pegawai; ?>"><?php echo $pd->nama_pegawai; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-md-4">
                    <div class="form-group mb-3">
                      <label for="pa">Pembebanan Anggaran:</label>
                      <?php
                      foreach ($sumber_anggaran as $sa) {
                        ?>
                        <input type="text" class="form-control" id="sumber_anggaran" name="sumber_anggaran" value="<?php echo $sa->sumber_anggaran; ?>" disabled>
                      <?php } ?>
                    </div>

                    <div class="form-group mb-3">
                      <label for="pejabat_spt">Pejabat TTD SPT:</label>
                      <?php
                      foreach ($ttd_spt as $tspt) {
                        ?>
                        <input type="text" class="form-control" id="ttd_spt" name="ttd_spt" value="<?php echo $tspt->nama_pegawai; ?>" disabled>
                      <?php } ?>
                    </div>

                    <div class="form-group mb-3">
                      <label for="pejabat_sppd">Pejabat TTD SPPD:</label>
                      <?php
                      foreach ($ttd_sppd as $tspp) {
                        ?>
                        <input type="text" class="form-control" id="ttd_sppd" name="ttd_sppd" value="<?php echo $tspp->nama_pegawai; ?>" disabled>
                      <?php } ?>
                    </div>

                    <div class="form-group mb-3">
                      <label for="pejabat_ppk">Pejabat Pembuat T? Komitmen (TTD):</label>
                      <?php
                      foreach ($ttd_pptk as $tppt) {
                        ?>
                        <input type="text" class="form-control" id="ttd_pptk" name="ttd_pptk" value="<?php echo $tppt->nama_pegawai; ?>" disabled>
                      <?php } ?>
                    </div>
                  </div>
                </div><!-- -->
              <?php } ?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- The Modal -->
    <div class="modal modal-danger fade" id="formModalDelete" role="dialog" aria-labelledby="myModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h5 class="modal-title text-center" id="myModal">Hapus Data SPPD</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x">&times;</span></button>
          </div>
          <form action="<?= base_url('/admin/spt/')?>hapusSpt/id" method="post">
            <!-- Modal body -->
            <div class="modal-body">
              <p class="text-center">
                Apakah Anda Yakin ?
              </p>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
              <input type="submit" class="btn btn-danger" name="submit" value="Yes">
            </div>
          </form>
        </div>
      </div>
    </div>