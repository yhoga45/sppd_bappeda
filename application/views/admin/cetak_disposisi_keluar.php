<!DOCTYPE html>
<html>
<head>
	<title>Disposisi</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/')?>disposisi.css">
</head>
<body>
	<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papau Barat">
	<div id="kop">
		<h2>PEMERINTAH PROVINSI PAPUA BARAT<div id="kop1"><i>BADAN PERENCANAAN PEMBANGUNAN DAERAH</i></div></h2>
	</div>

	<p id="alamat_kop">Jln. Brigjen Marinir (Purn) Abraham. O Atururi Perkantoran Gubernur Papua Barat – Manokwari Kode Pos 98315</p>
	<hr id="hr1">
	<hr id="hr2">
	<hr id="hr3">
	<hr id="hr4">

	<p id="bawah_kop">Lembar Disposisi</p>
	<table style="width: 100%;">
		<tbody>
			<tr>
				<td width="50%">
					<p>No.Surat/Rdg/Disposisi<span style="visibility: hidden;">ri</span>&nbsp;&nbsp;: 
						<?php 
						// var_dump($disposisi);
						// exit(0);
						if (isset($disposisi)) { 
							echo $disposisi->no_surat; 
						} ?>
					</p>
					<p>Surat/Rdg/Disposisi Untuk &nbsp;: 
						<?php 
						if (isset($disposisi)) { 
							echo $disposisi->tujuan;
						} ?>
					</p>
					<p>Tanggal<span style="visibility: hidden;">dg/Disposisi Dari </span>: 
						<?php 
						if (isset($disposisi)) { 
							echo $this->tgl_indo->tgl_indo($disposisi->tgl_surat); 
						} ?>
					</p>
				</td>
				<td width="50%">
					<p>Diterima Tanggal &nbsp;: 
						<?php 
						if (isset($disposisi)) { 
							echo $this->tgl_indo->tgl_indo($disposisi->tgl_diterima); 
						}?>
					</p>
					<p>No.Agenda<span style="visibility: hidden;">nggal</span> &nbsp;&nbsp;: 
						<?php 
						if (isset($disposisi)) { 
							echo $disposisi->no_surat; 
						}?>
					</p>
					<span>Sifat<span style="visibility: hidden;">rima Tanggal</span> :
					<?php if (isset($disposisi)) { 
						if ($disposisi->sifat == "Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="" checked="checked">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Rahasia
							</label>
						<?php } elseif ($disposisi->sifat == "Amat Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="" checked="checked">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Rahasia
							</label>
						<?php } elseif ($disposisi->sifat == "Rahasia") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="" checked="">Rahasia
							</label>
						<?php } else{ ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Rahasia
							</label>
						<?php }
					} ?>

				</span>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height:50px;">
				<p>Perihal<span style="visibility: hidden;"> Yth</span> &nbsp;: 
					<?php 
					if (isset($disposisi)) { 
						echo $disposisi->perihal;
					} ?>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height:50px;">
				<p>Kepada Yth : 
					<?php 
					if (isset($disposisi)) { 
						echo $disposisi->kepada;
					}?>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<center>
					<span>Disposisi : </span>
					<?php 
					if (isset($disposisi)) { 
						if ($disposisi->disposisi == "Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="" checked="checked">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
						<?php } elseif ($disposisi->disposisi == "Amat Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="" checked="checked">Amat Segera
							</label>
						<?php } else{ ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<?php 
						}
					} ?>						
				</center>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div>
					<?php $i = 1; foreach ($tujuan as $j => $t) : ?>
					<?php if ($i == 1): ?>
						<center>
						<?php endif; ?>
						<label class="checkbox-inline">
							<input type="checkbox" value="" <?php if (in_array($t->id, $tujuan_disposisi_dipilih_ids)) echo "checked='checked'"; ?>><?php echo $t->tujuan; ?>
						</label>
						<?php if ($i == 4 or $j == (count($tujuan)-1)): ?>
						</center>
						<?php $i = 0; endif; ?>
						<?php $i++; endforeach; ?>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="kotak-1">
		<div class="kiri-2"><b>Uraian :</b></div>
		<div style="clear: both;"></div>
		<p class="kiri-4" style="padding-bottom: 0px; margin-bottom: 0px;">
			<?php 
			if (isset($disposisi)) { 
				echo $disposisi->uraian;
			}?>

		</p>

		<div class="kanan-3" style="padding-top: 100px; margin-top: 0px;">Manokwari, 
			<?php 
			if (isset($disposisi)) {  
				echo $this->tgl_indo->tgl_indo(date('m/d/Y'));
			} ?>

		</div>
		<div class="kanan-3">
			<?php if (isset($disposisi)) {  
				echo $pejabat_disposisi->jabatan. " " . $pejabat_disposisi->ket_jabatan; 
			}?> SETDA PROVINSI PAPUA BARAT</div>

		<div style="clear:both;"></div><br><br><br>

		<div class="kanan-4"><b><u><?php if (isset($disposisi)) { echo $pejabat_disposisi->nama_pegawai; } ?></u></b></div>
		<div class="kanan-4"><b><?php if (isset($disposisi)) { echo $pejabat_disposisi->pangkat; } ?></b></div>
		<div class="kanan-4"><b><?php if (isset($disposisi)) { echo $pejabat_disposisi->nip; } ?></b></div>

		<div style="clear:both;"></div>
	</div>
</body>
</html>
