<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">SPT</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item active">SPT</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data SPT</h3><br><br>

                <div class="row">
                  <div class="col-lg-6">
<!--                     <?php 
                    if(isset($_SESSION['flash'])){
                      Flasher::flash();
                    } ?> -->
                  </div>  
                </div>
                <?php if(function_exists('check_if_role_is')){
                if (check_if_role_is('1') || check_if_role_is('2')){ ?>
                <a href="<?= base_url('/spt/tambah_spt/'); ?>" class="btn btn-success" title="Tambah">
                  <i class="fas fa-plus"></i> Tambah Data SPT
                </a>
                <?php } } ?>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No SPT</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                    foreach ($spt as $s) {

                      ?>
                      <tr>
                        <td><?= $i++; ?></td>
                        <td>
                          <?php if($this->cek_detail_spt->cek_detail_spt($s->id_spt) == true) {?>
                            <a href="<?= base_url('/spd/detail_spd/'); echo encrypt_url($s->id_spt); ?>"><?php echo $s->no_spt; ?></a>
                          <?php } else { 
                            echo $s->no_spt;
                          } ?>
                        </td>
                        <td class="text-center">
                          <?php if($this->cek_cetak_spt->cek_cetak_spt($s->id_spt) == true) {?>
                            <a class="btn btn-primary" href="<?= base_url('spt/cetak_spt/'); echo encrypt_url($s->id_spt);?>" target="_blank" role="button" title="Cetak spt">
                              <i class="fas fa-print"> SPT</i>
                            </a>
                          <?php } else { ?>
                            <button class="btn btn-primary" disabled><i class="fas fa-print"> SPT</i></button>
                          <?php } ?>
                          <?php if(function_exists('check_if_role_is')){
                          if (check_if_role_is('1') || check_if_role_is('2')){ ?>
                          <a class="btn btn-warning tampilModalUbahSpt" href="<?= base_url('/spt/ubah_spt/'); echo encrypt_url($s->id_spt);?>" role="button" title="Edit">
                            <i class="fas fa-edit"></i>
                          </a>
                          <?php 
                           } } ?>
                          <!-- <a href="<?php echo base_url('spt/hapusspt/'); echo encrypt_url($s->id_spt); ?>" class="btn btn-danger hapus"><i class="fa fa-trash"></i></a> -->
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Kota</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>


      <!-- The Modal -->
      <div class="modal modal-danger fade" id="formModalDelete" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h5 class="modal-title text-center" id="myModal">Hapus Data SPT & SPD</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
              </div>
              <form action="<?= base_url('/admin/spt/')?>hapusSptSpd/id" method="post">
                <!-- Modal body -->
                <div class="modal-body">
                  <p class="text-center">
                    Apakah Anda Yakin ?
                  </p>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                  <input type="submit" class="btn btn-danger" name="submit" value="Yes">
                </form>
              </div>
            </div>
          </div>
        </div>