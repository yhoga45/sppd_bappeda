<!DOCTYPE html>
<html>
<head>
	<title>SPT</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/')?>spt.css">
</head>
<body>
<?php $kaban = false;
	foreach($kepada as $kp){
		if($kp->id_jabatan == 1) { 
			$kaban = true;
		}
	}
	if($kaban == true) { ?>
		<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papua Barat">
		<div id="kop">
			<b style="font-family:Arial; font-size:16px;">PEMERINTAH PROVINSI PAPUA BARAT <div id="kop1"> SEKRETARIAT DAERAH 
				<p id="alamat_kop">Jalan Brigdjen Marinir (Purn) Abraham O. Aturui Arfai – Manokwari Provinsi Papua Barat <br>
				</p></div></b>
		</div>
		<div style="clear:both;"></div>
		<hr id="hr1">
		<hr id="hr2">
		<hr id="hr3">
		<hr id="hr4">

		<p id="bawah_kop">SURAT PERINTAH TUGAS</p>
		<hr style="border:2px; border-style: dashed; width:40%;">

		<p id="bawah_kop2">Nomor : <?php echo $spt->no_spt ?></p>
		<div style="height: 450px;">

		<div class="kiri">Dasar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan">1. DPA BAPPEDA Provinsi Papua Barat Nomor: 4.03.4.03.01.01.18 tentang Rapat-rapat Koordinasi dan Konsultasi Ke luar Daerah.</div>
		<!-- <div class="kanan">2. DPA SKPD Badan Perencanaan Pembangunan Daerah Papua Barat No.4.01.4.01.04.14.01</div> -->

		<div style="clear:both; margin-top: 10px;"></div>
		
		<!-- <div class="kiri">Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan"><?php echo $this->tgl_indo->tgl_indo($spt->tgl_spt)?></div> -->


		<div style="clear:both;"></div>

		<p id="bawah_kop3">MEMERINTAHKAN</p><br>

		<div class="kiri">Kepada &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<?php 
			$no = 1;
			foreach ($kepada as $kepada): ?>
		<div class="kanan"><?php echo "<b>" . $kepada->nama_pegawai . " /</b> NIP. " . $kepada->nip . " / " . $kepada->jabatan . " " . $kepada->ket_jabatan. " Provinsi Papua Barat"?>.</div>
		<?php endforeach; ?>

		<div style="clear:both;"></div>
		

		<div class="kiri">Supaya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan">1. Seterima SURAT PERINTAH TUGAS ini segera menyiapkan diri untuk berangkat dari <b><?php echo $kota_brgkt->nama_kota. " - ". $kota_tujuan->nama_kota. " (PP) "?></b> Dalam Rangka <?php echo $spt->maksut_pd ?>.</div>
		<div class="kanan">2. Berangkat pada kesempatan pertama dengan <?php echo $spt->alat_angkut ?> biaya Negara (Mata Anggaran BAPPEDA Provinsi Papua Barat T.A <?php echo date('Y'); ?>).</div>
		<div class="kanan">3. Lama Perjalanan Dinas <?php 
								echo $spt->durasi; 
								if ($spt->durasi == 1) {
									echo ' (Satu)';
								} elseif ($spt->durasi == 2) {
									echo ' (Dua)';
								} elseif ($spt->durasi == 3) {
									echo ' (Tiga)';
								} elseif ($spt->durasi == 4) {
									echo ' (Empat)';
								} elseif ($spt->durasi == 5) {
									echo ' (Lima)';
								} elseif ($spt->durasi == 6) {
									echo ' (Enam)';
								} elseif ($spt->durasi == 7) {
									echo ' (Tujuh)';
								} elseif ($spt->durasi == 8) {
									echo ' (Delapan)';
								} elseif ($spt->durasi == 9) {
									echo ' (Semibilan)';
								} elseif ($spt->durasi == 10) {
									echo ' (Sepuluh)';
								}
								?>  hari.</div>

		<div style="clear:both;"></div><br>

		<div class="kiri">Selesai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan">1. Melaksanakan Urusan Dinas segera melaporkan hasilnya kepada Kepala BAPPEDA Provinsi Papua Barat.</div>
		<div class="kanan">2. Agar diindahkan dan dilaksanakan dengan penuh rasa tanggung jawab.</div>

		<div style="clear:both;"></div><br>
		</div>

		<div class="kiri-2">Dikeluarkan di&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2">Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"><?php echo $this->tgl_indo->tgl_indo($spt->tgl_spt)?></div>

		<div style="clear:both;"></div>

		<div class="kanan-3">a.n. GUBERNUR PAPUA BARAT<br><b>SEKRETARIS DAERAH</b></div>

		<div style="clear:both;"></div><br><br><br>

		<div class="kanan-4"><b>Drs. NATANIEL D. MANDACAN, M.Si</b></div>
		<div class="kanan-4">PEMBINA UTAMA</div>
		<div class="kanan-4">NIP. 19621111 198903 1 029</div>

		<div style="clear:both;"></div><br>

		<div class="kiri-3"><b><u>Tembusan Disampaikan kepada Yth:</u></b></div><br>
		<div class="kiri-3">1. Bendahara Pengeluaran BAPPEDA Provinsi Papua Barat.</div>
		<div class="kiri-3">2. Yang Bersangkutan untuk dilaksanakan.</div>
		<div class="kiri-3">3. Arsip.</div>

		<div style="clear:both;"></div>
		
	</div>
	<?php } else { ?>
		<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papua Barat">
		<div id="kop">
			<b style="font-family:Arial; font-size:16px;">PEMERINTAH PROVINSI PAPUA BARAT <div id="kop1"> BADAN PERENCANAAN PEMBANGUNAN DAERAH 
				<p id="alamat_kop">Jalan Brigdjen Marinir (Purn) Abraham O. Aturui Arfai – Manokwari Provinsi Papua Barat <br>
				Tlp/Hp. 081248790969 email: bappedapapuabarat@yahoo.co.id website: bappeda.papuabaratprov.go.id 
				</p></div></b>
		</div>
		<div style="clear:both;"></div>
		<hr id="hr1">
		<hr id="hr2">
		<hr id="hr3">
		<hr id="hr4">

		<p id="bawah_kop">SURAT PERINTAH TUGAS</p>
		<hr style="border:2px; border-style: dashed; width:40%;">

		<p id="bawah_kop2">Nomor : <?php echo $spt->no_spt ?></p>
		<div style="height: 450px;">

		<div class="kiri">Dasar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan">1. DPA BAPPEDA Provinsi Papua Barat Nomor: 4.03.4.03.01.01.18 tentang Rapat-rapat Koordinasi dan Konsultasi Ke luar Daerah.</div>
		<!-- <div class="kanan">2. DPA SKPD Badan Perencanaan Pembangunan Daerah Papua Barat No.4.01.4.01.04.14.01</div> -->

		<div style="clear:both; margin-top: 10px;"></div>
		
		<!-- <div class="kiri">Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan"><?php echo $this->tgl_indo->tgl_indo($spt->tgl_spt)?></div> -->


		<div style="clear:both;"></div>

		<p id="bawah_kop3">MEMERINTAHKAN</p><br>

		<div class="kiri">Kepada &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<?php 
			$no = 1;
			foreach ($kepada as $kepada): ?>
		<div class="kanan"><?php echo $no++ .". <b>" . $kepada->nama_pegawai . " (" . $kepada->nip . ")</b> " . $kepada->jabatan . " " . $kepada->ket_jabatan?>.</div>
		<?php endforeach; ?>

		<div style="clear:both;"></div>
		<div class="kiri">Pengikut &nbsp;&nbsp;&nbsp;&nbsp;:</div>

		<?php 
			$no = 1;
			foreach ($pengikut as $pengikut): ?>
		<div class="kanan"><?php echo $no++ .". <b>" . $pengikut->nama_pegawai . " (" . $pengikut->nip . ")</b> " . $pengikut->jabatan . " " . $pengikut->ket_jabatan?>.</div>
		<?php endforeach; ?>
		<div style="clear:both;"></div>

		<div class="kiri">Supaya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan">1. Seterima SURAT PERINTAH TUGAS ini segera menyiapkan diri untuk berangkat dari <b><?php echo $kota_brgkt->nama_kota. " - ". $kota_tujuan->nama_kota. " (PP) "?></b> Dalam Rangka <?php echo $spt->maksut_pd ?>.</div>
		<div class="kanan">2. Berangkat pada kesempatan pertama Tgl <?php echo $this->tgl_indo->tgl_indo($spt->tgl_berangkat)." - ".$this->tgl_indo->tgl_indo($spt->tgl_kembali)?>  dengan <?php echo $spt->alat_angkut ?> biaya Negara (Mata Anggaran BAPPEDA Provinsi Papua Barat T.A <?php echo date('Y'); ?>).</div>
		<div class="kanan">3. Lama Perjalanan Dinas <?php 
								echo $spt->durasi; 
								if ($spt->durasi == 1) {
									echo ' (Satu)';
								} elseif ($spt->durasi == 2) {
									echo ' (Dua)';
								} elseif ($spt->durasi == 3) {
									echo ' (Tiga)';
								} elseif ($spt->durasi == 4) {
									echo ' (Empat)';
								} elseif ($spt->durasi == 5) {
									echo ' (Lima)';
								} elseif ($spt->durasi == 6) {
									echo ' (Enam)';
								} elseif ($spt->durasi == 7) {
									echo ' (Tujuh)';
								} elseif ($spt->durasi == 8) {
									echo ' (Delapan)';
								} elseif ($spt->durasi == 9) {
									echo ' (Semibilan)';
								} elseif ($spt->durasi == 10) {
									echo ' (Sepuluh)';
								}
								?>  hari.</div>

		<div style="clear:both;"></div><br>

		<div class="kiri">Selesai &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan">1. Melaksanakan Urusan Dinas segera melaporkan hasilnya kepada Kepala BAPPEDA Provinsi Papua Barat.</div>
		<div class="kanan">2. Agar diindahkan dan dilaksanakan dengan penuh rasa tanggung jawab.</div>

		<div style="clear:both;"></div><br>
		</div>

		<div class="kiri-2">Dikeluarkan di&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2">Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"><?php echo $this->tgl_indo->tgl_indo($spt->tgl_spt)?></div>

		<div style="clear:both;"></div>

		<div class="kanan-3"><b><?php echo $ttd_spt->jabatan. " ". $ttd_spt->ket_jabatan?></b></div>

		<div style="clear:both;"></div><br><br><br>

		<div class="kanan-4"><b><u><?php echo $ttd_spt->nama_pegawai?></u></b></div>
		<div class="kanan-4"><b><?php echo $ttd_spt->pangkat?></b></div>
		<div class="kanan-4"><b>NIP. <?php echo $ttd_spt->nip?></b></div>

		<div style="clear:both;"></div><br>

		<div class="kiri-3"><b><u>Tembusan Disampaikan kepada Yth:</u></b></div><br>
		<div class="kiri-3">1. Bendahara Pengeluaran BAPPEDA Provinsi Papua Barat.</div>
		<div class="kiri-3">2. Yang Bersangkutan untuk dilaksanakan.</div>
		<div class="kiri-3">3. Arsip.</div>

		<div style="clear:both;"></div>
		
	</div>

	<?php } ?>
</body>
</html>
