<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Tambah SPT</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/spt/')?>">SPT</a></li>
                <li class="breadcrumb-item active">Tambah SPT</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/spt/simpan_data_spt/')?>" method="post">

                  <input type="hidden" name="id" id="id">

                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="no_spt">No SPT:</label>

                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <input type="text" class="form-control text-center" id="no_spt" name="no_spt" value="094/" disabled>
                        </div>

                        <div class="form-group col-md-4">
                          <input type="text" class="form-control text-center" id="no_urut_spt" name="no_urut_spt" title="Nomor SPT" placeholder="<?php echo ($no_spt) ?>" value="<?php echo ($no_spt) ?>" readonly>
                        </div>

                        <div class="form-group col-md-5">
                          <input type="text" class="form-control text-center" id="kode_spt" name="kode_spt" value="/SPT/BAPPEDA/" disabled>
                        </div>

                        <div class="form-group col-md-5">
                          <div class="form-group mb-3">
                            <select class="form-control select2 mb-3" name="bulan_romawi" id="bulan_romawi" required>
                              <option value disabled hidden selected>Bulan SPT</option>
                              <option value="I">I</option>
                              <option value="II">II</option>
                              <option value="III">III</option>
                              <option value="IV">IV</option>
                              <option value="V">V</option>
                              <option value="VI">VI</option>
                              <option value="VII">VII</option>
                              <option value="VIII">VIII</option>
                              <option value="IX">IX</option>
                              <option value="X">X</option>
                              <option value="XI">XI</option>
                              <option value="XII">XII</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group col-md-3">
                          <input type="text" class="form-control text-center" id="tahun_spt" name="bulan_spt" value="<?php echo '/'.date('Y'); ?>" disabled>
                        </div>
                      </div>

                      <div class="form-group mb-3">
                        <label>Tanggal SPT:</label>
                        <div class="input-group date reservationdate" id="reservationdate" data-target-input="nearest">
                          <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_spt" id="tanggal_spt" />
                          <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                        </div>
                      </div>

                      <label>Tanggal Berangkat dan Kembali:</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input type="text" class="form-control float-right reservation" name="tgl_berangkat_dan_kembali" id="tgl_berangkat_dan_kembali">
                      </div>

                      <label for="durasi">Durasi(Hari):</label>
                      <input type="number" class="form-control mb-3" id="durasi" name="durasi" placeholder="Durasi" value="" disabled>

                    </div>

                    <div class="form-group col-md-4">
                      <div class="form-group mb-3">
                        <label for="asal">Berangkat Dari:</label>
                        <select class="form-control select2 mb-3" name="kota_brgkt" id="kota_brgkt" >
                          <option value disabled hidden selected>Pilih Kab/Kota Asal</option>
                          <?php foreach ($kota as $kt) { ?>
                            <option value="<?php echo $kt->id_kota; ?>"><?php echo $kt->nama_kota; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group mb-3">
                        <label for="tujuan">Kota Tujuan:</label>
                        <select class="form-control select2 mb-3" name="kota_tujuan" id="kota_tujuan">
                          <option value disabled hidden selected>Pilih Kab/Kota Tujuan</option>
                          <?php foreach ($kota as $kt) { ?>
                            <option value="<?php echo $kt->id_kota; ?>"><?php echo $kt->nama_kota; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group mb-3">
                        <label for="alat_angkut">Alat Angkut:</label>
                        <select class="form-control select2 mb-3" name="alat_angkut" id="alat_angkut" >
                          <option value disabled hidden selected>Pilih Alat Angkut</option>
                          <?php foreach ($alat_angkut as $aa) { ?>
                            <option value="<?php echo $aa->id; ?>"><?php echo $aa->alat_angkut; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="mpd">Maksud Perjalanan Dinas:</label>
                        <textarea class="form-control" id="maksut_pd" rows="4" name="maksut_pd" placeholder="Masukkan Penjelasan..."></textarea>
                      </div>
                    </div>

                    <div class="form-group col-md-4">
                      <div class="form-group mb-3">
                        <label for="pa">Pembebanan Anggaran:</label>
                        <select class="form-control select2" name="sumber_anggaran" id="sumber_anggaran" >
                          <option value disabled hidden selected>Pilih Sumber Anggaran</option>
                          <?php foreach ($sumber_anggaran as $sa) { ?>
                            <option value="<?php echo $sa->id; ?>"><?php echo $sa->sumber_anggaran; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group mb-3">
                        <label for="pejabat_spt">Pejabat TTD SPT:</label>
                        <select class="form-control select2" name="pejabat_spt" id="pejabat_spt">
                          <option value disabled hidden selected>Pilih SPT</option>
                          <?php foreach ($pejabat_ttd_spt as $pts) { ?>
                            <option value="<?php echo $pts->id; ?>"><?php echo $pts->nama_pegawai; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group mb-3">
                        <label for="pejabat_sppd">Pejabat TTD SPPD:</label>
                        <select class="form-control select2" name="pejabat_sppd" id="pejabat_sppd" >
                          <option value disabled hidden selected>Pilih SPPD</option>
                          <?php foreach ($pejabat_ttd_sppd as $ptsppd) { ?>
                            <option value="<?php echo $ptsppd->id; ?>"><?php echo $ptsppd->nama_pegawai; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <!-- <div class="form-group mb-3">
                        <label for="pejabat_ppk">Pejabat Pembuat T? Komitmen (TTD):</label>
                        <select class="form-control select2" name="pejabat_ppk" id="pejabat_ppk" required="">
                          <option value="null" disabled hidden selected>Pilih PPTK</option>
                          <?php foreach ($pejabat_ttd_pptk as $ptpptk) { ?>
                            <option value="<?php echo $ptpptk->id; ?>"><?php echo $ptpptk->nama_pegawai; ?></option>
                          <?php } ?>
                        </select>
                      </div> -->
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Tambah Data</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>


