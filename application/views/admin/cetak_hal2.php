<!DOCTYPE html>
<html>
<head>
	<title>SPPD Halaman 2</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/')?>hal2.css">
</head>
<body>
	<div class="kotak-kiri-1">
		<div class="kiri-2 hidden">I. Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2 hidden">&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div class="kanan-2 hidden"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2 hidden">&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div class="kanan-2 hidden"></div>

		<div style="clear:both;"></div>

		<div class="kanan-3 hidden"><?php echo $ttd_sppd->jabatan. " ". $ttd_sppd->ket_jabatan?> PROVINSI PAPUA BARAT</div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4 hidden"><b><u><?php echo $ttd_sppd->nama_pegawai?></u></b></div>
		<div class="kanan-4 hidden"><?php echo $ttd_sppd->pangkat?></div>
		<div class="kanan-4 hidden">NIP. <?php echo $ttd_sppd->nip?></div>

		<div style="clear:both;"></div>
	</div>

	<div class="kotak-kanan-1">
		<div class="kiri-2">I. Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div>

		<div class="kanan-3"><?php echo $ttd_sppd->jabatan. " ". $ttd_sppd->ket_jabatan?> PROVINSI PAPUA BARAT</div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4"><b><u><?php echo $ttd_sppd->nama_pegawai?></u></b></div>
		<div class="kanan-4"><?php echo $ttd_sppd->pangkat?></div>
		<div class="kanan-4">NIP. <?php echo $ttd_sppd->nip?></div>

		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>

	<div class="kotak-kiri-2">
		<div class="kiri-2">II. Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>
		<div class="kanan-4-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.</div>
	</div>

	<div class="kotak-kanan-2">
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>
		<div class="kanan-4-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.</div>

		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>

	<div class="kotak-kiri-2">
		<div class="kiri-2">III. Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>
		<div class="kanan-4-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.</div>
	</div>

	<div class="kotak-kanan-2">
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>
		<div class="kanan-4-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.</div>

		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>

	<div class="kotak-kiri-2">
		<div class="kiri-2">IV. Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>
		<div class="kanan-4-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.</div>
	</div>

	<div class="kotak-kanan-2">
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Berangkat dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Ke<span class="hidden">rangkat dari</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br>

		<div class="kanan-4-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>
		<div class="kanan-4-3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP.</div>

		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>

	<div class="kotak-kiri-2">
		<div class="kiri-2">V. Tiba Kembali&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">&nbsp;&nbsp;&nbsp;&nbsp;Ke<span class="hidden">ba Kembali</span>&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"></div>

		<div style="clear:both;"></div><br><br><br><br>

		<div class="kanan-3-2"><b>MENGETAHUI<br><?php echo $ttd_sppd->jabatan. " ". $ttd_sppd->ket_jabatan?> PROVINSI PAPUA BARAT</b></div>

		<div style="clear:both; margin-bottom: 8px;"></div><br><br>

		<div class="kanan-4"><b><u><?php echo $ttd_sppd->nama_pegawai?></u></b></div>
		<div class="kanan-4"><?php echo $ttd_sppd->pangkat?></div>
		<div class="kanan-4">NIP. <?php echo $ttd_sppd->nip?></div>

		<div style="clear:both;"></div>
	</div>

	<div class="kotak-kanan-2">
		<div class="kiri-2">Telah diperiksa dengan keterangan bahwa, perjalanan tersebut atas perintahnya dan semata-mata untuk kepentingan dinas/Biro Humas Dan Protokol Setda Provinsi Papua Barat dalam waktu sesingkat-singkatnya.</div>

		<div style="clear:both; margin-bottom: 12px;"></div><br>

		<div class="kanan-3-2"><b>MENGETAHUI<br><?php echo $ttd_sppd->jabatan. " ". $ttd_sppd->ket_jabatan?> PROVINSI PAPUA BARAT</b></div>

		<div style="clear:both; margin-bottom: 7.5px;"></div><br><br>

		<div class="kanan-4"><b><u><?php echo $ttd_sppd->nama_pegawai?></u></b></div>
		<div class="kanan-4"><?php echo $ttd_sppd->pangkat?></div>
		<div class="kanan-4">NIP. <?php echo $ttd_sppd->nip?></div>


		<div style="clear:both;"></div>
	</div>

	<div class="kotak-kanan-3">
		<div>VI. Catatan Lain lain :</div>
	</div>

	<div class="kotak-kanan-3">
		<div>VII. <b>PERHATIAN</b></div>
		<div style="font-size:10px;">Pejabat yang berwenang menerbitkan SPD, Pegawai yang melakukan Perjalanan Dinas, Para pejabat yang mengesahkan berangkat/tiba, serta Bendaharawan bertanggung jawab berdasarkan peraturan-peraturan Keuangan Negara, apabila negara menderita Rugi akibat kesalahan, kelalaian dan kealpaannya.</div>
	</div>
	
	<div style="float:left; width:10%; font-size:10px; text-decoration: underline;">
	Catatan:
	</div>
	<div style="float:left; width:90%; font-size:6px;">
	Segera meyerahkan kembali SPPD ini kepada pemegang kas BAPPEDA Provinsi Papua Barat selambat-lambatnya 3 (tiga) hari setelah kembali di tempat kedudukan dengan melampirkan tiket pulang-pergi (Pasal 8. SK. Gubernus Irian Jaya Barat No. 1 Tahun 2003 Tanggal 14 Juli 2003 tentang ketentuan perjalanan dinas di lingkungan Pemda Provinsi Papua Barat.
	</div>
</body>
</html>
