<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">STL</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item active">STL</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data STL</h3><br><br>

                <div class="row">
                  <div class="col-lg-6">
                    <?php 
                      if(isset($_SESSION['flash'])){
                        Flasher::flash();
                      } ?>
                  </div>  
                </div>
                <?php if(function_exists('check_if_role_is')){
                if (check_if_role_is('1') || check_if_role_is('2')){ ?>
                <a class="btn btn-success" href="<?= base_url('/stl/tambah_stl')?>" role="button" title="Tambah spt">
                  <i class="fas fa-plus"> Tambah STL</i>
                </a>
              <?php } } ?>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No STL</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php  
                  $no = 1;
                  foreach ($stl as $stl):
                ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo ($stl->no_stl)?></td>
                  <td class="text-center">
                    <?php if($this->cek_cetak_stl->cek_cetak_stl($stl->id_stl) == true) {?>
                      <a class="btn btn-primary" href="<?= base_url('/stl/cetak_stl1/'); echo encrypt_url($stl->id_stl);?>" target="_blank" role="button" title="Cetak STL">
                        <i class="fas fa-print"> Cetak STL</i>
                      </a>
                    <?php } else { ?>
                      <button class="btn btn-primary" disabled><i class="fas fa-print"> Cetak STL</i></button>
                    <?php } ?>
                    
                    <!-- <a class="btn btn-primary" href="<?= base_url('/stl/cetak_stl2/'); echo encrypt_url($stl->id_stl);?>" target="_blank" role="button" title="Cetak STL">
                      <i class="fas fa-print"> Hal 2</i>
                    </a> -->
                    <a class="btn btn-warning" href="<?= base_url('/stl/ubah_stl/'); echo encrypt_url($stl->id_stl);?>" role="button" title="Edit">
                      <i class="fas fa-edit"></i>
                    </a>
                    <!-- <a class="btn btn-danger" href="#" role="button" title="Hapus" data-toggle="modal" data-target="#formModalDelete">
                      <i class="fas fa-trash"></i>
                    </a> -->
                  </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kota</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>

<!-- The Modal -->
<div class="modal modal-danger fade" id="formModalDelete" role="dialog" aria-labelledby="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
                
    <!-- Modal Header -->
    <div class="modal-header">
      <h5 class="modal-title text-center" id="myModal">Hapus Data STL</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
    </div>
        <form action="<?= base_url('/admin/stl/')?>hapusStl/id" method="post">
        <!-- Modal body -->
        <div class="modal-body">
          <p class="text-center">
            Apakah Anda Yakin ?
          </p>
        </div>
                  
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
        <input type="submit" class="btn btn-danger" name="submit" value="Yes">
        </form>
        </div>
    </div>
  </div>
</div>