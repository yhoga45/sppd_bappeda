<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Daftar Disposisi Tiap Bidang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
              <li class="breadcrumb-item active">Lihat Daftar Disposisi Tiap Bidang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">
        <div class="card">
        <div class="card-body">

          <div class="text-center mb-4">
          <?php foreach($surat_masuk as $sm){ ?>

            <h3>Nomor Surat:&nbsp; <?php echo $sm->no_surat; ?> </h3>

            <?php } ?>

          </div>

            <table id="dataMaster2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kepala Bidang</th>
                <th>Nama Bidang</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach($disposisi_bidang as $sp){ ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $sp->nama; ?></td>
                <td><?php echo $sp->keterangan_jabatan; ?></td>
                <td>
                <?php if($this->cek_cetak_dispo->cek_cetak_dispo($sm->id_surat_masuk) == true) {?>
                <a class="btn btn-primary mb-1" href="<?= base_url('suratmasuk/cetakDisposisi/'); echo encrypt_url($sp->id_surat_masuk).'/'. encrypt_url($sp->id_user); ?>" target="_blank" role="button" title="Cetak File Disposisi">
                  <i class="fas fa-print"></i>
                </a>
                <?php } else { ?>
                  <button class="btn btn-primary mb-1" disabled><i class="fas fa-print"></i></button>
                <?php } ?>
                </td>
              </tr>
              <?php 
              $no ++; } 
              ?>
              <!-- <tr>
                <td>1</td>
                <td>Pojok TIK</td>
                <td>
                <a class="btn btn-primary mb-1" href="#" target="_blank" role="button" title="Cetak File Disposisi">
                  <i class="fas fa-print"></i>
                </a>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>Persandian</td>
                <td>
                <a class="btn btn-primary mb-1" href="#" target="_blank" role="button" title="Cetak File Disposisi">
                  <i class="fas fa-print"></i>
                </a>
                </td>
              </tr> -->
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama Kepala Bidang</th>
                <th>Nama Bidang</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            </table>
          
          <div style="clear: both;"></div>

          <a href="<?= base_url('suratmasuk')?>" class="btn btn-warning mt-2">
          <i class="fas fa-long-arrow-alt-left"></i> Kembali
      	  </a>
        </div>
        </div>
        </div>
      </div>
    </section>