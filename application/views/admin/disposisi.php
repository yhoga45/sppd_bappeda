    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Input Disposisi</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
                <li class="breadcrumb-item active">Input Disposisi</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            <form action="<?= base_url('/suratmasuk/tambahDisposisi/'); echo $this->uri->segment(3); ?>" method="post">

                <?php foreach ($disposisi_dipilih as $dd) {
                  $sifat = $dd->sifat;
                  $disposisi = $dd->disposisi;
                  $perihal = $dd->perihal;
                  $uraian = $dd->uraian_kadis;
                  $id_pejabat = $dd->id_pejabat_ttd_disposisi;
                  // echo ($dd->diketahui_kadis); exit(0);
                  foreach ($tujuan_disposisi_dipilih as $td) {
                    $tujuan_disposisi = $td->id_tujuan;
                  }
                  if (isset($sifat) || isset($disposisi) || isset($perihal) || isset($kepada) || isset($uraian) || isset($tujuan_disposisi) || isset($id_pejabat)) { ?>
                  <input type="hidden" name="id" id="id">               
                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <div class="form-group mb-3">
                        <label for="sifat">Sifat:</label>
                        <?php if ($sifat == 'Segera') { ?>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera" checked>
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>
                        <?php } elseif($sifat == 'Amat Segera'){ ?>

                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera" checked>
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>
                        <?php } elseif ($sifat == 'Rahasia') { ?>

                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia" checked>
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>

                        <?php } else { ?>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>
                        <?php } ?>
                      </div>

                      <!-- <div class="form-group mb-3">
                        <label for="perihal">Perihal:</label>
                        <textarea class="form-control" id="perihal" rows="4" name="perihal" placeholder="Perihal..."><?php echo $perihal; ?></textarea>
                      </div> -->
                      
                      <div class="form-group mb-3">
                        <label for="kirim_surat_masuk">Perintah Kirim Surat Kepada Yth:</label>
                        <div class="select2-blue">
                          <select class="select2" multiple="multiple" data-placeholder="Kepada Yth..." data-dropdown-css-class="select2-blue" id="perintah_kirim_surat" name="perintah_kirim_surat[]" style="width: 100%;" required>
                         <?php foreach($user_dipilih as $ud){ ?>
                          <option value="<?php echo $ud->id_user; ?>" hidden selected><?php echo $ud->nama.' ('.$ud->role.' '.$ud->keterangan_jabatan.')'; ?></option>
                              <?php } ?>
                              <option disabled="">=====================================================</option>
                                <?php foreach ($user as $u): 
                                  if($u->id_role == 3){ ?>
                                    <option value="<?php echo $u->id_user; ?>"><?php echo $u->nama.' ('.$u->role.' '.$u->jenis_instansi.')'; ?></option>
                                  <?php } else { ?>
                                    <option value="<?php echo $u->id_user; ?>"><?php echo $u->nama.' ('.$u->role.' '.$u->keterangan_jabatan.')';?></option>
                                  <?php }
                              endforeach; ?>
                          </select>
                        </div>
                      </div>

                      <!-- <div class="form-group mb-3">
                        <label for="kepada">Kepada:</label>
                        <textarea class="form-control" id="kepada" rows="4" name="kepada" placeholder="Kepada..."><?php echo $kepada; ?></textarea>
                      </div> -->

                      <div class="form-group mb-3">
                        <label for="sifat">Disposisi:</label>
                        <?php if ($disposisi == 'Segera') { ?>
                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera" checked>
                           <label for="customRadio4" class="custom-control-label">Segera</label>
                         </div>

                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera">
                           <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                         </div>
                        <?php } elseif($disposisi == 'Amat Segera'){ ?>
                          <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera">
                           <label for="customRadio4" class="custom-control-label">Segera</label>
                         </div>

                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera" checked>
                           <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                         </div>
                        
                        <?php } else { ?>
                          <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera">
                           <label for="customRadio4" class="custom-control-label">Segera</label>
                         </div>

                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera">
                           <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                         </div>
                        <?php }?>

                      </div>

                      <div class="form-group mb-3">
                        <label for="tujuan">Tujuan:</label>
                        <?php 
                        $i=1; 
                        foreach ($tujuan as $t) { ?>
                          <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="customCheckbox<?php echo $i; ?>" name="tujuan[]" value="<?php echo $t->id; ?>" <?php if (in_array($t->id, $tujuan_disposisi_dipilih_ids)) echo 'checked'; ?>>
                            <label for="customCheckbox<?php echo $i; ?>" class="custom-control-label"><?php echo $t->tujuan; ?></label>
                          </div>
                        <?php $i++; } ?>
                      </div>

                    </div>

                    <div class="form-group col-md-6">

                      <div class="form-group mb-3">
                        <label for="uraian">Isi Diposisi:</label>
                        <textarea class="form-control" id="summary-ckeditor" name="uraian" placeholder="Uraian..."><?php echo $uraian; ?></textarea>
                      </div>

                      <div class="form-group mb-3">
                        <label for="pejabat_disposisi">Pejabat TTD Disposisi:</label>
                        <select class="form-control select2" name="pejabat_disposisi" id="pejabat_disposisi">
                          <?php 
                            if(strlen($id_pejabat) > 0){ 
                              foreach ($ttd_disposisi_dipilih as $tdd) { ?>
                              <option value="<?php echo $tdd->id_pejabat_ttd_disposisi; ?>" hidden selected><?php echo $tdd->nama_pegawai; ?></option> 
                              <option disabled="">=================================================================================</option>
                              <?php } ?>                    
                            <?php } else { ?>
                              <option value="null" disabled hidden selected>Pilih Pejabat Disposisi</option>
                            <?php } ?>
                          <?php foreach ($pejabat_disposisi as $pd) { ?>
                          <option value="<?php echo $pd->id; ?>"><?php echo $pd->nama_pegawai; ?></option>
                          <?php  } ?>
                        </select>
                      </div>


                      <!-- <div class="form-group mb-3">
                        <label for="diketahui"></label>
                          <div class="custom-control custom-checkbox">
                          <?php  if($dd->diketahui_kadis == 'Y'){?>
                            <input class="custom-control-input" type="checkbox" id="customCheckbox20" name="diketahui" value="Y" checked="checked" >
                          <?php } else { ?>
                            <input class="custom-control-input" type="checkbox" id="customCheckbox20" name="diketahui" value="Y">
                          <?php } ?>
                            <label for="customCheckbox20" class="custom-control-label">Telah diketahui oleh Kepala Dinas</label>
                          </div>
                      </div> -->

                    </div>

                  </div>
                  <?php } else { ?>
                  <input type="hidden" name="id" id="id">               
                  <div class="form-row">
                    <div class="form-group col-md-6">

                      <div class="form-group mb-3">
                        <label for="sifat">Sifat:</label>

                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                        <label for="customRadio1" class="custom-control-label">Segera</label>
                        </div>
                                    
                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                        <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                        </div>

                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                        <label for="customRadio3" class="custom-control-label">Rahasia</label>
                        </div>
                                  
                      </div>

                      <div class="form-group mb-3">
                          <label for="perihal">Perihal:</label>
                          <textarea class="form-control" id="perihal" rows="4" name="perihal" placeholder="Perihal..."></textarea>
                      </div>
<!-- 
                      <div class="form-group mb-3">
                          <label for="kepada">Kepada:</label>
                          <textarea class="form-control" id="kepada" rows="4" name="kepada" placeholder="Kepada..."></textarea>
                      </div> -->

                      <div class="form-group mb-3">
                          <label for="kirim_surat_masuk">Perintah Kirim Surat Kepada Yth:</label>
                          <div class="select2-blue">
                          <select class="select2" multiple="multiple" data-placeholder="Kepada Yth..." data-dropdown-css-class="select2-blue" id="perintah_kirim_surat" name="perintah_kirim_surat[]" style="width: 100%;" required>
                            <?php 
                              foreach ($user as $u): 
                                if($u->id_role == 3){ ?>
                                  <option value="<?php echo $u->id_user; ?>"><?php echo $u->nama.' ('.$u->role.' '.$u->jenis_instansi.')'; ?></option>
                                <?php } else { ?>
                                  <option value="<?php echo $u->id_user; ?>"><?php echo $u->nama.' ('.$u->role.')';?></option>
                                <?php }
                            endforeach; ?>
                        </select>
                          </div>
                      </div>

                      <div class="form-group mb-3">
                        <label for="sifat">Disposisi:</label>

                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera">
                        <label for="customRadio4" class="custom-control-label">Segera</label>
                        </div>
                                    
                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera">
                        <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                        </div>
                                  
                      </div>

                      <div class="form-group mb-3">
                        <label for="tujuan">Tujuan:</label>
                        <?php 
                        $i=1;
                        foreach ($tujuan as $t) { ?>
                          <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="customCheckbox<?php echo $i; ?>" name="tujuan[]" value="<?php echo $t->id; ?>">
                            <label for="customCheckbox<?php echo $i; ?>" class="custom-control-label"><?php echo $t->tujuan; ?></label>
                          </div>
                        <?php $i++; } ?>

                      </div>

                    </div>

                    <div class="form-group col-md-6">

                      <div class="form-group mb-3">
                          <label for="uraian">Isi Disposisi:</label>
                          <textarea class="form-control" id="uraian" rows="6" name="uraian" placeholder="Isi Disposisi..."></textarea>
                      </div>

                      <div class="form-group mb-3">
                        <label for="pejabat_disposisi">Pejabat TTD Disposisi:</label>
                          <select class="form-control select2" name="pejabat_disposisi" id="pejabat_disposisi">
                            <option value="null" disabled hidden selected>Pilih Pejabat Disposisi</option>
                            <?php foreach ($pejabat_disposisi as $pd) { ?>
                              <option value="<?php echo $pd->id; ?>"><?php echo $pd->nama_pegawai; ?></option>
                            <?php  } ?>
                          </select>
                      </div>

                      <!-- <div class="form-group mb-3">
                        <label for="diketahui"></label>
                          <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="customCheckbox20" name="diketahui" value="Y">
                            <label for="customCheckbox20" class="custom-control-label">Telah diketahui oleh Kepala Dinas</label>
                          </div>
                      </div> -->

                    </div>

                  </div>
                  <?php }
                } ?>

                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

<script src="<?= base_url('assets/plugins/')?>ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'uraian' );
</script>

