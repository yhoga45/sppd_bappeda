    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Ubah Data Kasubid/Kasubag</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <?php foreach ($user as $u) { ?>
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?= base_url('pengguna')?>">Kaban</a></li>
                  <li class="breadcrumb-item"><a href="<?= base_url('pengguna/kabid/'); echo encrypt_url($u->parent_id_user);?>">Kabid/Kabag</a></li>
                  <li class="breadcrumb-item"><a href="<?= base_url('pengguna/kasubid/'); echo encrypt_url($u->parent_id_user).'/'.encrypt_url($u->parent_id_sub_user);?>">Kasubid/Kasubag</a></li>
                  <li class="breadcrumb-item active">Ubah Data Pengguna</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/pengguna/simpan_ubah_pengguna/');  echo $this->uri->segment(3).'/'.encrypt_url($u->id_role).'/'.encrypt_url($u->parent_id_user).'/'.encrypt_url($u->parent_id_sub_user); ?>" method="post">
                  <input type="hidden" name="id_user" value="<?php echo $u->id_user; ?>">

                  <div class="form-group" style="width: 50%">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $u->username; ?>" disabled>
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="username">Role:</label>
                    <input type="text" class="form-control" id="role" name="role" value="<?php echo $u->role; ?>" disabled>
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="nama">Nama:</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $u->nama; ?>" required>
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="nama">NIP:</label>
                    <input type="text" class="form-control" id="nip" name="nip" value="<?php echo $u->nip; ?>" required>
                  </div>

                  <div class="form-group mb-3" style="width: 50%">
                    <label for="username">Keterangan Jabatan: </label>
                    <textarea class="form-control" id="summary-ckeditor" name="keterangan_jabatan" placeholder="Keterangan Jabatan..."><?php echo $u->keterangan_jabatan; ?></textarea>
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="password">Reset Password:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  </div>
                <?php   } ?>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
