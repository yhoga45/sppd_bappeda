  <style>
    .tooltip-edit {
      position: relative;
      display: inline-block;
      word-break: normal;
      word-spacing: normal;
      white-space: normal;
      line-break: auto;
      font-size: .875rem;
      word-wrap: break-word;

    }

    .tooltip-edit-text {
      visibility: hidden;
      width: 200px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
      right: 40px;
      top: -20px;
    }

    .tooltip-edit:hover .tooltip-edit-text {
      visibility: visible;
    }

    .tooltip-delete {
      position: relative;
      display: inline-block;
      word-break: normal;
      word-spacing: normal;
      white-space: normal;
      line-break: auto;
      font-size: .875rem;
      word-wrap: break-word;
    }

    .tooltip-delete-text {
      visibility: hidden;
      width: 200px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
      right: 75px;
      top: -20px;
    }

    .tooltip-delete:hover .tooltip-delete-text {
      visibility: visible;
    }
  </style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Data Pengguna</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= base_url('/pengguna')?>">Kaban</a></li>
            <li class="breadcrumb-item"><a href="<?= base_url('/pengguna/kabid/'); echo $this->uri->segment(3); ?>">Kabid/Kabag</a></li>
            <li class="breadcrumb-item active">Kasubid/Kasubag</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data Pengguna (Kepala Sub Bidang)</h3><br><br>

            <div class="row">
              <div class="col-lg-6">
              </div>  
            </div>

            <a href="<?= base_url('/pengguna/tambah_kasubid/'); echo $this->uri->segment(3).'/'.$this->uri->segment(4); ?>" class="btn btn-success" title="Tambah">
              <i class="fas fa-plus"></i> Tambah Data Pengguna
            </a>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          <table id="dataMaster2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>No</th>
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Jabatan</th>
                            <th>Status</th>
                            <!-- <th>Foto</th> -->
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php $i = 0;
                foreach ($user as $u) { ?>
                  <tr>
                    <td><?= $i+1; ?></td>
                    <td><?php echo ($u->username); ?></td>
                    <td><?php echo ($u->nama); ?></td>
                    <td><?php echo $u->role.' '.$u->keterangan_jabatan; ?></td>
                    <!-- <td class="text-center">
                      <img src="<?= base_url('assets/img/profiluser/'); echo $u->foto_profil; ?> " class="img-circle elevation-2" alt="User Image" style="width: 80px; height: 80px;" >
                    </td> -->
                    <td><?php
                    if($u->status_user == 0){ 
                      ?>
                      <a class="btn btn-success" href="<?= base_url('Pengguna/activate_user_kasubid/'); echo $this->uri->segment(3).'/'.$this->uri->segment(4).'/'. encrypt_url($u->id_user); ?>" role="button" title="Aktifkan">
                      Aktifkan
                    </a>
                    <button class="btn btn-danger" role="button" title="Aktifkan" disabled>
                      Non-Aktif
                    </a>
                   <?php
                    } elseif($u->status_user == 1) { 
                  ?>
                      <button class="btn btn-success" role="button" title="Aktifkan" disabled>
                      Aktif
                    </button>
                    <a class="btn btn-danger" href="<?= base_url('Pengguna/disable_user_kasubid/'); echo $this->uri->segment(3).'/'.$this->uri->segment(4).'/'. encrypt_url($u->id_user); ?>" role="button" title="Aktifkan">
                      Non-Aktifkan
                    </a>
                  <?php
                   }
                    ?></td>
                    <td class="text-center">           
                      <a class="btn btn-warning tampilModalUbahPengguna" href="<?= base_url('pengguna/ubah_kasubid/'); echo encrypt_url($u->id_user); ?>" role="button" title="Edit">
                        <i class="fas fa-edit"></i>
                      </a>

                      <!-- <a class="btn btn-danger" href="#" role="button" title="Hapus" data-toggle="modal" data-target="#formModalHapus">
                        <i class="fas fa-trash"></i>
                      </a> -->
                  </td>
                </tr>
                <?php $i++; } ?>
              </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <!-- <th>Foto</th> -->
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>


  <!-- Modal -->
  <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="judulModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="judulModal">Tambah Data Pengguna</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= base_url('/admin/data_pengguna/')?>tambahPengguna" method="post">

            <input type="hidden" name="id" id="id">

            <div class="form-group">
              <label for="username">Username:</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Username">
            </div>

            <div class="form-group">
              <label for="nama">Nama:</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
            </div>

            <div class="form-group">
              <label for="foto">File Foto</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="foto" id="foto">
                  <label class="custom-file-label" for="foto">Choose file</label>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Tambah Data</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal modal-danger fade" id="formModalHapus" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title text-center" id="myModal">Hapus Data Pengguna</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
          </div>
          <form action="<?= base_url('/admin/data_pengguna/')?>hapusPengguna/id" method="post">
            <!-- Modal body -->
            <div class="modal-body">
              <p class="text-center">
                Apakah Anda Yakin ?
              </p>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
              <input type="submit" class="btn btn-danger" name="submit" value="Yes">
            </form>
          </div>
        </div>
      </div>
    </div>