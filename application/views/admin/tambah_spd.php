    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Tambah SPD</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/spt/')?>">SPT</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/spd/detail_spd/'); echo $this->uri->segment(3);?>?>">SPD</a></li>
                <li class="breadcrumb-item active">Tambah SPD</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/spd/simpan_data_spd/'); echo $this->uri->segment(3);?>" method="post">

                  <?php
                  foreach ($spt_where as $st) {

                    ?>

                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="no_spt">No SPPD:</label>
                        <div class="form-row">
                          <div class="form-group col-md-3">
                            <input type="text" class="form-control" id="no_spt" name="no_spt" value="094/" disabled>
                          </div>

                          <div class="form-group col-md-4">
                            <input type="text" class="form-control text-center" id="no_urut_spd" name="no_urut_spd" title="Nomor SPD" value="<?php echo ($no_spd) ?>" placeholder="<?php echo ($no_spd) ?>" readonly>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control text-center" id="kode_spt" name="kode_spt" value="/SPPD/BAPPEDA/" disabled>
                          </div>

                          <div class="form-group col-md-5">
                            <div class="form-group mb-3">
                              <input type="text" name="bulan_romawi" id="bulan_romawi" hidden value="<?php echo explode("/", $st->no_spt)[4]; ?>">
                              <select class="form-control select2 mb-3" name="bulan" id="bulan" disabled="">
                                <option value="<?php echo explode("/", $st->no_spt)[4]; ?>" disabled hidden selected><?php echo explode("/", $st->no_spt)[4]; ?></option>
                              </select>
                            </div>
                          </div>

                          <div class="form-group col-md-3">
                            <input type="text" class="form-control text-center" id="tahun_spt" name="bulan_spt" value="<?php echo date('Y'); ?>" disabled>
                          </div>
                        </div>

                        <div class="form-group mb-3">
                          <label>Tanggal dikeluarkan:</label>
                          <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_spt" id="tanggal_spt" value="<?php echo $st->tgl_spt; ?>" disabled/>
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                          </div>
                        </div>

                        <label>Tanggal Berangkat dan Kembali:</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="far fa-calendar-alt"></i>
                            </span>
                          </div>
                          <input type="text" class="form-control float-right reservation" name="tgl_berangkat_dan_kembali" id="tgl_berangkat_dan_kembali" value="<?php echo $st->tgl_berangkat. '-'. $st->tgl_kembali; ?>" disabled>
                        </div>

                        <label for="durasi">Durasi(Hari):</label>
                        <input type="number" class="form-control mb-3" id="durasi" name="durasi" placeholder="Durasi" value="<?php echo $st->durasi; ?>" disabled>

                        <div class="form-group mb-3">
                          <label for="asal">Berangkat Dari:</label>
                          <select class="form-control select2 mb-3" name="kota_brgkt" id="kota_brgkt" disabled>  
                           <?php
                           foreach ($kota_brgkt as $kb) {
                            ?>
                            <option value="null" disabled hidden selected><?php echo $kb->nama_kota; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                    </div>

                    <div class="form-group col-md-4">
                      <div class="form-group mb-3">
                        <label for="tujuan">Kota Tujuan:</label>
                        <select class="form-control select2 mb-3" name="tujuan" id="tujuan" disabled>

                          <?php
                          foreach ($kota_tujuan as $kt) {
                            ?>
                            <option value="null" disabled hidden selected><?php echo $kt->nama_kota; ?></option>
                          <?php } ?>

                        </select>
                      </div>

                      <div class="form-group mb-3">
                        <label for="alat_angkut">Alat Angkut:</label>
                        <select class="form-control select2 mb-3" name="alat_angkut" id="alat_angkut" disabled>
                          <option value="null" disabled hidden selected><?php echo $st->alat_angkut; ?></option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="mpd">Maksut Perjalanan Dinas:</label>
                        <textarea class="form-control" id="maksut_pd" rows="4" name="maksut_pd" placeholder="Masukkan Penjelasan..." disabled><?php echo $st->maksut_pd; ?></textarea>
                      </div>

                      <div class="form-group mb-3">
                        <label for="nama_pj_pd">Pegawai yang ditugaskan:</label>
                        <select class="form-control select2" name="id_pegawai_pd" id="id_pegawai_pd" required>
                          <option value disabled hidden selected>Pilih Pegawai</option>
                          <?php 
                            foreach ($pegawai_pd as $pegawai):
                              $id_pegawai = $pegawai->id_pegawai;
                              $save = true;
                              foreach ($cekdatapegawai as $cek):
                                $id_cek = $cek->id_pegawai;   
                                if($id_pegawai == $id_cek){
                                  $save = false;
                                }
                              endforeach;
                              if($save == true){ ?>
                                <option value="<?php echo $pegawai->id_pegawai ?>"><?php echo $pegawai->nama_pegawai ?></option>
                              <?php
                              }
                            endforeach; 
                          ?>
                        </select>
                      </div>

                      <div class="form-group">
                       <label>Pegawai Pengikut:</label>
                       <div class="select2-blue">
                        <select class="select2" multiple="multiple" data-placeholder="Cari Pengikut PD..." data-dropdown-css-class="select2-blue" id="id_pengikut_pd" name="id_pengikut_pd[]" style="width: 100%;">
                          <?php 
                            foreach ($pegawai_pd as $pegawai):
                              $id_pegawai = $pegawai->id_pegawai;
                              $save = true;
                              foreach ($cekdatapegawai as $cek):
                                $id_cek = $cek->id_pegawai;   
                                if($id_pegawai == $id_cek){
                                  $save = false;
                                }
                              endforeach;
                              if($save == true){ ?>
                                <option value="<?php echo $pegawai->id_pegawai ?>"><?php echo $pegawai->nama_pegawai ?></option>
                              <?php
                              }
                            endforeach; 
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="form-group col-md-4">
                    <div class="form-group mb-3">
                      <label for="pa">Pembebanan Anggaran:</label>
                      <select class="form-control select2" name="sumber_anggaran" id="sumber_anggaran" disabled>
                        <option value="null" disabled hidden selected><?php echo $st->sumber_anggaran; ?></option>
                      </select>
                    </div>

                    <div class="form-group mb-3">
                      <label for="pejabat_spt">Pejabat TTD SPT:</label>
                      <select class="form-control select2" name="pejabat_spt" id="pejabat_spt" disabled>   
                        <?php
                        foreach ($ttd_spt as $tspt) {
                          ?>
                          <option value="null" disabled hidden selected><?php echo $tspt->nama_pegawai; ?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group mb-3">
                      <label for="pejabat_sppd">Pejabat TTD SPPD:</label>
                      <select class="form-control select2" name="pejabat_sppd" id="pejabat_sppd" disabled>
                        <?php
                        foreach ($ttd_sppd as $tspp) {
                          ?>
                          <option value="null" disabled hidden selected><?php echo $tspp->nama_pegawai; ?></option>
                        <?php } ?>

                      </select>
                    </div>

                    <!-- <div class="form-group mb-3">
                      <label for="pejabat_ppk">Pejabat Pembuat T? Komitmen (TTD):</label>
                      <select class="form-control select2" name="pejabat_ppk" id="pejabat_ppk" disabled>
                        <?php
                        foreach ($ttd_pptk as $tppt) {
                          ?>
                          <option value="null" disabled hidden selected><?php echo $tppt->nama_pegawai; ?></option>
                        <?php } ?>
                      </select>
                    </div> -->
                  </div>
                </div>
              <?php } ?>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

