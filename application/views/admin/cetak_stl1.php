<!DOCTYPE html>
<html>
<head>
	<title>STL</title> 
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/')?>stl1.css">
</head>
<body>
<?php $kaban = false;
	foreach ($pegawai_stl as $kp){
		if($kp->id_jabatan == 1){ 
			$kaban = true; 
		} 
	}
	if($kaban == true){ ?>
		<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papua Barat">
		<div id="kop">
			<b style="font-family:Arial; font-size:16px;">PEMERINTAH PROVINSI PAPUA BARAT <div id="kop1">SEKRETARIAT DAERAH 
				<p id="alamat_kop">Jalan Brigdjen Marinir (Purn) Abraham O. Aturui Arfai – Manokwari Provinsi Papua Barat <br>
				</p></div></b>
		</div>
		<div style="clear:both;"></div>
		<hr id="hr1">
		<hr id="hr2">
		<hr id="hr3">
		<hr id="hr4">

		<p id="bawah_kop">SURAT TUGAS LUAR</p>
		<hr style="border:2px; border-style: dashed; width:40%;">

		<p id="bawah_kop2">Nomor : <?php echo $stl->no_stl ?></p>

		<div style="height:450px;">
			<div class="kiri">Dasar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>	
			<div class="kanan">1. Surat dari Balai Pelaksanaan Jalan Nasional Nomor: W 04.02-Bb 1/15 tanggal 15 Juli 2020 tentang Undangan Rapat Koordinasi.</div>
			<div class="kanan">2. Perintah Sekretaris Daerah Papua Barat tanggal 20 Juli 2020.</div>

			<div style="clear:both;"></div><br>

			<div class="kiri">Kepada &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
			<?php 
			foreach ($pegawai_stl as $kepada): ?>
				<div class="kanan"><?php echo "<b>" . $kepada->nama_pegawai . " /</b> NIP. " . $kepada->nip . " / " . $kepada->jabatan . " " . $kepada->ket_jabatan." Provinsi Papua Barat"?>.</div>
			<?php endforeach; ?>

			<div style="clear:both;"></div><br>

			<div class="kiri">Untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
			<div class="kanan">1. Setelah menerima Surat Perintah Tugas ini segera menyiapkan diri untuk melaksanakan tugas luar.</div>
			<div class="kanan">2. Mengikuti / Melaksanakan Kegiatan : <?php echo $stl->maksud ?>.</div>
			<div class="kanan">3. Tugas luar dilaksanakan pada tanggal <b><?php echo $this->tgl_indo->tgl_indo($stl->tgl_mulai)?></b> - <b><?php echo $this->tgl_indo->tgl_indo($stl->tgl_selesai)?>.</b></div>
			<div class="kanan">4. Lokasi tugas luar / Kegiatan : <?php echo $stl->lokasi ?>.</div>
			<div class="kanan">5. Dan setelah berada pada lokasi tugas luar/kegiatan, segera melakukan validasi surat tugas luar dan pengambilan gambar melalui aplikasi SIKKEPO mobile Android.</div>
			<div class="kanan">6. Jika dilokasi tugas luar/kegiatan, tidak terjangkau signal seluler/offline, segera melakukan pengambilan gambar selfie sebagai validasi tugas luar.</div>
			<div class="kanan">7. Setelah melaksanakan tugas luar/kegiatan segera melaporkan hasilnya kepada Kepala Badan Perencanaan Pembangunan Daerah Provinsi Papua Barat.</div>
			<div class="kanan">8. Untuk jadi perhatian dan dilaksanakan dengan rasa penuh tanggung jawab.</div>

			<div style="clear:both;"></div><br>

		</div>

		<div class="kiri-2">Dikeluarkan di&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2">Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"><?php if(strlen($this->tgl_indo->tgl_indo($stl->tgl_stl)) > 0){ echo $this->tgl_indo->tgl_indo($stl->tgl_stl); } ?></div>

		<div style="clear:both;"></div>

		<div class="kanan-3">a.n. GUBERNUR PAPUA BARAT<br><b>SEKRETARIS DAERAH</b></div>

		<div style="clear:both;"></div><br><br><br>

		<div class="kanan-4">Drs. NATANIEL D. MANDACAN, M.Si</div>
		<div class="kanan-4">PEMBINA UTAMA</div>
		<div class="kanan-4">NIP. 19621111 198903 1 029</div>

		<div style="clear:both;"></div><br>

		<!-- <div class="kiri-3"><b><u>Tembusan:</u></b></div><br>
		<div class="kiri-3">1. Kepala Inspektur Provinsi Papua Barat.</div>
		<div class="kiri-3">2. Kepala Badan Kepegawaian Daerah Provinsi Papua Barat.</div>
		<div class="kiri-3">3. Kepala BPKAD Provinsi Papua Barat.</div>
		<div class="kiri-3">4. Arsip.</div>

		<div style="clear:both;"></div> -->
	</div>

	<?php } else { ?>
		<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papua Barat">
		<div id="kop">
			<!-- if kaban echo SEKRETARIAT DAERAH -->
			<b style="font-family:Arial; font-size:16px;">PEMERINTAH PROVINSI PAPUA BARAT <div id="kop1">BADAN PERENCANAAN PEMBANGUNAN DAERAH 
				<p id="alamat_kop">Jalan Brigdjen Marinir (Purn) Abraham O. Aturui Arfai – Manokwari Provinsi Papua Barat <br>
				Tlp/Hp. 081248790969 email: bappedapapuabarat@yahoo.co.id website: bappeda.papuabaratprov.go.id 
				</p></div></b>
		</div>
		<div style="clear:both;"></div>
		<hr id="hr1">
		<hr id="hr2">
		<hr id="hr3">
		<hr id="hr4">

		<p id="bawah_kop">SURAT TUGAS LUAR</p>
		<hr style="border:2px; border-style: dashed; width:40%;">

		<p id="bawah_kop2">Nomor : <?php echo $stl->no_stl ?></p>

		<div style="height:450px;">
			<div class="kiri">Dasar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
			<div class="kanan">1. Surat dari Sekretaris Daerah Provinsi Papua Barat Nomor: 005/930/SETDA-PB/2020 tanggal 23 Juli 2020 tentang Undangan.</div>
			<div class="kanan">2. Perintah Kepala BAPPEDA Provinsi Papua Barat.</div>

			<!-- if kaban echo Dibawah ini -->
			<!-- <div class="kanan">1. Surat dari Balai Pelaksanaan Jalan Nasional Nomor: W 04.02-Bb 1/15 tanggal 15 Juli 2020 tentang Undangan Rapat Koordinasi.</div>
			<div class="kanan">2. Perintah Sekretaris Daerah Papua Barat tanggal 20 Juli 2020.</div> -->

			<div style="clear:both;"></div><br>

			<div class="kiri">Kepada &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
			<?php 
			$no = 1;
			foreach ($pegawai_stl as $kepada): ?>
				<div class="kanan"><?php echo $no++ .". <b>" . $kepada->nama_pegawai . " /</b> NIP. " . $kepada->nip . " / " . $kepada->jabatan . " " . $kepada->ket_jabatan . " BAPPEDA Provinsi Papua Barat"?>.</div>
			<?php endforeach; ?>

			<div style="clear:both;"></div><br>

			<div class="kiri">Untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</div>
			<div class="kanan">1. Setelah menerima Surat Perintah Tugas ini segera menyiapkan diri untuk melaksanakan tugas luar.</div>
			<div class="kanan">2. Mengikuti / melaksanakan Kegiatan : <b><?php echo $stl->maksud ?></b>.</div>
			<div class="kanan">3. Tugas luar dilaksanakan pada tanggal <b><?php echo $this->tgl_indo->tgl_indo($stl->tgl_mulai)?></b> s/d <b><?php echo $this->tgl_indo->tgl_indo($stl->tgl_selesai)?>.</b></div>
			<div class="kanan">4. Lokasi tugas luar / kegiatan : <b><?php echo $stl->lokasi ?></b>.</div>
			<div class="kanan">5. Dan setelah berada pada lokasi tugas luar/kegiatan, segera melakukan validasi surat tugas luar dan pengambilan gambar melalui aplikasi SIKKEPO mobile Android.</div>
			<div class="kanan">6. Jika dilokasi tugas luar/kegiatan, tidak terjangkau signal seluler/offline, segera melakukan pengambilan gambar selfie sebagai validasi tugas luar.</div>
			<div class="kanan">7. Setelah melaksanakan tugas luar/kegiatan segera melaporkan hasilnya kepada Kepala Badan Perencanaan Pembangunan Daerah Provinsi Papua Barat.</div>
			<div class="kanan">8. Untuk jadi perhatian dan dilaksanakan dengan rasa penuh tanggung jawab.</div>

			<div style="clear:both;"></div><br>

		</div>

		<div class="kiri-2">Dikeluarkan di&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2">Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"><?php if(strlen($this->tgl_indo->tgl_indo($stl->tgl_stl)) > 0){ echo $this->tgl_indo->tgl_indo($stl->tgl_stl); } ?></div>

		<div style="clear:both;"></div>

		<div class="kanan-3"><b><?php if($ttd_spt->id_pegawai != 1){
			echo "an. KEPALA <br>";
		} echo $ttd_spt->jabatan. " ". $ttd_spt->ket_jabatan?></b></div>

		<div style="clear:both;"></div><br><br><br>

		<div class="kanan-4"><b><u><?php echo $ttd_spt->nama_pegawai?></u></b></div>
		<div class="kanan-4"><b><?php echo $ttd_spt->pangkat?></b></div>
		<div class="kanan-4"><b>NIP. <?php echo $ttd_spt->nip?></b></div>

		<div style="clear:both;"></div><br>

		<!-- <div class="kiri-3"><b><u>Tembusan:</u></b></div><br>
		<div class="kiri-3">1. Kepala Inspektur Provinsi Papua Barat.</div>
		<div class="kiri-3">2. Kepala Badan Kepegawaian Daerah Provinsi Papua Barat.</div>
		<div class="kiri-3">3. Kepala BPKAD Provinsi Papua Barat.</div>
		<div class="kiri-3">4. Arsip.</div>

		<div style="clear:both;"></div> -->
	</div>

	<?php }  ?>
</body>
</html>
