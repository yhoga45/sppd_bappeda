    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Ubah SPT</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/spt/')?>">SPT</a></li>
                <li class="breadcrumb-item active">Ubah SPT</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/spt/simpan_ubah_spt/'); echo $this->uri->segment(3); ?>" method="post">

                  <input type="hidden" name="id" id="id">

                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="no_spt">No SPT:</label>

                      <div class="form-row">
                        <div class="form-group col-md-3">
                          <input type="text" class="form-control" id="no_spt" name="no_spt" value="094/" disabled>
                        </div>
                        <?php foreach ($spt_where as $sw) { ?>
                          <div class="form-group col-md-4">
                            <input type="text" class="form-control text-center" id="no_urut_spt" name="no_urut_spt" title="Nomor SPT" value="<?php echo explode("/",$sw->no_spt)[1]; ?>" placeholder="No SPT" readonly>
                          </div>

                          <div class="form-group col-md-5">
                            <input type="text" class="form-control text-center" id="kode_spt" name="kode_spt" value="/<?php echo explode("/",$sw->no_spt)[2];?>/<?php echo explode("/",$sw->no_spt)[3];?>/" readonly>
                          </div>

                          <div class="form-group col-md-5">
                            <div class="form-group mb-3">
                              <select class="form-control select2 mb-3" name="bulan_romawi" id="bulan_romawi">
                                <option value="<?php echo explode("/",$sw->no_spt)[4]; ?>" hidden selected><?php echo explode("/",$sw->no_spt)[4]; ?> (Data Lama)</option>
                                <option disabled="">===================</option>
                                <option value="I">I</option>
                                <option value="II">II</option>
                                <option value="III">III</option>
                                <option value="IV">IV</option>
                                <option value="V">V</option>
                                <option value="VI">VI</option>
                                <option value="VII">VII</option>
                                <option value="VIII">VIII</option>
                                <option value="IX">IX</option>
                                <option value="X">X</option>
                                <option value="XI">XI</option>
                                <option value="XII">XII</option>
                              </select>
                              <!--  <span style="color: gray; font-size: 12px;">Data Sebelumnya : </span> <span style="color: red; font-size: 13px;"><?php echo ''.explode("/",$sw->no_spt)[4]; ?></span> -->
                            </div>
                          </div>

                          <div class="form-group col-md-3">
                            <input type="text" class="form-control text-center" id="tahun_spt" name="bulan_spt" value="<?php echo date('Y'); ?>" disabled>
                          </div>
                        </div>

                        <?php if((empty($sw->tgl_spt))){ ?>
                          <div class="form-group mb-3">
                            <label>Tanggal SPT:</label>
                            <div class="input-group date reservationdate" id="reservationdate" data-target-input="nearest">
                              <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_spt" id="tanggal_spt"/>
                              <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                              </div>
                            </div>
                          </div>
                        <?php } else { ?>
                        <div class="form-group mb-3">
                          <label>Tanggal SPT:</label>
                          <div class="input-group date reservationdate" id="reservationdate" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_spt" id="tanggal_spt" value="<?php echo $sw->tgl_spt; ?>"/>
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                          </div>
                        </div>
                        <?php } ?>


                        <label>Tanggal Berangkat dan Kembali:</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="far fa-calendar-alt"></i>
                            </span>
                          </div>
                          <input type="text" class="form-control float-right reservation" name="tgl_berangkat_dan_kembali" id="tgl_berangkat_dan_kembali" value="<?php echo $sw->tgl_berangkat. '-'. $sw->tgl_kembali; ?>" required>
                        </div>

                        <label for="durasi">Durasi(Hari):</label>
                        <input type="number" class="form-control mb-3" id="durasi" name="durasi" value="<?php echo $sw->durasi; ?>" disabled>

                      </div>

                      <div class="form-group col-md-4">
                        <div class="form-group mb-3">
                          <label for="asal">Berangkat Dari:</label>

                          <?php if(empty($sw->id_kota_brgkt)){ ?>
                            <select class="form-control select2 mb-3" name="kota_brgkt" id="kota_brgkt">
                              <option value disabled hidden selected>Pilih Kab/Kota Asal</option>
                              <?php foreach ($kota as $kt) { ?>
                                <option value="<?php echo $kt->id_kota; ?>"><?php echo $kt->nama_kota; ?></option>
                              <?php } ?>
                            </select>
                          <?php } else { ?>
                          <select class="form-control select2 mb-3" name="kota_brgkt" id="kota_brgkt">
                            <?php foreach ($kota_brgkt as $kbr) { ?>
                             <option value="<?php echo $kbr->id_kota ?>" hidden selected><?php echo $kbr->nama_kota ?> (Data Lama)</option>
                             <option disabled="">=====================================================</option>
                           <?php } 

                           foreach ($kota as $kt) { ?>
                            <option value="<?php echo $kt->id_kota; ?>"><?php echo $kt->nama_kota; ?></option>
                          <?php } ?>
                        </select>
                        <?php } ?>
                      </div>

                      <div class="form-group mb-3">
                        <label for="tujuan">Kota Tujuan:</label>
                        <?php if(empty($sw->id_kota_tujuan)){ ?>
                          <select class="form-control select2 mb-3" name="kota_tujuan" id="kota_tujuan">
                            <option value disabled hidden selected>Pilih Kab/Kota Tujuan</option>
                            <?php foreach ($kota as $kt) { ?>
                              <option value="<?php echo $kt->id_kota; ?>"><?php echo $kt->nama_kota; ?></option>
                            <?php } ?>
                          </select>
                        <?php } else { ?>
                        <select class="form-control select2 mb-3" name="kota_tujuan" id="kota_tujuan">
                          <?php foreach ($kota_tujuan as $ktj) { ?>
                           <option value="<?php echo $ktj->id_kota ?>" hidden selected><?php echo $ktj->nama_kota ?> (Data Lama)</option>
                           <option disabled="">=====================================================</option>
                         <?php } 

                         foreach ($kota as $kt) { ?>
                          <option value="<?php echo $kt->id_kota; ?>"><?php echo $kt->nama_kota; ?></option>
                        <?php } ?>
                      </select>
                      <?php } ?>
                    </div>

                    <div class="form-group mb-3">
                      <label for="alat_angkut">Alat Angkut:</label>
                      <?php if(empty($sw->id_alat_angkut)){ ?>
                        <select class="form-control select2 mb-3" name="alat_angkut" id="alat_angkut">
                          <option value disabled hidden selected>Pilih Alat Angkut</option>
                          <?php foreach ($alat_angkut as $aa) { ?>
                            <option value="<?php echo $aa->id; ?>"><?php echo $aa->alat_angkut; ?></option>
                          <?php } ?>
                        </select>
                      <?php } else { ?>
                      <select class="form-control select2 mb-3" name="alat_angkut" id="alat_angkut">
                       <?php foreach ($alat_ang as $a_a) { ?>
                        <option value="<?php echo $a_a->id_alat_angkut; ?>" hidden selected><?php echo $a_a->alat_angkut; ?> (Data Lama)</option>
                        <option disabled="">=====================================================</option>
                       <?php } 
                          foreach ($alat_angkut as $aa) { ?>
                          <option value="<?php echo $aa->id; ?>"><?php echo $aa->alat_angkut; ?></option>
                        <?php } ?>
                      </select>
                      <?php } ?>
                    </div>

                    <div class="form-group">
                      <label for="mpd">Maksut Perjalanan Dinas:</label>
                      <?php if(empty($sw->maksut_pd)){ ?>
                        <textarea class="form-control" id="maksut_pd" rows="4" name="maksut_pd" placeholder="Masukkan Penjelasan..."></textarea>
                      <?php } else { ?>
                        <textarea class="form-control" id="maksut_pd" rows="4" name="maksut_pd"><?php echo $sw->maksut_pd; ?></textarea>
                      <?php } ?>
                    </div>
                  </div>

                  <div class="form-group col-md-4">
                    <div class="form-group mb-3">
                      <label for="pa">Pembebanan Anggaran:</label>
                      <?php if(empty($sw->id_sumber_anggaran)){ ?> 
                        <select class="form-control select2" name="sumber_anggaran" id="sumber_anggaran">
                          <option value disabled hidden selected>Pilih Sumber Anggaran</option>
                          <?php foreach ($sumber_anggaran as $sa) { ?>
                            <option value="<?php echo $sa->id; ?>"><?php echo $sa->sumber_anggaran; ?></option>
                          <?php } ?>
                        </select>
                      <?php } else { ?>
                      <select class="form-control select2" name="sumber_anggaran" id="sumber_anggaran">
                       <?php foreach ($sum_ang as $s_a) { ?>
                        <option value="<?php echo $s_a->id_sumber_anggaran; ?>" hidden selected><?php echo $s_a->sumber_anggaran; ?> (Data Lama)</option>
                        <option disabled="">=====================================================</option>
                       <?php } 
                        foreach ($sumber_anggaran as $sa) { ?>
                        <option value="<?php echo $sa->id; ?>"><?php echo $sa->sumber_anggaran; ?></option>
                       <?php } ?>
                      </select>
                      <?php } ?>
                  </div>

                  <div class="form-group mb-3">
                    <label for="pejabat_spt">Pejabat TTD SPT:</label>
                    <?php if(empty($sw->id_pejabat_ttd_spt)){ ?>
                      <select class="form-control select2" name="pejabat_spt" id="pejabat_spt">
                        <option value disabled hidden selected>Pilih SPT</option>
                        <?php foreach ($pejabat_ttd_spt as $pts) { ?>
                          <option value="<?php echo $pts->id; ?>"><?php echo $pts->nama_pegawai; ?></option>
                        <?php } ?>
                      </select>
                    <?php } else { ?>
                    <select class="form-control select2" name="pejabat_spt" id="pejabat_spt">
                      <?php foreach ($ttd_spt as $tspt) { ?>
                        <option value="<?php echo $tspt->id_pejabat_ttd_spt; ?>" hidden selected><?php echo $tspt->nama_pegawai; ?> (Data Lama)</option>
                        <option disabled="">=====================================================</option>
                      <?php } 

                      foreach ($pejabat_ttd_spt as $pts) { ?>
                        <option value="<?php echo $pts->id; ?>"><?php echo $pts->nama_pegawai; ?></option>
                      <?php } ?>
                    </select>
                    <?php } ?>
                  </div>

                  <div class="form-group mb-3">
                    <label for="pejabat_sppd">Pejabat TTD SPPD:</label>
                    <?php if(empty($sw->id_pejabat_ttd_sppd)){ ?> 
                      <select class="form-control select2" name="pejabat_sppd" id="pejabat_sppd">
                        <option value disabled hidden selected>Pilih SPPD</option>
                        <?php foreach ($pejabat_ttd_sppd as $ptsppd) { ?>
                          <option value="<?php echo $ptsppd->id; ?>"><?php echo $ptsppd->nama_pegawai; ?></option>
                        <?php } ?>
                      </select>
                    <?php } else { ?>
                    <select class="form-control select2" name="pejabat_sppd" id="pejabat_sppd">
                      <?php foreach ($ttd_sppd as $tsppd) { ?>
                        <option value="<?php echo $tsppd->id_pejabat_ttd_sppd; ?>" hidden selected><?php echo $tsppd->nama_pegawai; ?> (Data Lama)</option>
                        <option disabled="">=====================================================</option>
                      <?php } 

                      foreach ($pejabat_ttd_sppd as $ptsppd) { ?>
                        <option value="<?php echo $ptsppd->id; ?>"><?php echo $ptsppd->nama_pegawai; ?></option>
                      <?php } ?>
                    </select>
                    <?php } ?>
                  </div>

                  <!-- <div class="form-group mb-3">
                    <label for="pejabat_ppk">Pejabat Pembuat T? Komitmen (TTD):</label>
                    <select class="form-control select2" name="pejabat_ppk" id="pejabat_ppk">
                      <?php foreach ($ttd_pptk as $tpptk) { ?>
                        <option value="<?php echo $tpptk->id_pejabat_ttd_pptk; ?>" hidden selected><?php echo $tpptk->nama_pegawai; ?> (Data Lama)</option>
                        <option disabled="">=====================================================</option>
                      <?php } 

                      foreach ($pejabat_ttd_pptk as $ptpptk) { ?>
                        <option value="<?php echo $ptpptk->id; ?>"><?php echo $ptpptk->nama_pegawai; ?></option>
                      <?php } ?>
                    </select>
                  </div> -->
                </div>
              </div>

            <?php } ?>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Ubah Data</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</section>

