    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Input Disposisi</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
                <li class="breadcrumb-item active">Input Disposisi</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            <form action="<?= base_url('/suratmasuk/tambahDisposisi/'); echo $this->uri->segment(3); ?>" method="post">

            <?php foreach ($disposisi_dipilih as $dd) {
                  $sifat = $dd->sifat;
                  if (isset($sifat)) { ?>
                <input type="hidden" name="id" id="id">               
                  <div class="form-row">
                    <div class="form-group col-md-6">

                    <div class="form-group mb-3">
                        <label for="sifat">Sifat:</label>
                        <?php if ($sifat == 'Segera') { ?>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera" checked>
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>
                        <?php } elseif($sifat == 'Amat Segera'){ ?>

                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera" checked>
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>
                        <?php } elseif ($sifat == 'Rahasia') { ?>

                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia" checked>
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>

                        <?php } else { ?>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                            <label for="customRadio1" class="custom-control-label">Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                            <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                          </div>
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                            <label for="customRadio3" class="custom-control-label">Rahasia</label>
                          </div>
                        <?php } ?>
                      </div>

                      <!-- <div class="form-group mb-3">
                        <label for="perihal">Perihal:</label>
                        <textarea class="form-control" id="perihal" rows="4" name="perihal" placeholder="Perihal..."><?php echo $perihal; ?></textarea>
                      </div> -->
                      <?php } else { ?>

                        <div class="form-group mb-3">
                        <label for="sifat">Sifat:</label>

                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                        <label for="customRadio1" class="custom-control-label">Segera</label>
                        </div>
                                    
                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                        <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                        </div>

                        <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                        <label for="customRadio3" class="custom-control-label">Rahasia</label>
                        </div>
                                  
                      </div>

                      <!-- <div class="form-group mb-3">
                          <label for="perihal">Perihal:</label>
                          <textarea class="form-control" id="perihal" rows="4" name="perihal" placeholder="Perihal..."></textarea>
                      </div> -->

                          <?php }
                    } ?>
                    </div>   
                </div>

                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>

            </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    
</main>