<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Surat Keluar</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item active">Surat Keluar</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Surat Keluar</h3><br><br>

                <div class="row">
                  <div class="col-lg-6">
                    <?php 
                    if(isset($_SESSION['flash'])){
                      Flasher::flash();
                    } ?>
                  </div>  
                </div>
               <?php if(function_exists('check_if_role_is')){
              if (check_if_role_is('2')){ ?>
                  <a class="btn btn-success" href="<?= base_url('/suratkeluar/tambah_surat_keluar')?>" role="button" title="Tambah Surat Keluar">
                    <i class="fas fa-plus"> Tambah Surat Keluar</i>
                  </a>
              <?php } } ?>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No Agenda</th>
                      <th>Kode Klasifikasi</th>
                      <th>Perihal</th>
                      <th>File</th>
                      <th>Tujuan Surat</th>
                      <th>No. Surat</th>
                      <th>Tgl Surat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                    $no = 1;
                    foreach ($data as $tampil):
                      ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo ($tampil->no_agenda); ?></td>
                        <td><?php echo ($tampil->kode_klasifikasi); ?></td>
                        <td><?php echo ($tampil->isi_ringkas); ?></td>
                        <td><a href="<?= base_url('/upload/surat_keluar/')?><?php echo ($tampil->file)?>" target="_blank"><?php echo ($tampil->file); ?></a></td>
                        <td><?php echo ($tampil->tujuan); ?></td>
                        <td><?php echo ($tampil->no_surat); ?></td>
                        <td><?php echo ($tampil->tgl_surat); ?></td>
                        <td class="text-center">
<!--                           <a class="btn btn-primary" href="<?= base_url('suratkeluar/cetakDisposisi/'); ?><?php echo ($tampil->id_surat); ?>" target="_blank" role="button" title="Cetak File Disposisi">
                            <i class="fas fa-print"></i>
                          </a> -->
                          <?php if(function_exists('check_if_role_is')){
                          if (check_if_role_is('1') || check_if_role_is('2')){ ?>
                          <a class="btn btn-warning" href="<?= base_url('/suratkeluar/ubah_surat_keluar/'); echo encrypt_url($tampil->id_surat)?>?>" role="button" title="Edit">
                            <i class="fas fa-edit"></i>
                          </a>
                          <?php } }?>
<!--                           <a class="btn btn-success" href="<?= base_url('/suratkeluar/disposisi/'); ?><?php echo ($tampil->id_surat); ?>" role="button" title="Disposisi">
                            <i class="fas fa-file"></i>
                          </a> -->
                          <!-- <a class="btn btn-danger hapusSuratKeluar" href="#" role="button" title="Hapus" data-toggle="modal" data-target="#formModalDelete" data-id="<?php echo ($tampil->id_surat); ?>">
                            <i class="fas fa-trash"></i>
                          </a> -->
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>No Agenda</th>
                      <th>Kode Klasifikasi</th>
                      <th>Perihal</th>
                      <th>File</th>
                      <th>Tujuan Surat</th>
                      <th>No. Surat</th>
                      <th>Tgl Surat</th>
                      <th>Aksi</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>

      <!-- The Modal -->
      <div class="modal modal-danger fade" id="formModalDelete" role="dialog" aria-labelledby="myModal">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h5 class="modal-title text-center" id="myModal">Hapus Data Surat Keluar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
              </div>
              <!-- Modal body -->
              <div class="modal-body">  
                <form action="" method="post">
                  <input type="hidden" id="idHapus" name="idHapus" value="">
                  <p class="text-center">
                    Apakah Anda Yakin ?
                  </p>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                  <input type="submit" class="btn btn-danger" name="submit" value="Yes">
                </form>
              </div>
            </div>
          </div>
        </div>