    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Profil Saya</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item active">Profil Saya</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">

              <div class="card-header">
                <?php foreach ($profile as $p) { ?>
                  <a class="btn btn-warning" href="<?= base_url('/profileuser/ubah_profile/'); echo encrypt_url($p->id_user);?>" role="button" title="Ubah Profil Saya">
                    <i class="fas fa-edit"> Ubah Profil Saya</i>
                  </a>
                </div>

                <div class="card-body">

                  <div class="row">
                    <div class="col-md-2 mr-3">
                      <label for="role">Foto:</label><br>
                      <img src="<?= base_url('/assets/img/profiluser/');  echo $p->foto_profil; ?>" class="img-circle elevation-2" alt="User Image" style="width:200px;height: 200px;">
                    </div>

                    <div class="col-md-8">
                      <label>Hak Akses:</label>
                      <p><?php echo $p->role; ?></p>

                      <label>Username:</label>
                      <p><?php echo $p->username; ?></p>

                      <label>Nama:</label>
                      <p><?php echo $p->nama; ?></p>
                    </div>
                  <?php  } ?>

                </div>
              </div>
            </div>
          </div>
        </section>
