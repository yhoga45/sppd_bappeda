<!DOCTYPE html>
<html>
<head>
	<title>SPPD Halaman 1</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/')?>hal1.css">
</head>
<body>
	<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papua Barat">
	<div id="kop">
		<!-- Kaban Dengan Yang lain Kop nya sama -->
		<b style="font-family:Arial; font-size:16px;">PEMERINTAH PROVINSI PAPUA BARAT <div id="kop1">BADAN PERENCANAAN PEMBANGUNAN DAERAH 
			<p id="alamat_kop">Jalan Brigdjen Marinir (Purn) Abraham O. Aturui Arfai – Manokwari Provinsi Papua Barat <br>
			Tlp/Hp. 081248790969 email: bappedapapuabarat@yahoo.co.id website: bappeda.papuabaratprov.go.id 
			</p></div></b>
	</div>
	<div style="clear:both;"></div>
	<hr id="hr1">
	<hr id="hr2">
	<hr id="hr3">
	<hr id="hr4">

	<p id="bawah_kop">SURAT PERINTAH PERJALANAN DINAS</p>

	<p id="bawah_kop2">Nomor : <?php echo $kepada->no_sppd ?></p>

	<table style="width: 100%;">
		<tbody>
			<?php foreach ($spt_where as $sw) { ?>
				<tr>
					<td>01</td>
					<td>Pejabat berwenang yang memberi perintah</td>
					<td colspan="2">KEPALA BAPPEDA PAPUA BARAT</td>
				</tr>
				<tr>
					<td>02</td>
					<td>Nama/NIP Pegawai yang diperintahkan</td>
					<td colspan="2"><b><?php echo $kepada->nama_pegawai. "/" ?> <br> <i>NIP. <?php echo $kepada->nip;?></i></b></td>
				</tr>
				<tr>
					<td>03</td>
					<td>
						<p>a. Pangkat dan Golongan ruang gaji menurut PP No. 6 Tahun 1997</p><br>
						<p>b. Jabatan / Instansi </p><br>
						<p>c. Tingkat Menurut Peraturan Perjalan Dinas</p>
					</td>
					<td colspan="2">
						<p>a. <?php echo $kepada->pangkat. " (". $kepada->golongan. ")"?></p><br>
						<p>b. <?php echo $kepada->jabatan;?></p><br>
						<p>c. C</p>
					</td>
				</tr>
				<tr>
					<td>04</td>
					<td>Maksud Perjalanan Dinas</td>
					<td colspan="2"><b><?php echo $sw->maksut_pd ?></b>
					</td>
				</tr>
				<tr>

					<td>05</td>
					<td>Alat Angkut Yang Dipergunakan</td>
					<td colspan="2"><?php echo $sw->alat_angkut; ?></td>

				</tr>
				<tr>
					<td>06</td>
					<td>
						<p>a. Tempat Berangkat</p>
						<p>a. Tempat Tujuan</p>
					</td>
					<td colspan="2">
						<p>a. <b><?php echo $kota_brgkt->nama_kota?></b></p>
						<p>b. <b><?php echo $kota_tujuan->nama_kota?></b></p>
					</td>
				</tr>
				<tr>
					<td>07</td>
					<td>
						<p>a. Lamanya Perjalanan Dinas</p>
						<p>b. Tanggal Berangkat</p>
						<p>c. Tanggal Harus Kembali</p>
					</td>
					<td colspan="2">
						<p>a. 
							<?php 
							echo $sw->durasi; 
							if ($sw->durasi == 1) {
								echo ' (Satu)';
							} elseif ($sw->durasi == 2) {
								echo ' (Dua)';
							} elseif ($sw->durasi == 3) {
								echo ' (Tiga)';
							} elseif ($sw->durasi == 4) {
								echo ' (Empat)';
							} elseif ($sw->durasi == 5) {
								echo ' (Lima)';
							} elseif ($sw->durasi == 6) {
								echo ' (Enam)';
							} elseif ($sw->durasi == 7) {
								echo ' (Tujuh)';
							} elseif ($sw->durasi == 8) {
								echo ' (Delapan)';
							} elseif ($sw->durasi == 9) {
								echo ' (Semibilan)';
							} elseif ($sw->durasi == 10) {
								echo ' (Sepuluh)';
							}
							?> 
						Hari</p>
						<p>b. <?php echo $this->tgl_indo->tgl_indo($sw->tgl_berangkat); ?></p>
						<p>c. <?php echo $this->tgl_indo->tgl_indo($sw->tgl_kembali); ?></p>
					</td>
				</tr>
				<tr>
					<td>08</td>
					<td>Pengikut: &nbsp; Nama </td>
					<td style="text-align:center;">Tanggal Lahir</td>
				</td>
				<td style="text-align:center;">Keterangan
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<b>						
						<?php 
						$no = 1;
						foreach ($pengikut as $pn) {

							echo $no.'. '. $pn->nama_pegawai.'<br>';
							$no++;
						}
						?>
					</b>
				</td>
				<td>
					<b>	
						<?php 
						
						foreach ($pengikut as $pn) {

							echo ' - <br>';
							
						}
						?> 
					</b>
				</td>
				<td><b>
					<?php 
				
					foreach ($pengikut as $pn) {

						echo $pn->ket_jabatan.'<br>';
			
					}
					?>
				</b></td>
			</tr>
			<tr>
				<td>09</td>
				<td>
					<p>Pembebanan Anggaran</p>
					<p>a. Instansi</p>
					<p>b. Mata Anggaran</p>
				</td>
				<td colspan="2">

					<p><b><?php echo "$sw->sumber_anggaran;" ?></b></p>
					<p>a. BAPPEDA Provinsi Papua Barat</p>
					<p>b. Biaya Perjalanan Dinas Tahun <?= date('Y'); ?></p>
					</td>
				</tr>
				<tr>
					<td>10</td>
					<td>Keterangan Lain-lain</td>
					<td colspan="2">Membawa Perlengkapan Seperlunya</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

	<div class="footer">
		<div style="clear:both;"></div><br>
		<?php foreach ($spt_where as $sw) { ?>
		<div class="kiri-2">Dikeluarkan di&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2">Manokwari</div>
		<div style="clear:both;"></div>
		<div class="kiri-2">Pada Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </div>
		<div class="kanan-2"><?= $this->tgl_indo->tgl_indo($sw->tgl_spt); ?></div>
		<?php } ?>

		<div style="clear:both;"></div>

		<?php foreach ($ttd_sppd as $tsd) { ?>
		<div class="kanan-3"><b><?php echo $tsd->jabatan." ".$tsd->ket_jabatan; ?></b></div>

		<div style="clear:both;"></div><br><br><br>

		<div class="kanan-4"><b><u><?php echo $tsd->nama_pegawai; ?></u></b></div>
		<div class="kanan-4"><b><?php echo $tsd->pangkat; ?></b></div>
		<div class="kanan-4"><b>NIP. <?php echo $tsd->nip; ?></b></div>
		<?php } ?>
		<div style="clear:both;"></div><br>

		<div class="kiri-3"><b><u>Tembusan Disampaikan Kepada Yth:</u></b></div><br>
		<div class="kiri-3">1. Bendahara Pengeluaran.</div>
		<div class="kiri-3">2. Yang Bersangkutan Untuk Dilaksanakan.</div>
		<div class="kiri-3">3. Arsip.</div>

		<div style="clear:both; margin-top: 10px;"></div>
	</div>
</div>
</body>
</html>
