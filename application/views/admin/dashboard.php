<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?php echo $spt ?></h3>

              <p>SPT</p>
            </div>
            <div class="icon">
              <i class="ion ion-document"></i>
            </div>
            <a href="<?= base_url('/spt')?>" class="small-box-footer">Info Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?php echo $spd ?></h3>

              <p>SPD</p>
            </div>
            <div class="icon">
              <i class="ion ion-document"></i>
            </div>
            <a href="<?= base_url('/spt')?>" class="small-box-footer">Info Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3><?php echo $surat_masuk ?></h3>

              <p>Surat Masuk</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
            <a href="<?= base_url('/suratmasuk')?>" class="small-box-footer">Info Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?php echo $surat_keluar ?></h3>

              <p>Surat Keluar</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
            <a href="<?= base_url('/suratkeluar')?>" class="small-box-footer">Info Selengkapnya <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->

      <div class="row mt-4">
        <div class="col-lg-12 col-12">
          <div class="text-center"><h3>Notifikasi Selengkapnya</h3></div><br>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Jenis Surat</th>
                <th>Tanggal</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php  
              $no = 1;
              foreach ($notif as $nt):
                if($nt->status_dibaca == 'belum'){ ?>
                  <tr style='background-color:rgba(77, 184, 255, 0.2); color:gray;'>
                <?php } else {
                ?>
                <tr style='background-color:white;'>

                <?php } ?>
                  <td><?= $no++; ?></td>
                  <td><?php echo $nt->jenis; ?></td>
                  <td><?php echo $this->tgl_lengkap->tgl_lengkap($nt->tgl_notif); ?></td>
                  <td>
                 <a class="btn btn-primary" href="<?= base_url('Admin/ubah_status_baca_notif/'); echo $nt->id_notif.'/'.$nt->id_jenis_surat; ?>" role="button" title="Lihat Surat">Lihat Surat</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Jenis Surat</th>
                <th>Tanggal</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>

      <div class="row mt-4">
        <div class="col-lg-12 col-12">
          <div class="text-center"><h3>Data Perjalanan Dinas Bulan Ini:</h3></div><br>
          <table id="example3" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>No SPT</th>
                <th>Nama</th>
                <th>NIP</th>
                <th>Kota Tujuan</th>
                <th>Tanggal Berangkat dan Kembali</th>
                <th>Maksud Perjalanan Dinas</th>
              </tr>
            </thead>
            <tbody>
            <?php  
              $no = 1;
              foreach ($data as $tampil):
                ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?php echo $tampil->no_spt ?></td>
                  <td><?php echo $tampil->nama_pegawai ?></td>
                  <td><?php echo $tampil->nip ?></a></td>
                  <td><?php echo $tampil->nama_kota ?></td>
                  <td><?php echo $tampil->tgl_berangkat. " - " . $tampil->tgl_kembali ?></td>
                  <td><?php echo $tampil->maksut_pd ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>No SPT</th>
                <th>Nama</th>
                <th>NIP</th>
                <th>Kota Tujuan</th>
                <th>Tanggal Berangkat dan Kembali</th>
                <th>Maksud Perjalanan Dinas</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>

        <!-- <div class="col-lg-12 col-12 mt-4 mb-4 text-center">
          <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23F6BF26&amp;ctz=Asia%2FJakarta&amp;src=ZW4uaW5kb25lc2lhbiNob2xpZGF5QGdyb3VwLnYuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%230B8043&amp;showTitle=1&amp;showNav=1&amp;showTz=0&amp;hl=id&amp;showCalendars=1&amp;showPrint=0&amp;title=Kalender" style="border:solid 1px #777" width="1200" height="600" frameborder="0" scrolling="no"></iframe>
        </div> -->

      <!-- </div> -->
        
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
