<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Tambah Kabid/Kabag</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('pengguna')?>">Kaban</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('pengguna/kabid/'); echo $this->uri->segment(3);?>">Kabid/Kabag</a></li>
                <li class="breadcrumb-item active">Tambah Data Kabid/Kabag</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/Pengguna/simpan_data_kabid/'); echo $this->uri->segment(2); ?>" method="post">

                  <input type="hidden" name="id" id="id">

                  <div class="form-group" style="width: 50%">
                    <label for="nama">Nama Lengkap:</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="">
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="nama">NIP:</label>
                    <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" required="">
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" required="">
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                  </div>

                  <div class="form-group mb-3" style="width: 50%">
                    <label for="role">Hak Akses:</label>
                    <select class="form-control select2 mb-3" name="role" id="role">
                      <option value="null" disabled hidden selected>Pilih Hak Akses</option>
                      <?php foreach ($role as $r) { ?>
                        <option value="<?php echo $r->id; ?>"><?php echo $r->role; ?></option>
                      <?php   } ?>

                    </select>
                  </div>

                  <div class="form-group mb-3" style="width: 50%">
                    <label for="username">Keterangan Jabatan: </label>
                    <textarea class="form-control" id="summary-ckeditor" name="keterangan_jabatan" placeholder="Keterangan Jabatan..."></textarea>
                  </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Tambah Data</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
