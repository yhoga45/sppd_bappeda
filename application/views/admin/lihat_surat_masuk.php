<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Lihat Status Surat Masuk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
              <li class="breadcrumb-item active">Lihat Status Surat Masuk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">
        <div class="card">
        <div class="card-body">

          <div class="text-center mb-4">
          <?php foreach($surat_masuk as $sm){ ?>

            <h3>Nomor Surat:&nbsp; <?php echo $sm->no_surat; ?> </h3>

            <?php } ?>

          </div>

            <table id="dataMaster2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Status Surat</th>
                <th>Pengirim Surat</th>
                <th>Tanggal Kirim Surat</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach($status_posisi as $sp){ ?>
              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $sp->nama_penerima; ?></td>
                <td><?php echo $sp->nama_pengirim; ?></td>
                <td><?php echo $this->tgl_lengkap->tgl_lengkap($sp->tgl_kirim); ?></td>
              </tr>
              <?php 
              $no ++; } 
              ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Status Surat</th>
                <th>Pengirim Surat</th>
                <th>Tanggal Kirim Surat</th>
              </tr>
            </tfoot>
            </table>
          
          <div style="clear: both;"></div>

          <a href="<?= base_url('suratmasuk')?>" class="btn btn-warning mt-2">
          <i class="fas fa-long-arrow-alt-left"></i> Kembali
      	  </a>
        </div>
        </div>
        </div>
      </div>
    </section>