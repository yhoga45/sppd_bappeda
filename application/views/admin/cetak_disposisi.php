<!DOCTYPE html>
<html>
<head>
	<title>Disposisi</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/')?>disposisi.css">
</head>
<body>
	<div class="kotak-2">
		<img src="<?= base_url('assets/img/')?>logo.png" alt="Logo Papau Barat">
		<div id="kop">
			<!-- Kaban Dengan Yang lain Kop nya sama -->
			<b style="font-family:Arial; font-size:16px;">PEMERINTAH PROVINSI PAPUA BARAT <div id="kop1">BADAN PERENCANAAN PEMBANGUNAN DAERAH 
				<p id="alamat_kop">Jalan Brigdjen Marinir (Purn) Abraham O. Aturui Arfai – Manokwari Provinsi Papua Barat <br>
				Tlp/Hp. 081248790969 email: bappedapapuabarat@yahoo.co.id website: bappeda.papuabaratprov.go.id 
				</p></div></b>
		</div>
	</div>
	<div style="clear:both;"></div>
	<!-- <hr id="hr1">
	<hr id="hr2">
	<hr id="hr3">
	<hr id="hr4"> -->

	<div id="bawah_kop"><u>Lembar Disposisi</u></div>
	<table style="width: 100%;">
		<tbody>
			<tr>
				<td width="50%">
					<p>No.Surat/RDG/Disposisi<span style="visibility: hidden;">ri</span>&nbsp;&nbsp;: 
						<?php 
						// var_dump($disposisi);
						// exit(0);
						if (isset($disposisi)) { 
							echo $disposisi->no_surat; 
						} ?>
					</p>
					<p>Surat/RDG/Disposisi Lain &nbsp;: 
						<?php 
						if (isset($disposisi)) { 
							echo $disposisi->asal_surat;
						} ?>
					</p>
					<p>Tanggal<span style="visibility: hidden;">DG/Disposisi Dari </span>: 
						<?php 
						if (isset($disposisi)) { 
							echo $this->tgl_indo->tgl_indo($disposisi->tgl_surat); 
						} ?>
					</p>
				</td>
				<td width="50%">
					<p>Diterima Tanggal &nbsp;: 
						<?php 
						if (isset($disposisi)) { 
							echo $this->tgl_indo->tgl_indo($disposisi->tgl_diterima); 
						}?>
					</p>
					<p>No Agenda<span style="visibility: hidden;">nggal</span> &nbsp;&nbsp;: 
						<?php 
						if (isset($disposisi)) { 
							echo $disposisi->no_agenda; 
						}?>
					</p>
					<span>Sifat<span style="visibility: hidden;">rima Tanggal</span> :
					<?php if (isset($disposisi)) { 
						if ($disposisi->sifat == "Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="" checked="checked">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Rahasia
							</label>
						<?php } elseif ($disposisi->sifat == "Amat Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="" checked="checked">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Rahasia
							</label>
						<?php } elseif ($disposisi->sifat == "Rahasia") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="" checked="">Rahasia
							</label>
						<?php } else{ ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Rahasia
							</label>
						<?php }
					} ?>

				</span>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height:50px;">
				<p>Perihal<span style="visibility: hidden;"> Yth</span> &nbsp;&nbsp;&nbsp;&nbsp;: 
					<?php 
					if (isset($disposisi)) { 
						echo $disposisi->isi_ringkas;
					} ?>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="height:50px;">
				<p>Kepada Yth &nbsp;&nbsp;&nbsp;:
					<?php foreach ($kepada as $k) { ?>
						<p>- <?php echo $k->role.' '.$k->keterangan_jabatan; ?> </p>
					<?php } ?>
				</p>
			</td>
		</tr>
		<!-- <tr>
			<td colspan="2" style="height:50px;">
				<p>Disposisi dari : 
				Kepala Badan Perencanaan Pembangunan Daerah
				</p>
			</td>
		</tr> -->
		<!-- <tr>
			<td colspan="2">
				<center>
					<span>Disposisi : </span>
					<?php 
					if (isset($disposisi)) { 
						if ($disposisi->disposisi == "Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="" checked="checked">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
						<?php } elseif ($disposisi->disposisi == "Amat Segera") { ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="" checked="checked">Amat Segera
							</label>
						<?php } else{ ?>
							<label class="checkbox-inline" >
								<input type="checkbox" value="">Segera
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" value="">Amat Segera
							</label>
							<?php 
						}
					} ?>						
				</center>
			</td>
		</tr> -->
		<tr>
			<td colspan="2">
				<div>
					<?php $i = 1; foreach ($tujuan as $j => $t) : ?>
					<?php if ($i == 1): ?>
						<center>
						<?php endif; ?>
						<label class="checkbox-inline">
							<input type="checkbox" value="" <?php if (in_array($t->id, $tujuan_disposisi_dipilih_ids)) echo "checked='checked'"; ?>><?php echo $t->tujuan; ?>
						</label>
						<?php if ($i == 4 or $j == (count($tujuan)-1)): ?>
						</center>
						<?php $i = 0; endif; ?>
						<?php $i++; endforeach; ?>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="kotak-1">
		<div><u><b>Uraian :</b></u>
		<div class="kiri-4" style="padding-bottom: 0px; margin-bottom: 0px; margin-top: 20px; font-size:11px;">
		<?php if($this->uri->segment(2)=='cetakDisposisiKasubid'){
			echo $disposisi->uraian_kabid; ?>
			</div></div>
			<div style="clear: both;"></div>
			

			<div class="kanan-3" style="padding-top: 100px; margin-top: 0px;">Manokwari, 
				<?php 
				if (isset($disposisi)) {  
					echo $this->tgl_indo->tgl_indo(date('m/d/Y'));
				} ?>

			</div>
			<div class="kanan-3" style="text-transform: uppercase;">
			<h4>
				<?php if (isset($disposisi)) {  
					echo $pejabat_disposisi->role. " " . $pejabat_disposisi->keterangan_jabatan; 
				}?> 
				</h4>
			</div>

			<div style="clear:both;"></div><br><br><br>

			<div class="kanan-4"><b><u><?php if (isset($disposisi)) { echo $pejabat_disposisi->nama; } ?></u></b></div>
			<div class="kanan-4"><b><?php if (isset($disposisi)) { echo 'NIP. '.$pejabat_disposisi->nip; } ?></b></div>

			<div style="clear:both;"></div>
		</div>

		<?php } else {
			echo $disposisi->uraian_kadis; ?>
			</div></div>
			<div style="clear: both;"></div>
			

			<div class="kanan-3" style="padding-top: 100px; margin-top: 0px;">Manokwari, 
				<?php 
				if (isset($disposisi)) {  
					echo $this->tgl_indo->tgl_indo(date('m/d/Y'));
				} ?>

			</div>
			<div class="kanan-3" style="text-transform: uppercase;">
			<h4>
				<?php if (isset($disposisi)) {  
					echo $pejabat_disposisi->jabatan. " " . $pejabat_disposisi->ket_jabatan; 
				}?> 
				</h4>
			</div>

			<div style="clear:both;"></div><br><br><br>

			<div class="kanan-4"><b><u><?php if (isset($disposisi)) { echo $pejabat_disposisi->nama_pegawai; } ?></u></b></div>
			<div class="kanan-4"><b><?php if (isset($disposisi)) { echo $pejabat_disposisi->pangkat; } ?></b></div>
			<div class="kanan-4"><b><?php if (isset($disposisi)) { echo 'NIP. '.$pejabat_disposisi->nip; } ?></b></div>

			<div style="clear:both;"></div>
		</div>
		<?php } ?>
		<!-- <?php if(!empty($disposisi->uraian_kadis)){ ?>
			<b>Kepala Badan : </b> &nbsp;
		<?php echo $disposisi->uraian_kadis; } 
		
		if (!empty($disposisi->uraian_kabid)){ ?>
			<b>Kepala Bidang : </b>
			<br>
		<?php  } ?>	 -->

		
</body>
</html>
