    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Input Disposisi</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/suratkeluar/')?>">Surat Keluar</a></li>
                <li class="breadcrumb-item active">Input Disposisi</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/suratkeluar/tambahDisposisi/'); echo $this->uri->segment(3); ?>" method="post">
                  <?php 
                  foreach ($disposisi_dipilih as $dd) {
                    $sifat = $dd->sifat;
                    $disposisi = $dd->disposisi;
                    $perihal = $dd->perihal;
                    $kepada = $dd->kepada;
                    $uraian = $dd->uraian;
                    foreach ($tujuan_disposisi_dipilih as $td) {
                      $tujuan_disposisi = $td->id_tujuan;
                    }
                    if (isset($sifat) && isset($perihal) && isset($kepada) && isset($uraian) || isset($tujuan_disposisi)) { ?>

                     <input type="hidden" name="id" id="id">

                     <div class="form-row">
                      <div class="form-group col-md-6">
                       <div class="form-group mb-3">
                         <label for="sifat">Sifat:</label>
                         <?php if ($sifat == 'Segera') { ?>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera" checked>
                             <label for="customRadio1" class="custom-control-label">Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                             <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                             <label for="customRadio3" class="custom-control-label">Rahasia</label>
                           </div>
                         <?php } elseif($sifat == 'Amat Segera'){ ?>

                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                             <label for="customRadio1" class="custom-control-label">Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera" checked>
                             <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                             <label for="customRadio3" class="custom-control-label">Rahasia</label>
                           </div>
                         <?php } elseif ($sifat == 'Rahasia') { ?>

                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                             <label for="customRadio1" class="custom-control-label">Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                             <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia" checked>
                             <label for="customRadio3" class="custom-control-label">Rahasia</label>
                           </div>

                         <?php } else { ?>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                             <label for="customRadio1" class="custom-control-label">Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                             <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                           </div>
                           <div class="custom-control custom-radio">
                             <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                             <label for="customRadio3" class="custom-control-label">Rahasia</label>
                           </div>
                         <?php } ?>
                       </div>

                       <!-- <div class="form-group mb-3">
                        <label for="perihal">Perihal:</label>
                        <textarea class="form-control" id="perihal" rows="4" name="perihal" placeholder="Perihal..."><?php echo $perihal; ?></textarea>
                       </div> -->

                      <div class="form-group mb-3">
                        <label for="kepada">Kepada:</label>
                        <textarea class="form-control" id="kepada" rows="4" name="kepada" placeholder="Kepada..."><?php echo $kepada; ?></textarea>
                      </div>
                      <div class="form-group mb-3">
                        <label for="sifat">Disposisi:</label>
                        <?php if ($disposisi == 'Segera') { ?>
                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera" checked>
                           <label for="customRadio4" class="custom-control-label">Segera</label>
                         </div>

                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera">
                           <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                         </div>
                        <?php } elseif($disposisi == 'Amat Segera'){ ?>
                          <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera">
                           <label for="customRadio4" class="custom-control-label">Segera</label>
                         </div>

                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera" checked>
                           <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                         </div>
                        
                        <?php } else { ?>
                          <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera">
                           <label for="customRadio4" class="custom-control-label">Segera</label>
                         </div>

                         <div class="custom-control custom-radio">
                           <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera">
                           <label for="customRadio5" class="custom-control-label">Amat Segera</label>
                         </div>
                        <?php }?>

                      </div>
                    </div>

               <div class="form-group col-md-6">

                <div class="form-group mb-3">
                 <label for="tujuan">Tujuan:</label>
                 <?php 
                 $i=0; 
                 foreach ($tujuan as $t) { ?>

                  <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="customCheckbox<?php echo $i; ?>" name="tujuan[]" value="<?php echo $t->id; ?>" <?php if (in_array($t->id, $tujuan_disposisi_dipilih_ids)) echo 'checked'; ?>>
                    <label for="customCheckbox<?php echo $i; ?>" class="custom-control-label"><?php echo $t->tujuan; ?></label>
                  </div>

                  <?php $i++; } ?>

                </div>

                <div class="form-group mb-3">
                  <label for="uraian">Uraian:</label>
                  <textarea class="form-control" id="uraian" rows="6" name="uraian" placeholder="Uraian..."><?php echo $uraian; ?></textarea>
                </div>

                <div class="form-group mb-3">
                  <label for="pejabat_disposisi">Pejabat TTD Disposisi:</label>
                  <select class="form-control select2" name="pejabat_disposisi" id="pejabat_disposisi">
                    <?php foreach ($ttd_disposisi_dipilih as $tdd) { ?>
                    <option value="<?php echo $tdd->id_pejabat_ttd_disposisi; ?>" hidden selected><?php echo $tdd->nama_pegawai; ?></option>
                     <?php } ?>
                    <option disabled="">=================================================================================</option>
                    <?php foreach ($pejabat_disposisi as $pd) { ?>
                      <option value="<?php echo $pd->id; ?>"><?php echo $pd->nama_pegawai; ?></option>
                    <?php  } ?>
                  </select>
                </div>
              </div>

            </div>

            <?php  
          } else { ?>
            <input type="hidden" name="id" id="id">

            <div class="form-row">
              <div class="form-group col-md-6">

                <div class="form-group mb-3">
                  <label for="sifat">Sifat:</label>

                  <div class="custom-control custom-radio">
                   <input class="custom-control-input" type="radio" id="customRadio1" name="sifat" value="Segera">
                   <label for="customRadio1" class="custom-control-label">Segera</label>
                 </div>

                 <div class="custom-control custom-radio">
                   <input class="custom-control-input" type="radio" id="customRadio2" name="sifat" value="Amat Segera">
                   <label for="customRadio2" class="custom-control-label">Amat Segera</label>
                 </div>

                 <div class="custom-control custom-radio">
                   <input class="custom-control-input" type="radio" id="customRadio3" name="sifat" value="Rahasia">
                   <label for="customRadio3" class="custom-control-label">Rahasia</label>
                 </div>

               </div>

               <!-- <div class="form-group mb-3">
                <label for="perihal">Perihal:</label>
                <textarea class="form-control" id="perihal" rows="4" name="perihal" placeholder="Perihal..."></textarea>
              </div> -->

              <div class="form-group mb-3">
                <label for="kepada">Kepada:</label>
                <textarea class="form-control" id="kepada" rows="4" name="kepada" placeholder="Kepada..."></textarea>
              </div>

              <div class="form-group mb-3">
                <label for="sifat">Disposisi:</label>

                <div class="custom-control custom-radio">
                 <input class="custom-control-input" type="radio" id="customRadio4" name="disposisi" value="Segera">
                 <label for="customRadio4" class="custom-control-label">Segera</label>
               </div>

               <div class="custom-control custom-radio">
                 <input class="custom-control-input" type="radio" id="customRadio5" name="disposisi" value="Amat Segera">
                 <label for="customRadio5" class="custom-control-label">Amat Segera</label>
               </div>

             </div>

           </div>

           <div class="form-group col-md-6">

            <div class="form-group mb-3">
              <label for="tujuan">Tujuan:</label>

              <?php 
              $i=0;
              foreach ($tujuan as $t) { ?>

                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input" type="checkbox" id="customCheckbox<?php echo $i; ?>" name="tujuan[]" value="<?php echo $t->id; ?>">
                  <label for="customCheckbox<?php echo $i; ?>" class="custom-control-label"><?php echo $t->tujuan; ?></label>
                </div>

                <?php $i++; } ?>

              </div>

              <div class="form-group mb-3">
                <label for="uraian">Uraian:</label>
                <textarea class="form-control" id="uraian" rows="6" name="uraian" placeholder="Uraian..."></textarea>
              </div>

              <div class="form-group mb-3">
                <label for="pejabat_disposisi">Pejabat TTD Disposisi:</label>
                <select class="form-control select2" name="pejabat_disposisi" id="pejabat_disposisi">
                  <option value="null" disabled hidden selected>Pilih Pejabat Disposisi</option>
                  <?php foreach ($pejabat_disposisi as $pd) { ?>
                    <option value="<?php echo $pd->id; ?>"><?php echo $pd->nama_pegawai; ?></option>
                  <?php  } ?>
                </select>
              </div>

            </div>

          </div>
        <?php  }
      } 
      ?>


    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Simpan Data</button>
    </div>
  </form>
</div>
</div>
</div>
</div>
</section>

