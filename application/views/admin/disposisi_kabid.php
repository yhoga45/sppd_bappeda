    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Input Disposisi</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
                <li class="breadcrumb-item active">Input Disposisi</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            <form action="<?= base_url('/suratmasuk/tambahDisposisi/'); echo $this->uri->segment(3); ?>" method="post">

            <div class="col-md-12">
              <div class="text-center mb-4">
                <h3>Detail Disposisi</h3>
              </div>
              <?php foreach($disposisi_dipilih as $dd){ ?>
              <div style="float: left; width: 22%;">
                <b>Sifat</b>
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <p>:&nbsp;<?php echo $dd->sifat; ?></p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Perihal</b>
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <p>:&nbsp;<?php echo $dd->isi_ringkas; ?></p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Perintah Kirim Surat Kepada</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                  :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <?php $no = 1; foreach($user_dipilih as $ud){ ?>
                        <p><?php echo $no++; ?>.&nbsp;<?php echo $ud->nama. ' (' .$ud->role. ' ' .$ud->keterangan_jabatan.')'; ?> </p>
                 <?php } ?>
                 </ol>
                </p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Disposisi</b>
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <p>:&nbsp;<?php echo $dd->disposisi; ?></p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Tujuan</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                  :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <?php $no = 1; foreach ($tujuan_disposisi_dipilih as $t) { ?>
                    <p><?php echo $no++; ?>.&nbsp;<?php echo $t->tujuan ?> </p>
                 <?php } ?>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Isi Disposisi Kepala Badan/Sekretaris</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                  :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <?php echo $dd->uraian_kadis; ?>
              </div>

              <div style="clear: both;"></div>

                <input type="hidden" name="id" id="id">               
                  <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="form-group mb-3">
                            <label for="uraian">Isi Disposisi:</label>
                            <textarea class="form-control" id="summary-ckeditor" name="uraian_kabid" placeholder="Uraian...">
                            <?php foreach($uraian_kabid as $uk){
                              if(isset($uk->uraian_kabid)){
                                echo $uk->uraian_kabid;
                              }
                            } ?>
                            </textarea>
                        </div>
                    </div>   
                  </div>

                  <?php } ?>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                  </div>
                
            </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <script src="<?= base_url('assets/plugins/')?>ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'uraian_kabid' );
    </script>
</main>