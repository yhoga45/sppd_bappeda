    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Ubah Profil Saya</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/profileuser/profil')?>">Profil</a></li>
                <li class="breadcrumb-item active">Ubah Profil Saya</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <form action="<?= base_url('/profileuser/simpan_ubah_profile/'); ?>" method="post" enctype="multipart/form-data">
                  <?php foreach ($profile as $p) { ?>

                    <input type="hidden" name="id_user" id="id_user" value="<?php echo $p->id_user; ?>">

                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  
                      <div class="profile-pic">
                        <img src="<?= base_url('/assets/img/profiluser/'); echo $p->foto_profil; ?>" id="img-profile" width="200">
                        <div class="profile-pic-edit"><a href="#" onclick="clickToBrowseFile()"><i class="fa fa-pencil fa-lg"></i></a></div>
                      </div>
                      <input type="file" name="img_profil_admin" accept="image/png,image/gif,image/jpg,image/jpeg" /> 
                    </div><br>

                    <div class="form-group mb-3" style="width: 50%">
                      <label for="role">Hak Akses:</label>
                      <select class="form-control select2 mb-3" name="role" id="role" disabled>
                        <?php foreach ($role as $r) { ?>

                        <option value="<?php echo $r->role; ?>" disabled hidden selecte><?php echo $r->role; ?></option>

                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group" style="width: 50%">
                      <label for="username">Username:</label>
                      <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo $p->username; ?>" disabled>
                    </div>

                    <div class="form-group" style="width: 50%">
                      <label for="nama">Nama:</label>
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" value="<?php echo $p->nama; ?>">
                    </div>

                    <div class="form-group" style="width: 50%">
                      <label for="nip">NIP:</label>
                      <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" value="<?php echo $p->nip; ?>">
                    </div>

                    <div class="form-group" style="width: 50%">
                      <label for="password">Reset Password:</label>
                      <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>

                  <?php } ?>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Ubah Data</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
