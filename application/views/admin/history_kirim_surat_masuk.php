<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Riwayat Kirim Surat Masuk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
              <li class="breadcrumb-item active">Riwayat Kirim Surat Masuk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">
        <div class="card">
        <div class="card-body">

          <div class="text-center mb-4">
            <h3>Tabel Riwayat Kirim Surat Masuk</h3>
            <hr>
          </div>

            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nomor Surat</th>
                <th>Perihal</th>
                <th>File</th>
                <th>Detail</th>
              </tr>
            </thead>
            <tbody>
            <?php 
            $i = 1;
            foreach($history_kirim as $hs){ ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $hs->no_surat; ?></td>
                <td><?php echo $hs->isi_ringkas; ?></td>
                <td><a href="<?= base_url('/upload/surat_masuk/')?><?php echo ($hs->file); ?>" target="_blank"><?php echo ($hs->file);?></a></td>
                <td>
                  <button type="button" class="btn btn-primary tampilDetailHistory" data-toggle="modal" data-target="#modalDetailHistory" data-id="<?php echo ($hs->id_surat_masuk)?>"><i class="fas fa-compass"></i></button>
                </td>
              </tr>
              <?php $i++;} ?>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nomor Surat</th>
                <th>Perihal</th>
                <th>File</th>
                <th>Detail</th>
              </tr>
            </tfoot>
            </table>
          
          <div style="clear: both;"></div>

          <a href="<?= base_url('suratmasuk')?>" class="btn btn-warning mt-2">
          <i class="fas fa-long-arrow-alt-left"></i> Kembali
      	  </a>
        </div>
        </div>
        </div>
      </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modalDetailHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Perjalanan Surat</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <table id="detailModalHistory" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Pengirim</th>
                <th>Tujuan</th>
                <th>Tanggal</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Pak KaBan</td>
                <td>Pak Sekretaris</td>
                <td>2020-08-08</td>
              </tr>
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>