    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Kirim Surat Masuk</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/suratmasuk')?>">Surat Masuk</a></li>
                <li class="breadcrumb-item active">Kirim Surat Masuk</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-12">
      <div class="card">
      <div class="card-body">

        <div class="col-md-12">
          <div class="text-center mb-4">
            <h3>Detail Surat Masuk</h3>
          </div>
          <?php
              $no = 1;
              foreach($data as $tampil):
          ?>
          <div style="float: left; width: 20%;">
            <b>No. Surat</b>
          </div>
          <div style="float: left; text-align:justify; width: 80%;">
            <p>:&nbsp;<?php echo ($tampil->no_surat); ?></p>
          </div>

          <div style="clear: both;"></div>

          <div style="float: left; width: 20%;">
            <b>Asal Surat</b>
          </div>
            
          <div style="float: left; text-align:justify; width: 80%;">
            <p>:&nbsp;<?php echo ($tampil->asal_surat); ?></p>
          </div>

          <div style="clear: both;"></div>

          <div style="float: left; width: 20%;">
            <b>Isi Ringkas</b>
          </div>

          <div style="float: left; text-align:justify; width: 80%;">
            <p>:&nbsp;<?php echo ($tampil->isi_ringkas); ?></p>
          </div>

          <div style="clear: both;"></div>

          <div style="float: left; width: 20%;">
            <b>Tgl Surat</b>
          </div>

          <div style="float: left; text-align:justify; width: 80%;">
            <p>:&nbsp;<?php echo $this->tgl_indo->tgl_indo($tampil->tgl_surat); ?></p>
          </div>

          <div style="clear: both;"></div>

          <div style="float: left; width: 20%;">
            <b>Tgl Diterima</b>
          </div>
          
          <div style="float: left; text-align:justify; width: 80%;">
            <p>:&nbsp;<?php echo $this->tgl_indo->tgl_indo($tampil->tgl_diterima); ?></p>
          </div>

          <div style="clear: both;"></div>

          <div style="float: left; width: 20%;">
            <b>File</b>
          </div>

          <div style="float: left; text-align:justify; width: 80%;">
            <p>:&nbsp;<a href="<?= base_url('/upload/surat_masuk/')?><?php echo ($tampil->file); ?>" target="_blank"><?php echo ($tampil->file);?></a></p>
          </div>

          <div style="clear: both;"></div>

          <hr>

          <div class="text-center mb-4">
                <h3>Detail Disposisi</h3>
              </div>
              <div style="float: left; width: 22%;">
                <b>Sifat</b>
              </div>
              <div style="float: left; text-align:justify; width: 78%;">
                <p><span>:&nbsp;</span><?php echo $tampil->sifat; ?> </p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Perihal</b>
              </div>
              <div style="float: left; text-align:justify; width: 78%;">
                <p><span>:&nbsp;</span><?php echo $tampil->isi_ringkas; ?></p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Perintah Kirim Surat Kepada</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                 <?php $no = 1; foreach($perintah_kirim as $pk): 
                  if($pk->role == 3){ ?>
                    <p><?php echo $no++; ?>.&nbsp;<?php echo $pk->role.' '.$pk->jenis_instansi; ?></p>
                 <?php } else {
                  ?>
                    <p><?php echo $no++; ?>.&nbsp;<?php echo $pk->role.' '.$pk->keterangan_jabatan; ?></p>
                  <?php } 
                endforeach; ?>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Disposisi</b>
              </div>
              <div style="float: left; text-align:justify; width: 78%;">
                <p><span>:&nbsp;</span><?php echo $tampil->disposisi; ?></p>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Tujuan</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <?php $no = 1; foreach($tujuan_disposisi_dipilih as $tdd): ?>
                  <p><?php echo $no++; ?>.&nbsp;<?php echo $tdd->tujuan; ?></p>
                  <?php endforeach; ?>
              </div>

              <div style="clear: both;"></div>

              <div style="float: left; width: 22%;">
                <b>Isi Disposisi Kepala Badan/Sekretaris</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <?php echo $tampil->uraian_kadis; ?>
              </div>

              <div style="clear: both;"></div>

              <!-- <div style="float: left; width: 22%;">
                <b>Telah diketahui oleh Kepala Dinas</b>
              </div>
              <div style="float: left; text-align:justify; width: 78%;">
                <p><span>:&nbsp;</span><?php 
                if($tampil->diketahui_kadis == 'T'){ 
                  echo 'Belum Diketahui';
                 }else{
                  echo 'Sudah Diketahui';
                 } ?></p>
              </div> -->

              <div style="clear: both;"></div>

              <!-- <div style="float: left; width: 22%;">
              <b>Isi Disposisi Kepala Bidang</b>
              </div>
              <div style="float: left; margin-right: 3px;">
                :
              </div>
              <div style="float: left; text-align:justify; width: 75%;">
                <?php echo $d->uraian_kabid; ?>
              </div> -->
              <div style="clear: both;"></div>
          
        </div>
        <?php if(!check_if_role_is('7') && !check_if_role_is('8')){ ?>   
        <div class="col-md-5 mt-4">
        <form action="<?= base_url('/Suratmasuk/simpan_kirim_user_surat_masuk/'); echo encrypt_url($tampil->id_surat_masuk); ?>" method="post">
          <div class="form-group">
              <label for="kirim_surat_masuk">Kirim Surat Masuk Kepada:</label>
              <div class="select2-blue">
                <select class="select2" multiple="multiple" data-placeholder="Kirim Kepada..." data-dropdown-css-class="select2-blue" id="id_user" name="id_user[]" style="width: 100%;" required>
                    <?php foreach ($user_dipilih as $ud): ?>
                      <option value="<?php echo $ud->id_user; ?>" hidden selected><?php echo $user->nama; ?></option>
                      <option disabled="">=====================================================</option>
                    <?php endforeach; ?>
                    <?php 
                      foreach ($role as $role): 
                      if($role->id == 3){ ?>
                        <option value="<?php echo $role->id_user; ?>"><?php echo $role->nama.' ('.$role->role.' '.$role->jenis_instansi.')'; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $role->id_user; ?>"><?php echo $role->nama.' ('.$role->role.' '.$role->keterangan_jabatan.')';?></option>
                   <?php }
                  endforeach; ?>
                </select>
              </div>
          </div>

          <button type="submit" class="btn btn-primary">Kirim Surat</button>
          <a class="btn btn-danger" href="<?= base_url('suratmasuk')?>">Batal</a>
          
        </div>
        </form>
      </div>
      </div>
      </div>
      </div>
      <?php 
      }
        endforeach; 
      ?>
    </section>
    <br>
    
</main>