<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Surat Keluar</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('/suratkeluar')?>">Surat Keluar</a></li>
              <li class="breadcrumb-item active">Tambah Surat Keluar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-12">
      <div class="card">
      <div class="card-body">
      <form action="<?= base_url('/suratkeluar/tambahSuratKeluar')?>" enctype="multipart/form-data" method="post">

      <input type="hidden" name="id" id="id">
          
      <div class="form-row">
        <div class="form-group col-md-6">

          <div class="form-group mb-3">
            <label for="no_agenda">No Agenda:</label>
            <input type="number" class="form-control" id="no_agenda" name="no_agenda" placeholder="No Agenda" value="" required>
          </div>

          <div class="form-group mb-3">
            <label for="kode_klarifikasi">Kode Klasifikasi:</label>
            <input type="text" class="form-control" id="kode_klasifikasi" name="kode_klasifikasi" placeholder="Kode Klasifikasi" value="" required>
          </div>

          <label for="no_surat">No Surat:</label>
          <div class="form-row">
            <div class="form-group col-md-2">
              <input type="text" class="form-control text-center" id="kode_klasifikasi1" name="kode_klasifikasi1" placeholder="Kode Klasifikasi" value="" readonly>
            </div>
            <div class="form-group col-md-2">
              <input type="text" class="form-control text-center" id="no_surat" name="no_surat" placeholder="<?php echo ($no_surat) ?>" value="<?php echo ($no_surat) ?>" readonly>
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control text-center" id="kode_surat" name="kode_surat" placeholder="Kode Surat" value="" required>
            </div>
            <div class="form-group col-md-2">
              <select class="form-control select2 mb-3" name="bulan_romawi" id="bulan_romawi" required>
                <option value disabled hidden selected>Bulan</option>
                <option value="I">I</option>
                <option value="II">II</option>
                <option value="III">III</option>
                <option value="IV">IV</option>
                <option value="V">V</option>
                <option value="VI">VI</option>
                <option value="VII">VII</option>
                <option value="VIII">VIII</option>
                <option value="IX">IX</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
              </select>
            </div>
            <div class="form-group col-md-2">
              <input type="text" class="form-control text-center" id="tahun" name="tahun" value="<?php echo '/'.date('Y'); ?>" readonly>
            </div>

          </div>

          <div class="form-group mb-3">
            <label for="tujuan_surat">Tujuan Surat Kepada:</label>
            <textarea class="form-control" id="tujuan_surat" rows="4" name="tujuan_surat" placeholder="Tujuan Surat Kepada..." required></textarea>
          </div>

        </div>

        <div class="form-group col-md-6">

          <div class="form-group mb-3">
              <label>Tanggal Surat:</label>
                <div class="input-group date reservationdate" id="reservationdate" data-target-input="nearest">
                  <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_surat" id="tanggal_surat" required />
                  <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
          </div>

          <div class="form-group">
          <label for="file_surat_keluar">File Surat Keluar</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="file_surat_keluar" id="file_surat_keluar">
                <label class="custom-file-label" for="file_surat_keluar">Pilih File</label>
              </div>
            </div>
          </div>

          <div class="form-group">
              <label for="isi_ringkas">Perihal:</label>
              <textarea class="form-control" id="isi_ringkas" rows="4" name="isi_ringkas" placeholder="Perihal..."></textarea>
          </div>

          

        </div>

      </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
   </section>

