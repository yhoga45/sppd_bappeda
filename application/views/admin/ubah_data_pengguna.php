    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Ubah Pengguna</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/pengguna')?>">Kaban</a></li>
                <li class="breadcrumb-item active">Ubah Data Kaban</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
              <?php foreach ($user as $u) { ?>
                <form action="<?= base_url('/pengguna/simpan_ubah_pengguna/'); echo $this->uri->segment(3).'/'.encrypt_url($u->id_role);?>" method="post">
                 
                  <input type="hidden" name="id_user" value="<?php echo $u->id_user; ?>">

                  <div class="form-group" style="width: 50%">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $u->username; ?>" disabled>
                  </div>

                  <div class="form-group mb-3" style="width: 50%">
                    <label for="role">Role:</label>
                      <input type="text" class="form-control" id="role" name="role" value="<?php echo $u->role. ' ' . $u->jenis_instansi; ?>" disabled>
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="nama">Nama:</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $u->nama; ?>" required>
                  </div>

                  <div class="form-group" style="width: 50%">
                    <label for="nama">NIP:</label>
                    <input type="text" class="form-control" id="nip" name="nip" value="<?php echo $u->nip; ?>" required>
                  </div>

                  

                  <div class="form-group" style="width: 50%">
                    <label for="password">Reset Password:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  </div>
                <?php   } ?>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Ubah Data</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
