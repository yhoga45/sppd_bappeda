<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Surat Masuk</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item active">Surat Masuk</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Surat Masuk</h3><br><br>

                <div class="row">
                  <div class="col-lg-6">
                    <?php 
                      if(isset($_SESSION['flash'])){
                        Flasher::flash();
                      } ?>
                  </div>  
                </div>
              <?php if(function_exists('check_if_role_is')){
              if (check_if_role_is('2')){ ?>
              <a class="btn btn-success" href="<?= base_url('/suratmasuk/tambah_surat_masuk'); ?>" role="button" title="Tambah Surat Masuk">
                <i class="fas fa-plus"> Tambah Surat Masuk</i>
              </a>
              <?php } } ?>
              <a class="btn btn-warning" href="<?= base_url('/suratmasuk/history_kirim_surat_masuk/'); echo encrypt_url($this->session->userdata('id_user'));?>" role="button" title="Riwayat Surat Masuk">
                <i class="fas fa-history"> Riwayat Kirim Surat Masuk</i>
              </a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>No Agenda</th>
                  <th>Kode Klasifikasi</th>
                  <th>Perihal</th>
                  <th>File</th>
                  <th>Asal Surat</th>
                  <th>No. Surat</th>
                  <th>Tgl Surat</th>
                  <th>Tgl Diterima</th>
                  <th>Lihat Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php  
                  $no = 1;
                  foreach ($data as $tampil):
                ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo ($tampil->no_agenda);?></td>
                  <td><?php echo ($tampil->kode_klasifikasi);?></td>
                  <td><?php echo ($tampil->isi_ringkas);?></td>
                  <td><a href="<?= base_url('/upload/surat_masuk/')?><?php echo ($tampil->file); ?>" target="_blank"><?php echo ($tampil->file);?></a></td>
                  <td><?php echo ($tampil->asal_surat);?></td>
                  <td><?php echo ($tampil->no_surat);?></td>
                  <td><?php echo ($tampil->tgl_surat);?></td>
                  <td><?php echo ($tampil->tgl_diterima);?></td>
                  <td class="text-center">
                      <a href="<?= base_url('suratmasuk/lihat_status/'); echo encrypt_url($tampil->id_surat_masuk); ?>" class="btn btn-success" title="Lihat Status">
                        <i class="fas fa-eye"></i>
                      </a>
                  </td>
                  <td class="text-center">
                    <?php if($this->cek_cetak_dispo->cek_cetak_dispo($tampil->id_surat_masuk) == true) {?>     
                      <a class="btn btn-primary mb-1" href="<?= base_url('suratmasuk/cek_count_disposisi/'); echo encrypt_url($tampil->id_surat_masuk); ?>" target="_blank" role="button" title="Cetak File Disposisi">
                        <i class="fas fa-print"></i>
                      </a>
                      <?php } else { ?>
                        <button class="btn btn-primary mb-1" disabled><i class="fas fa-print"></i></button>
                      <?php } ?>
                      <?php if(function_exists('check_if_role_is')){
                        if(check_if_role_is('1') || check_if_role_is('2')){
                          if($this->cek_edit_surat->cek_edit_surat($tampil->id_surat_masuk) == true) { ?>     
                            <a class="btn btn-warning mb-1" href="<?= base_url('/suratmasuk/ubah_surat_masuk/'); echo encrypt_url($tampil->id_surat_masuk);?>" role="button" title="Edit">
                              <i class="fas fa-edit"></i>
                            </a>
                    <?php } else { ?>
                            <button class="btn btn-warning mb-1" disabled><i class="fas fa-edit"></i></button>
                    <?php } 
                        } 
                      } ?>
                    <a class="btn btn-success mb-1" href="<?= base_url('/suratmasuk/disposisi/'); echo encrypt_url($tampil->id_surat_masuk);?>" role="button" title="Disposisi">
                      <i class="fas fa-file"></i>
                    </a>
                    <?php if(function_exists('check_if_role_is')){
                    if (check_if_role_is('1') || check_if_role_is('2') || check_if_role_is('3') || check_if_role_is('4')){ ?>
                      <a class="btn btn-success mb-1" href="<?= base_url('suratmasuk/kirim_surat_masuk/'); echo encrypt_url($tampil->id_surat_masuk); ?>" role="button" title="Kirim Surat Masuk">
                        <i class="fas fa-paper-plane"></i>
                      </a>
                    <?php } else if(check_if_role_is('5') || check_if_role_is('6')){
                      if($this->cek_kirim_surat->cek_kirim_surat($tampil->id_surat_masuk) == true) { ?>
                       <a class="btn btn-success mb-1" href="<?= base_url('suratmasuk/kirim_surat_masuk/'); echo encrypt_url($tampil->id_surat_masuk); ?>" role="button" title="Kirim Surat Masuk">
                        <i class="fas fa-paper-plane"></i>
                      </a>
                      <?php } else { ?>
                      <button class="btn btn-success mb-1" disabled title="Disposisi Belum Terisi!">
                        <i class="fas fa-paper-plane"></i>
                      </button>
                    <?php } } } ?>
                    <?php if(function_exists('check_if_role_is')){
                    if (check_if_role_is('1') || check_if_role_is('2')){ ?>
                      <a class="btn btn-danger mb-1 hapus" href="<?= base_url('suratmasuk/hapusSuratMasuk/'); echo encrypt_url($tampil->id_surat_masuk); ?>" role="button" title="Hapus Surat Masuk" >
                        <i class="fas fa-trash"></i>
                      </a>
                    <?php } } ?>
                  </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                      <th>No</th>
                      <th>No Agenda</th>
                      <th>Kode Klasifikasi</th>
                      <th>Perihal</th>
                      <th>File</th>
                      <th>Asal Surat</th>
                      <th>No. Surat</th>
                      <th>Tgl Surat</th>
                      <th>Tgl Diterima</th>
                      <th>Lihat Status</th>
                      <th>Aksi</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>

<!-- The Modal -->
<div class="modal modal-danger fade" id="formModalDelete" role="dialog" aria-labelledby="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
                
    <!-- Modal Header -->
    <div class="modal-header">
      <h5 class="modal-title text-center" id="myModal">Hapus Data Surat Masuk</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span data-feather="x"><span>&times;</span></button>
    </div>
    <!-- Modal body -->
    <div class="modal-body">
        <form action="" method="post">
          <input type="hidden" id="idHapus" name="idHapus" value="">
          <p class="text-center">
            Apakah Anda Yakin ?
          </p>

        </div>
                  
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
        <input type="submit" class="btn btn-danger" name="submit" value="Yes">
        </form>
        </div>
    </div>
  </div>
</div>