  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ubah STL</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/admin')?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('/stl')?>">STL</a></li>
              <li class="breadcrumb-item active">Ubah STL</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-12">
      <div class="card">
      <div class="card-body">
      <form action="<?= base_url('/stl/simpan_ubah_stl/'); echo $this->uri->segment(3); ?>" method="post">

      <input type="hidden" name="id" id="id">
    <?php foreach ($stl as $stl) : ?>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="no_spt">No STL:</label>

          <div class="form-row">
            <div class="form-group col-md-2">
              <input type="text" class="form-control text-center" id="no_stl" name="no_stl" value="094/" disabled>
            </div>

            <div class="form-group col-md-2">
              <input type="text" class="form-control text-center" id="no_urut_stl" name="no_urut_stl" title="Nomor STL" value="<?php echo explode("/", $stl->no_stl )[1] ?>" placeholder="<?php echo explode("/", $stl->no_stl )[1] ?>" readonly>
            </div>

            <div class="form-group col-md-3">
              <input type="text" class="form-control text-center" id="kode_stl" name="kode_stl" value="/STL/BAPPEDA/" disabled>
            </div>

            <div class="form-group col-md-3">
              <div class="form-group mb-3">
                <select class="form-control select2 mb-3" name="bulan_romawi" id="bulan_romawi" required>
                  <option value="<?php echo explode("/",$stl->no_stl)[4]; ?>" hidden selected><?php echo explode("/",$stl->no_stl)[4]; ?> (Data Lama)</option>
                  <option value="I">I</option>
                  <option value="II">II</option>
                  <option value="III">III</option>
                  <option value="IV">IV</option>
                  <option value="V">V</option>
                  <option value="VI">VI</option>
                  <option value="VII">VII</option>
                  <option value="VIII">VIII</option>
                  <option value="IX">IX</option>
                  <option value="X">X</option>
                  <option value="XI">XI</option>
                  <option value="XII">XII</option>
                </select>
              </div>
            </div>

            <div class="form-group col-md-2">
              <input type="text" class="form-control text-center" id="tahun_stl" name="bulan_stl" value="<?php echo date('Y'); ?>" disabled>
            </div>
          </div>

          <div class="form-group mb-3">
              <label>Tanggal STL:</label>
                <div class="input-group date reservationdate" id="reservationdate" data-target-input="nearest">
                  <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="tanggal_stl" id="tanggal_stl" value="<?php echo $stl->tgl_stl; ?>" />
                  <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
          </div>
          
          <label>Tanggal Serta Waktu Tugas Luar:</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="far fa-clock"></i>
              </span>
            </div>
            <input type="text" class="form-control float-right reservation" name="tgl_serta_waktu_tugas_luar" id="reservationtime" value="<?php echo $stl->tgl_mulai. '-'. $stl->tgl_selesai; ?>">
          </div>

          <div class="form-group">
              <label for="lokasi">Lokasi Tugas Luar/Kegiatan:</label>
              <?php if(empty($stl->lokasi)){ ?> 
                <textarea class="form-control" id="lokasi" rows="2" name="lokasi" placeholder="Masukkan Lokasi..."></textarea>
              <?php } else { ?>
                <textarea class="form-control" id="lokasi" rows="2" name="lokasi" placeholder="Masukkan Lokasi..."><?php echo $stl->lokasi; ?> </textarea>
              <?php } ?>
          </div>

        </div>

          <div class="form-group col-md-6">

          <div class="form-group">
              <label for="mpd">Maksud Tugas Luar:</label>
              <?php if(empty($stl->maksud)){ ?> 
                <textarea class="form-control" id="mtl" rows="4" name="mtl" placeholder="Masukkan Penjelasan..."></textarea>
              <?php } else { ?>
                <textarea class="form-control" id="mtl" rows="4" name="mtl" placeholder="Masukkan Penjelasan..."><?php echo $stl->maksud; ?></textarea>
              <?php } ?>
            </div>

            <div class="form-group mb-3">
              <label for="nama_pj_tl">TL Kepada:</label>
                <div class="select2-blue">
                    <select class="select2" multiple="multiple" data-placeholder="Cari PJ TL..." data-dropdown-css-class="select2-blue" id="nama_pj_tl" name="nama_pj_tl[]" style="width: 100%;">
                      <?php foreach ($pegawai_dipilih as $pd) : ?>
                         <option value="<?php echo $pd->id_pegawai; ?>" hidden selected><?php echo $pd->nama_pegawai; ?></option>
                       <?php endforeach; ?>
                      <?php 
                        foreach ($pegawai_stl as $pegawai): ?>
                          <option value="<?php echo $pegawai->id_pegawai ?>"><?php echo $pegawai->nama_pegawai ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
            </div>

            <div class="form-group mb-3">
              <label for="pejabat_stl">Pejabat TTD STL:</label>
                <?php if(empty($stl->id_pejabat_spt)){ ?> 
                  <select class="form-control select2" name="pejabat_stl" id="pejabat_stl">
                    <option value disabled hidden selected>Pilih Pejabat TTD STL</option>
                    <?php foreach ($pejabat_ttd_spt as $pts) : ?>
                      <option value="<?php echo $pts->id; ?>"><?php echo $pts->nama_pegawai; ?></option>
                    <?php endforeach; ?>
                  </select>
                <?php } else { ?>
                <select class="form-control select2" name="pejabat_stl" id="pejabat_stl">
                  <?php foreach ($pejabat_ttd_dipilih as $ptd) : ?>
                        <option value="<?php echo $ptd->id_pejabat_spt; ?>" hidden selected><?php echo $ptd->nama_pegawai; ?></option>
                        <option disabled="">=====================================================</option>
                      <?php endforeach; ?> 
                  <?php foreach ($pejabat_ttd_spt as $pts) : ?>
                    <option value="<?php echo $pts->id; ?>"><?php echo $pts->nama_pegawai; ?></option>
                  <?php endforeach; ?>
                </select>
                <?php } ?>
            </div>
          </div>
      </div>
    <?php endforeach; ?>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Ubah Data</button>
      </div>
        </form>
      </div>
    </div>
  </div>
</div>
   </section>

