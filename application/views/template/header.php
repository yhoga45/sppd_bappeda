<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SPPD BAPPEDA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url('assets/adminlte/')?>plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->

  <!-- SweetAlert -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/sweetalert.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
  
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <script type="text/javascript">
    base_url = '<?php echo base_url(); ?>';
  </script>
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- SEARCH FORM -->
      <!-- <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form> -->

      <!-- Right navbar links -->
      <?php 
          $notif = notif();
        ?>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <div class="dropdown">
          <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration:none">
            <span class="badge badge-danger"><?php echo $notif['count']; ?></span>
            <i class="fas fa-bell" style="font-size:18px"></i>
          </button>
          <div class="dropdown-menu dropdown-menu-right pb-0" aria-labelledby="dropdownMenuButton"  style="max-width: 300px; min-width:300px;">
          <?php
          
          if($notif['count'] == 0){ ?>
            <div class="dropdown-divider"></div>
            <p style="color:gray; font-size:12px; text-align:center;">Tidak ada notifikasi terbaru</p>
         <?php 
          }
          foreach ($notif['notif_lonceng'] as $nt):  
          ?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?= base_url('Admin/ubah_status_baca_notif/'); echo $nt->id_notif.'/'.$nt->id_jenis_surat; ?>" style="color:#1aa3ff ;text-align:center; font-size:13px;"><?php $tgl = explode(' ', $nt->tgl_notif); echo $nt->jenis.', '.$nt->tgl_notif; ?></a>
            <?php
           endforeach; 
           ?>
            <a class="dropdown-item bg-primary mt-2 text-center text-white py-1" style="font-size:12px" href="<?= base_url('admin')?>">Lihat Selengkapnya</a>
          </div>
        </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
          </a>
        </li>
        <li>
          <a class="btn btn-default" href="<?= base_url('Login/logout')?>" role="button">
            <i class="fas fa-power-off"></i>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="<?= base_url('/admin')?>" class="brand-link">
        <img src="<?= base_url('assets/img/')?>logo.png" alt="Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light" style="font-size: 20px">BAPPEDA<br><!--  <span style="padding-left: 18.5%;">PIMPINAN</span> --></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?= base_url('assets/img/profiluser/'.$this->session->userdata('foto_profil'));?>" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="<?= base_url('/profileuser/profile/'); echo encrypt_url($this->session->userdata('id_user'));?>" class="d-block"><?php echo 'Halo, '.explode(" ", $this->session->userdata('nama'))[0]; ?></a>
          </div>
        </div>