<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="<?= base_url('/admin')?>" class="nav-link <?php if($page == 'dashboard') {echo 'active';} ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>

          </li>
          <?php if(function_exists('check_if_role_is')){
            if (check_if_role_is('1') || check_if_role_is('2')){ ?>
              <li class="nav-item has-treeview <?php if($page == 'pegawai' || $page == 'pejabat_ttd') {echo 'menu-open';} ?>">
                <a href="#" class="nav-link <?php if($page == 'pegawai' || $page == 'pejabat_ttd') {echo 'active';} ?>">
                  <i class="nav-icon fas fa-copy"></i>
                  <p>
                    Data Master
                    <i class="fas fa-angle-left right"></i>
                    <!-- <span class="badge badge-info right">6</span> -->
                  </p>
                </a>
                <ul class="nav nav-treeview" style="display: <?php if($page == 'pegawai' || $page == 'pejabat_ttd') {echo 'block';} else {echo 'none';} ?>">
                  <li class="nav-item">
                    <a href="<?= base_url('/pegawai/')?>" class="nav-link <?php if($page == 'pegawai') {echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pegawai</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('/pejabatttd/')?>" class="nav-link <?php if($page == 'pejabat_ttd') {echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pejabat TTD</p>
                    </a>
                  </li>  
                </ul>
              </li> 
        <?php } ?>
          <li class="nav-item">
            <a href="<?= base_url('/spt/')?>" class="nav-link <?php if($page == 'spt_spd') {echo 'active';} ?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>SPT & SPD</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?= base_url('/stl/')?>" class="nav-link <?php if($page == 'stl') {echo 'active';} ?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>STL</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('/suratmasuk/')?>" class="nav-link <?php if($page == 'surat_masuk') {echo 'active';} ?>">
              <i class="nav-icon fas fa-envelope"></i>
              <i class="nav-icon fas fa-arrow-circle-down"></i>
              <p>Surat Masuk</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?= base_url('/suratkeluar/')?>" class="nav-link <?php if($page == 'surat_keluar') {echo 'active';} ?>">
              <i class="nav-icon fas fa-envelope"></i>
              <i class="nav-icon fas fa-arrow-circle-up"></i>
              <p>Surat Keluar</p>
            </a>
          </li>
        <?php if (check_if_role_is('1') || check_if_role_is('2')) { ?>
          <li class="nav-item">
            <a href="<?= base_url('/pengguna/')?>" class="nav-link <?php if($page == 'data_pengguna') {echo 'active';} ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>Data Pengguna</p>
            </a>
          </li>
        <?php } 
      }
      ?>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>