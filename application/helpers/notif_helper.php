<?php
function notif()
{
    
    $CI =& get_instance();
    $CI->load->model('model_surat_keluar');
    $where = [];
    $id_user = $CI->session->userdata('id_user');
    $data['notif_lonceng'] = $CI->model_surat_keluar->get_duatable_order_by('*','tbl_notifikasi','tbl_jenis_surat', 'tbl_notifikasi.id_jenis_surat=tbl_jenis_surat.id', 'status_dibaca="belum" AND id_user='.$id_user, 'tbl_notifikasi.id_notif', 'DESC');
    $data['count'] = $CI->model_surat_keluar->get_count_where('notifikasi', 'status_dibaca="belum" AND id_user='.$id_user)->row()->jumlah;
    return $data;
}

// function ubah_status_baca_notif(){
//     $CI =& get_instance();
//     $CI->load->model('model_surat_keluar');
//     $id_notif = $CI->uri->segment(3);
//     $id_jenis_surat = $CI->uri->segment(4);
//     // var_dump($id_notif);
//     // exit(0);

//     $isi_ubah_status= array(
//         'status_dibaca' => 'sudah',
//     );
//     $CI->model_surat_keluar->update_data('notifikasi', 'id='.$id_notif, $isi_ubah_status);

//     if($id_jenis_surat == 1){
//         redirect('/surat_masuk/','refresh');
//     } elseif($id_jenis_surat == 2){
//         redirect('surat_keluar/surat_tugas/','refresh');
//     } else if($id_jenis_surat == 3){
//         redirect('surat_keluar/surat_umum/','refresh');
//     }
    
// }