<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//login
$route['default_controller'] = 'Admin/login';

// //halaman depan admin
// $route['admin/(:any)'] = 'admin/index/$1';

// //halaman data_master
// $route['admin/data_master/pegawai'] = 'pegawai/index';
// $route['admin/data_master/pegawai/(:any)'] = 'pegawai/$1';

// $route['admin/data_master/pejabat_ttd_spt'] = 'pejabatspt/index';
// $route['admin/data_master/pejabat_ttd_spt/(:any)'] = 'pejabatspt/$1';

// $route['admin/data_master/pejabat_ttd_sppd'] = 'pejabatsppd/index';
// $route['admin/data_master/pejabat_ttd_sppd/(:any)'] = 'pejabatsppd/$1';

// $route['admin/data_master/pejabat_ttd_pptk'] = 'pejabatpptk/index';
// $route['admin/data_master/pejabat_ttd_pptk/(:any)'] = 'pejabatpptk/$1';


// //$route['admin/data_master/(:any)/(:any)'] = '$1/$2';
// //$route['admin/data_master/(:any)/(:any)/(:any)'] = '$1/$2/$3';

// //pegawai
// //$route['admin/data_master/pegawai/cetak'] = 'admin/cetak_pegawai';
// //tambah
// //edit
// //hapus

// //spt
// //halaman spt
// $route['admin/spt'] = 'spt/index';
// $route['admin/spt/(:any)'] = 'spt/$1';

// //$route['admin/spt/tambahSpt'] = 'admin/tambah_spt';
// //$route['admin/spt/ubahSpt'] = 'admin/ubah_spt';
// //hapus
// $route['admin/spt/cetak_spt/(:any)'] = 'admin/cetak_spt/$1';

// //detail_spd
// $route['admin/spt/detail_spd/(:any)'] = 'admin/detail_spd/$1';
// //tambah
// //edit
// //hapus
// $route['admin/spt/detail_spd/cetak/hal_1/(:any)'] = 'admin/cetak_hal1/$1';
// $route['admin/spt/detail_spd/cetak/hal_2/(:any)'] = 'admin/cetak_hal2/$1';

// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;
