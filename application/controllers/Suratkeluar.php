<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratKeluar extends CI_Controller {
	public $tgl_indo = NULL;
	function __construct(){
		parent::__construct();
		$this->tgl_indo = & get_instance();
		$this->load->database();
		$this->load->helper('login');
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
	}

	function index()
	{
		$where = [];
		$limit = [];
		$a['data'] = $this->admin_model->get_order_by('*','surat_keluar', $where, 'id_surat', 'Desc', $limit);

		$data['page'] = 'surat_keluar';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/surat_keluar', $a);
		$this->load->view('template/footer'); 
	}

	function tambah_surat_keluar(){
		$where = [];
		$data['no_surat'] = [];
		$last = $this->admin_model->get_order_by('id_surat','surat_keluar', $where, 'id_surat', 'Desc', 1);
		if (!empty($last)) {
			$last_id = $last[0]->id_surat + 1;
			if($last_id <= 9){
				$data['no_surat'] = '00'.$last_id;
			} elseif ($last_id <= 99) {
				$data['no_surat'] = '0'.$last_id;
			} else{
				$data['no_surat'] = $last_id;
			}
		} else{
			$data['no_surat'] = '001';
		}
		$data['page'] = 'surat_keluar';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/tambah_surat_keluar', $data);
		$this->load->view('template/footer');
	}

	function ubah_surat_keluar($id){
		$id = decrypt_url($id);
		$a['data'] = $this->admin_model->get_tbl_id('surat_keluar', 'id_surat=' .$id)->result_object();
		$no_surat = '';
		foreach ($a['data'] as $tampil) {
			$no_surat = $tampil->no_surat;
		}
		$a['pisah'] = explode('/', $no_surat);
		$data['page'] = 'surat_keluar';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_surat_keluar', $a);
		$this->load->view('template/footer');
	}

	function tambahSuratKeluar(){
		$id_instansi 		= $this->session->userdata('id_instansi');
		$no_agenda			= $this->input->post('no_agenda');
		$kode_klasifikasi	= $this->input->post('kode_klasifikasi');
		$no_surat			= $this->input->post('no_surat');
		$kode_surat			= $this->input->post('kode_surat');
		$bulan_romawi		= $this->input->post('bulan_romawi');
		$isi_ringkas		= $this->input->post('isi_ringkas');
		$file				= $this->uploadFile();
		$tujuan 			= $this->input->post('tujuan_surat');
		$tgl_surat			= $this->input->post('tanggal_surat');
		$no_surat_full		= $kode_klasifikasi.'/'.$no_surat.'/'.$kode_surat.'/'.$bulan_romawi.'/'.date('Y');
		

		$object = array(
			'no_agenda' 		=> $no_agenda,
			'kode_klasifikasi'	=> $kode_klasifikasi,
			'isi_ringkas'  		=> $isi_ringkas,
			'file'   			=> $file,
			'tujuan' 		  	=> $tujuan,
			'no_surat'			=> $no_surat_full,
			'id_instansi'		=> $id_instansi,
			'tgl_surat'  		=> $tgl_surat,
			'insert_date' => date("Y-m-d H:i:s"),
			'author_insert' 	=>	$this->session->userdata('username')
		);

		$cns = $this->admin_model->get_tbl_where('surat_keluar', 'tbl_surat_keluar.no_surat="'.$no_surat_full.'"')->result();
		if(!empty($cns)){
			$this->session->set_flashdata('message1', array('type'=>'error','text'=>'Gagal Menambah Data Dengan Nomor Surat Sama'));
			redirect('suratkeluar','refresh');
		} else {

			$where = [];
			$this->admin_model->tambah('surat_keluar', $object);


			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data surat keluar oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);

			$this->admin_model->tambah('log_user', $isi_data_log);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Surat Keluar'));
			redirect('suratkeluar','refresh');
		}
	}

	function ubahSuratkeluar(){
		$id_surat 			= $this->input->post('id');
		$no_agenda			= $this->input->post('no_agenda');
		$kode_klasifikasi	= $this->input->post('kode_klasifikasi');
		$no_surat			= $this->input->post('no_surat');
		$kode_surat			= $this->input->post('kode_surat');
		$bulan_romawi		= $this->input->post('bulan_romawi');
		$isi_ringkas		= $this->input->post('isi_ringkas');
		if (!empty($_FILES['file_surat_keluar']['name'])) {
			$this->deleteImage($id_surat);
			$file			= $this->uploadFile();
		} else {
			$file			= $this->input->post('file_lama');
		}
		$tujuan 			= $this->input->post('tujuan_surat');
		$tgl_surat			= $this->input->post('tanggal_surat');
		$no_surat_full		= $kode_klasifikasi.'/'.$no_surat.'/'.$kode_surat.'/'.$bulan_romawi.'/'.date('Y');

		$object = array(
			'no_agenda' 		=> $no_agenda,
			'kode_klasifikasi'	=> $kode_klasifikasi,
			'isi_ringkas'  		=> $isi_ringkas,
			'file'   			=> $file,
			'tujuan' 		  	=> $tujuan,
			'no_surat'			=> $no_surat_full,
			'tgl_surat'  		=> $tgl_surat,
			'last_update' => date("Y-m-d H:i:s"),
			'author_update' 	=>	$this->session->userdata('username')
		);

		$cns = $this->admin_model->get_tbl_where('surat_keluar', 'tbl_surat_keluar.no_surat="'.$no_surat_full.'"')->result();
		if(!empty($cns)){
			$id_surat_db = $cns[0]->id_surat;
			if($id_surat == $id_surat_db){
				$this->admin_model->update_data('surat_keluar', 'id_surat='.$id_surat, $object);
				$this->load->library('user_agent');
				$browser = $this->agent->browser();
				$browser_version = $this->agent->version();
				$os = $this->agent->platform();
				$ip_address = $this->input->ip_address();

				// insert log activity
				$isi_data_log = array(
					'tgl_aktivitas' => date("Y-m-d H:i:s"),
					'keterangan' => 'ubah data surat keluar dg id '.$id_surat.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
					'jenis_aktivitas' => 'ubah data',
					'username' => $this->session->userdata('username'),
					'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
				);
				$this->admin_model->tambah('log_user', $isi_data_log);

				$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Surat Keluar'));
				redirect('suratkeluar','refresh');	
			} else {
				$this->session->set_flashdata('message1', array('type'=>'error','text'=>'Gagal Mengubah Data Dengan Nomor Surat Sama'));
			redirect('suratkeluar','refresh');
			}
		} else {
			$this->admin_model->update_data('surat_keluar', 'id_surat='.$id_surat, $object);
			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'ubah data surat keluar dg id '.$id_surat.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'ubah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			$this->admin_model->tambah('log_user', $isi_data_log);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Surat Keluar'));
			redirect('suratkeluar','refresh');
		}
	}

	private function uploadFile(){
		$config['upload_path']          = './upload/surat_keluar/';
		$config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx';
		$config['max_size']             = 25600;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('file_surat_keluar')) {
			return $this->upload->data("file_name");
		}
	}

	private function deleteImage($id){
		$product = $this->admin_model->get_tbl_id('surat_keluar', 'id_surat='.$id)->row();
		if ($product->file != null) {
			$filename = explode(".", $product->file)[0];
			return array_map('unlink', glob(FCPATH."upload/surat_keluar/$filename.*"));
		}
	}


	function hapusSuratKeluar(){
		$id_surat	= $this->input->post('idHapus');

		$data['suratkeluar'] = $this->admin_model->get_tbl_where('surat_keluar', 'id_surat='.$id_surat)->result();
		foreach ($data['suratkeluar'] as $sk) {
			$no_surat = $sk->no_surat;
		}

		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'hapus data surat keluar dg nomor '.$no_surat.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'hapus data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->deleteImage($id_surat);
		$this->admin_model->hapus_data('surat_keluar', 'id_surat='.$id_surat);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data Surat Keluar'));
		redirect('suratkeluar','refresh');
	}


	function disposisi(){
		$id_surat_keluar = $this->uri->segment(3);

		$count_data = 0;
		$data['cek_disposisi'] = $this->admin_model->get_tbl_where('disposisi_keluar', 'id_surat_keluar='.$id_surat_keluar)->result();
		foreach ($data['cek_disposisi'] as $cek) {
			$cek_id_surat = $cek->id_surat_keluar;
			if ($cek_id_surat == $id_surat_keluar) {
				$count_data++;
			}
		}

		// var_dump($count_data);
		// exit(0);
		if ($count_data == 0) {

			$isi_data_disposisi = array (
				'id_surat_keluar' => $id_surat_keluar,
				'insert_date' => date("Y-m-d H:i:s"),
				'author_insert' 	=>	$this->session->userdata('username')
			);
			$this->admin_model->tambah('disposisi_keluar', $isi_data_disposisi);	
		}
		$where = [];
		// $data['disposisi'] = $this->admin_model->get_limatable('*', 'tbl_disposisi', 'tbl_tujuan_disposisi', 'tbl_pejabat_ttd_disposisi', 'tbl_tujuan', 'tbl_pegawai', 'tbl_disposisi.id_pejabat_ttd_disposisi=tbl')
		$data['tujuan'] =  $this->admin_model->get_tbl('tujuan')->result();
		$data['pejabat_disposisi'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd_disposisi', 'tbl_pegawai', 'tbl_pejabat_ttd_disposisi.id_pegawai=tbl_pegawai.id_pegawai', $where, 'tbl_pejabat_ttd_disposisi.id');

		$data['disposisi_dipilih'] =  $this->admin_model->get_tbl_where('disposisi_keluar','id_surat_keluar='.$id_surat_keluar)->result();
		$data['ttd_disposisi_dipilih'] = $this->admin_model->get_tigatable('*', 'tbl_disposisi_keluar', 'tbl_pejabat_ttd_disposisi', 'tbl_pegawai', 'tbl_disposisi_keluar.id_pejabat_ttd_disposisi = tbl_pejabat_ttd_disposisi.id', 'tbl_pejabat_ttd_disposisi.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_disposisi_keluar.id_surat_keluar='.$id_surat_keluar, 'tbl_disposisi_keluar.id_surat_keluar');

		$data['tujuan_disposisi_dipilih'] = $this->admin_model->get_tigatable('*', 'tbl_tujuan_disposisi_keluar', 'tbl_disposisi_keluar', 'tbl_tujuan', 'tbl_tujuan_disposisi_keluar.id_disposisi_keluar = tbl_disposisi_keluar.id_disposisi', 'tbl_tujuan_disposisi_keluar.id_tujuan=tbl_tujuan.id', 'tbl_disposisi_keluar.id_surat_keluar='.$id_surat_keluar, 'tbl_tujuan.id');
		$data['tujuan_disposisi_dipilih_ids'] = [];
		foreach ($data['tujuan_disposisi_dipilih'] as $d) {
			$data['tujuan_disposisi_dipilih_ids'][] = $d->id_tujuan;
		}

		$data['page'] = 'surat_keluar';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/disposisi_keluar', $data);
		$this->load->view('template/footer');
		
	}

	function tambahDisposisi(){
		$this->load->library('upload');
		$id_surat_keluar = $this->uri->segment(3);
		$sifat						= $this->input->post('sifat');
		$perihal					= $this->input->post('perihal');
		$kepada						= $this->input->post('kepada');
		$disposisi					= $this->input->post('disposisi');
		$uraian						= $this->input->post('uraian');
		$id_pejabat_ttd_disposisi	= $this->input->post('pejabat_disposisi');
		$id_tujuan = $this->input->post('tujuan');
		// 		var_dump($id_surat_masuk);
		// // count($id_pengikut_pd);
		// exit(0);

		$data['disposisi_ini'] = $this->admin_model->get_tbl_where('disposisi_keluar', 'id_surat_keluar='.$id_surat_keluar)->result();
		foreach ($data['disposisi_ini'] as $di) {
			$id_disposisi = $di->id_disposisi;
		}

		$isi_update_disposisi = array(
			'id_surat_keluar' 	=>	$id_surat_keluar,
			'sifat' => $sifat,
			'perihal' => $perihal,
			'kepada' => $kepada,
			'disposisi' => $disposisi,
			'uraian' => $uraian,
			'id_pejabat_ttd_disposisi' => $id_pejabat_ttd_disposisi,
			'last_update' => date("Y-m-d H:i:s"),
			'author_update' 	=>	$this->session->userdata('username')

		);

		$this->admin_model->update_data('disposisi_keluar','id_surat_keluar='.$id_surat_keluar, $isi_update_disposisi);


		if ($id_tujuan == NULL) {

			$this->admin_model->hapus_data('tujuan_disposisi_keluar', 'id_disposisi_keluar='.$id_disposisi);

		} else {

			$i=0;
			$cek_kesamaan = 0;
			$data['cek_tujuan_disposisi'] = $this->admin_model->get_tbl_where('tujuan_disposisi_keluar', 'id_disposisi_keluar='.$id_disposisi)->result();
			if(isset($data['cek_tujuan_disposisi'])){
				$this->admin_model->hapus_data('tujuan_disposisi_keluar', 'id_disposisi_keluar='.$id_disposisi);

				//perulangan data array dari inputan multiple
				foreach ($id_tujuan as $it => $tujuan) {
					$isi_data_tujuan = array(
						'id_disposisi_keluar' => $id_disposisi,
						'id_tujuan' => $tujuan
					);
					$this->admin_model->tambah('tujuan_disposisi_keluar', $isi_data_tujuan);
				}
				// }

			} else {

				//perulangan data array dari inputan multiple
				foreach ($id_tujuan as $it => $tujuan) {
					$isi_data_tujuan = array(
						'id_disposisi_keluar' => $id_disposisi,
						'id_tujuan' => $tujuan
					);
					$this->admin_model->tambah('tujuan_disposisi_keluar', $isi_data_tujuan);
				}
			}
		}

		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'ubah data disposisi surat keluar oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'ubah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menyimpan Data Disposisi'));
		redirect('/suratkeluar/','refresh');
	}


	function cetakDisposisi($id){

		$where = [];
		$data['disposisi'] = $this->admin_model->get_duatable('*', 'tbl_surat_keluar', 'tbl_disposisi_keluar', 'tbl_disposisi_keluar.id_surat_keluar=tbl_surat_keluar.id_surat', 'tbl_surat_keluar.id_surat='.$id, 'tbl_surat_keluar.id_surat')[0];
		$data['pejabat_disposisi'] = $this->admin_model->get_limatable('*', 'tbl_disposisi_keluar', 'tbl_pejabat_ttd_disposisi', 'tbl_pegawai', 'tbl_jabatan', 'tbl_golongan', 'tbl_disposisi_keluar.id_pejabat_ttd_disposisi = tbl_pejabat_ttd_disposisi.id', 'tbl_pejabat_ttd_disposisi.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_pegawai.id_jabatan=tbl_jabatan.id_jabatan', 'tbl_pegawai.id_golongan=tbl_golongan.id_golongan', 'tbl_disposisi_keluar.id_surat_keluar='.$id)[0];

		$data['tujuan'] =  $this->admin_model->get_tbl('tujuan')->result();
		$data['tujuan_disposisi_dipilih'] = $this->admin_model->get_tigatable('*', 'tbl_tujuan_disposisi_keluar', 'tbl_disposisi_keluar', 'tbl_tujuan', 'tbl_tujuan_disposisi_keluar.id_disposisi_keluar = tbl_disposisi_keluar.id_disposisi', 'tbl_tujuan_disposisi_keluar.id_tujuan=tbl_tujuan.id', 'tbl_disposisi_keluar.id_surat_keluar='.$id, 'tbl_tujuan.id');
		$data['tujuan_disposisi_dipilih_ids'] = [];
		foreach ($data['tujuan_disposisi_dipilih'] as $d) {
			$data['tujuan_disposisi_dipilih_ids'][] = $d->id_tujuan;
		}


		$mpdf = new \Mpdf\Mpdf();
		$data = $this->load->view('admin/cetak_disposisi_keluar', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('Disposisi_surat_keluar.pdf', 'I');


	}

	public function tgl_indo($tanggal){
		$bulan = array (
			1 =>   	'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('/', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[1] . ' ' . $bulan[ (int)$pecahkan[0] ] . ' ' . $pecahkan[2];
	}


}
