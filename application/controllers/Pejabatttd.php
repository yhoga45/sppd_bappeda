<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PejabatTTD extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
		if(function_exists('check_if_role_is')){
			if (!check_if_role_is('1') && !check_if_role_is('2')){
				echo "<script>alert('Maaf, anda tidak izinkan membuka fitur ini!')</script>";
				redirect(base_url("admin/"));
			}
		}
	}

	function index()
	{
		$where = [];
		$a['golongan'] = $this->admin_model->get_tbl('golongan')->result_object();
		$a['jabatan'] = $this->admin_model->get_tbl('jabatan')->result_object();
		$a['pegawai'] = $this->admin_model->get_tbl('pegawai')->result_object();
		$a['data'] = $this->admin_model->get_empattable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', $where, 'tbl_pejabat_ttd.id');
		$a['cek_ttd_stl'] = $this->admin_model->get_tbl('stl')->result();
		$a['cek_ttd_spt'] = $this->admin_model->get_tbl('spt')->result();
		$a['cek_ttd_spd'] = $this->admin_model->get_tbl('spt')->result();
		$a['cek_ttd_disposisi'] = $this->admin_model->get_tbl('disposisi')->result();

		$data['page'] = 'pejabat_ttd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_master/pejabat_ttd', $a);
		$this->load->view('template/footer');
	}


	function tambahDataPejabatTTD(){
		$id_pegawai	  = $this->input->post('nip');
		$object = array(
			'id_pegawai'   => $id_pegawai
		);
		$this->admin_model->tambah('pejabat_ttd', $object);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Pejabat'));
		redirect('pejabatttd','refresh');
	}

	function hapusPejabat($id){
		$id = decrypt_url($id);
		$this->admin_model->hapus_data('pejabat_ttd', 'id='.$id);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data Pejabat'));
		redirect('pejabatttd','refresh');
	}


	function getById(){
		$id	= $this->input->post('id');
		$data=$this->admin_model->get_empattable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_pejabat_ttd.id='.$id, 'tbl_pejabat_ttd.id');
		echo json_encode($data);		
	}

}
