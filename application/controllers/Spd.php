<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spd extends CI_Controller {
	public $tgl_indo = NULL;
	function __construct(){
		parent::__construct();
		$this->tgl_indo = & get_instance();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
		// if(function_exists('check_if_role_is')){
		// 	if (!check_if_role_is('1') && !check_if_role_is('2')){
		// 		echo "<script>alert('Maaf, anda tidak izinkan membuka fitur ini!')</script>";
		// 		redirect(base_url("admin/"));
		// 	}
		// }
	}

	//halaman detail_spd
	public function detail_spd($id){
		$id = decrypt_url($id);
		$where = array(
			'view_sppd.id_spt' => $id
		);
		//tangkap data dengan id tertentu lalu jadikan parameter di view
		$data['spt'] = $this->admin_model->get_tbl_limit('spt', 'id_spt='.$id, 1)->result();

		$data['spd'] = $this->admin_model->get_tigatable('*', 'view_sppd', 'tbl_pegawai', 'tbl_spt', 'view_sppd.id_pegawai = tbl_pegawai.id_pegawai', 'view_sppd.id_spt=tbl_spt.id_spt', $where, 'view_sppd.id_sppd');
		$data['page'] = 'spt_spd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/detail_spd', $data);
		$this->load->view('template/footer');
	}

	public function tambah_data_spd(){
		$id_spt = decrypt_url($this->uri->segment(3));
		$where=[];

		$data['no_spd'] = [];
		//$last = $this->admin_model->get_order_by('id_sppd','sppd', 'id_spt='.$id_spt, 'id_sppd', 'Desc', 1);
		$last = $this->admin_model->get_view_where('sppd', 'id_spt='.$id_spt)->num_rows();

		if ($last >= 1) {
			$last = $last + 1;
			if($last <= 9){
				$data['no_spd'] = '00'.$last;
			} elseif ($last_id <= 99) {
				$data['no_spd'] = '0'.$last;
			} else{
				$data['no_spd'] = $last;
			}
		} else{
			$data['no_spd'] = '001';
		}

		$data['pegawai_pd'] = $this->admin_model->get_tbl('pegawai')->result();
		//tambahan
		$data['cekdatapegawai'] = $this->admin_model->cek_pegawai('id_spt, id_pegawai', 'sppd', 'pegawai_pengikut', 'id_spt='.$id_spt);
		//tambahan 
		$data['spt_where'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_sumber_anggaran', 'tbl_alat_angkut', 'tbl_spt.id_sumber_anggaran = tbl_sumber_anggaran.id', 'tbl_spt.id_alat_angkut=tbl_alat_angkut.id', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['kota_brgkt'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_brgkt=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['kota_tujuan'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_tujuan=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['ttd_spt'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_spt = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['ttd_sppd'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_sppd = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		// $data['ttd_pptk'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd_pptk', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_pptk = tbl_pejabat_ttd_pptk.id', 'tbl_pejabat_ttd_pptk.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['page'] = 'spt_spd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/tambah_spd', $data);
		$this->load->view('template/footer');
	}

	public function ubah_data_spd(){
		$id_spt = decrypt_url($this->uri->segment(3));
		$id_spd = decrypt_url($this->uri->segment(4));

		$data['sppd'] = $this->admin_model->get_view_where('sppd', 'id_sppd='.$id_spd.' AND '.'id_spt='.$id_spt)->result();

		$data['pegawai_dipilih'] = $this->admin_model->get_duatable('*', 'view_sppd', 'tbl_pegawai', 'view_sppd.id_pegawai=tbl_pegawai.id_pegawai', 'view_sppd.id_sppd='.$id_spd, 'view_sppd.id_sppd');

		$data['pegawai_pengikut'] = $this->admin_model->get_duatable('*', 'tbl_pegawai_pengikut', 'tbl_pegawai', 'tbl_pegawai_pengikut.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_pegawai_pengikut.id_sppd='.$id_spd.' AND '.'tbl_pegawai_pengikut.id_spt='.$id_spt, 'tbl_pegawai_pengikut.id_spt');

		$data['pegawai_pd'] = $this->admin_model->get_tbl('pegawai')->result();
		//tambahan
		$data['cekdatapegawai'] = $this->admin_model->cek_pegawai('id_spt, id_pegawai', 'sppd', 'pegawai_pengikut', 'id_spt='.$id_spt);
		//tambahan 
		$data['spt_where'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_sumber_anggaran', 'tbl_alat_angkut', 'tbl_spt.id_sumber_anggaran = tbl_sumber_anggaran.id', 'tbl_spt.id_alat_angkut=tbl_alat_angkut.id', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['kota_brgkt'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_brgkt=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['kota_tujuan'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_tujuan=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['ttd_spt'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_spt = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		$data['ttd_sppd'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_sppd = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		// $data['ttd_pptk'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd_pptk', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_pptk = tbl_pejabat_ttd_pptk.id', 'tbl_pejabat_ttd_pptk.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		$data['page'] = 'spt_spd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_spd', $data);
		$this->load->view('template/footer');

	}

	public function simpan_data_spd(){ 
		$this->load->library('upload');
		$id_spt =  decrypt_url($this->uri->segment(3));
		$no_spd = $this->input->post('no_spd');
		$no_urut_spd = $this->input->post('no_urut_spd');
		$kode_spd = $this->input->post('kode_spd');
		// var_dump($kode_spd);
		// exit(0);
		$kode_klasifikasi = '094/';
		$bulan_romawi  = $this->input->post('bulan_romawi');
		$ket_spd	= '/SPPD/BAPPEDA/'.$bulan_romawi.'/'.date('Y');
		$id_pegawai_pd = $this->input->post('id_pegawai_pd');
		$id_pengikut_pd = $this->input->post('id_pengikut_pd');
		$id_instansi 		= $this->session->userdata('id_instansi');


		$isi_data_spd = array(
			'kode_klasifikasi'	=>	$kode_klasifikasi,
			'no_spd'	=> $no_urut_spd,
			'ket_spd'	=> $ket_spd,
			'id_pegawai' => $id_pegawai_pd,
			'id_spt' => $id_spt,
			'id_instansi'		=> $id_instansi,
			'insert_date' => date("Y-m-d H:i:s"),
			'author_insert' 	=>	$this->session->userdata('username')

		);

		$this->admin_model->tambah('sppd', $isi_data_spd);

		$data['cek_kaban'] = $this->admin_model->get_tigatable('*', 'tbl_spt','tbl_sppd', 'tbl_pegawai', 'tbl_spt.id_spt=tbl_sppd.id_spt','tbl_sppd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_sppd.id_spt='.$id_spt, 'tbl_sppd.id_sppd');
		foreach($data['cek_kaban'] as $ck){
			$id_jabatan = $ck->id_jabatan;
			$id_spt = $ck->id_spt;
			$no_spt = $ck->no_spt;
		}
		if($id_jabatan == 1){
			$no_spt_br = str_replace("BAPPEDA","SETDA-PB",$no_spt);
			$isi_ubah_spt = array(
				'no_spt' => $no_spt_br,
				'last_update' => date("Y-m-d H:i:s"),
				'author_update' 	=>	$this->session->userdata('username')
	
			);
			$this->admin_model->update_data('spt','id_spt='.$id_spt, $isi_ubah_spt);
		} else {
			$no_spt_br = str_replace("SETDA-PB","BAPPEDA",$no_spt);
			$isi_ubah_spt = array(
				'no_spt' => $no_spt_br,
				'last_update' => date("Y-m-d H:i:s"),
				'author_update' 	=>	$this->session->userdata('username')
	
			);
			$this->admin_model->update_data('spt','id_spt='.$id_spt, $isi_ubah_spt);
		}

 		//proses simpan data
		if (isset($id_pengikut_pd)) {
			$where = [];
			$data['last_spd'] =  $this->admin_model->get_order_by('id_sppd','sppd', $where, 'id_sppd', 'Desc', 1);

			foreach ($data['last_spd'] as $ls) {
				$last_id_spd = $ls->id_sppd;
			}


   			//perulangan data array dari inputan multiple
			$i=0;
			foreach ($id_pengikut_pd as $ip => $pengikut) {
				$isi_data_pengikut = array(
					'id_spt' => $id_spt,
					'id_sppd' => $last_id_spd,
					'id_pegawai' => $pengikut
				);
				$this->admin_model->tambah('pegawai_pengikut', $isi_data_pengikut);
				$i++;
			}
		}


		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'tambah data spd oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'tambah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data SPD'));
		redirect(base_url('/spd/detail_spd/'.encrypt_url($id_spt)));
		
		
	}

	public function simpan_ubah_spd(){
		$this->load->library('upload');
		$id_spt =  decrypt_url($this->uri->segment(3));
		$id_spd =  decrypt_url($this->uri->segment(4));
		$id_pegawai_pd = $this->input->post('id_pegawai_pd');
		$id_pengikut_pd = $this->input->post('id_pengikut_pd');
		// var_dump($id_pengikut_pd);
		// // count($id_pengikut_pd);
		// exit(0);
		$no_urut_spd = $this->input->post('no_urut_spd');
		$bulan_romawi  = $this->input->post('bulan_romawi');
		$kode_klasifikasi = '094/';
		$ket_spd	= '/SPPD/BAPPEDA/'.$bulan_romawi.'/'.date('Y');
		$isi_ubah_spd = array(
			'id_pegawai' => $id_pegawai_pd,
			'kode_klasifikasi'	=>	$kode_klasifikasi,
			'no_spd'	=> $no_urut_spd,
			'ket_spd'	=> $ket_spd,
			'last_update' => date("Y-m-d H:i:s"),
			'author_update' 	=>	$this->session->userdata('username')

		);

		$this->admin_model->update_data('sppd','id_sppd='.$id_spd.' AND id_spt='.$id_spt, $isi_ubah_spd);

		$data['cek_kaban'] = $this->admin_model->get_tigatable('*', 'tbl_spt','tbl_sppd', 'tbl_pegawai', 'tbl_spt.id_spt=tbl_sppd.id_spt','tbl_sppd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_sppd.id_sppd='.$id_spd, 'tbl_sppd.id_sppd');
		foreach($data['cek_kaban'] as $ck){
			$id_jabatan = $ck->id_jabatan;
			$id_spt = $ck->id_spt;
			$no_spt = $ck->no_spt;
		}
		if($id_jabatan == 1){
			$no_spt_br = str_replace("BAPPEDA","SETDA-PB",$no_spt);
			$isi_ubah_spt = array(
				'no_spt' => $no_spt_br,
				'last_update' => date("Y-m-d H:i:s"),
				'author_update' 	=>	$this->session->userdata('username')
	
			);
			$this->admin_model->update_data('spt','id_spt='.$id_spt, $isi_ubah_spt);
		} else {
			$no_spt_br = str_replace("SETDA-PB","BAPPEDA",$no_spt);
			$isi_ubah_spt = array(
				'no_spt' => $no_spt_br,
				'last_update' => date("Y-m-d H:i:s"),
				'author_update' 	=>	$this->session->userdata('username')
	
			);
			$this->admin_model->update_data('spt','id_spt='.$id_spt, $isi_ubah_spt);
		}


		if ($id_pengikut_pd == NULL) {

			$this->admin_model->hapus_data('pegawai_pengikut', 'id_sppd='.$id_spd.' AND id_spt='.$id_spt);

		} else {

			$i=0;
			$cek_kesamaan = 0;
			$data['cek_pengikut'] = $this->admin_model->get_tbl_where('pegawai_pengikut', 'id_sppd='.$id_spd.' AND id_spt='.$id_spt)->result();
			if(isset($data['cek_pengikut'])){
				// foreach ($data['cek_pengikut'] as $cp) {
				// 	if ($id_pengikut_pd[$i] == $cp->id_pegawai) {
				// 		$cek_kesamaan = $cek_kesamaan + 1;
				// 	}
				// 	$i++;
				// }

				// if (count($id_pengikut_pd) != $cek_kesamaan) {
				$this->admin_model->hapus_data('pegawai_pengikut', 'id_sppd='.$id_spd.' AND id_spt='.$id_spt);

					//perulangan data array dari inputan multiple
				foreach ($id_pengikut_pd as $ip => $pengikut) {
					$isi_data_pengikut = array(
						'id_spt' => $id_spt,
						'id_sppd' => $id_spd,
						'id_pegawai' => $pengikut
					);
					$this->admin_model->tambah('pegawai_pengikut', $isi_data_pengikut);
				}
				// }

			} else {
				//perulangan data array dari inputan multiple
				foreach ($id_pengikut_pd as $ip => $pengikut) {
					$isi_data_pengikut = array(
						'id_spt' => $id_spt,
						'id_sppd' => $id_spd,
						'id_pegawai' => $pengikut
					);
					$this->admin_model->tambah('pegawai_pengikut', $isi_data_pengikut);
				}
			}
		}


		$this->load->library('user_agent');

		$browser = $this->agent->browser();

		$browser_version = $this->agent->version();

		$os = $this->agent->platform();

		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'ubah data spd dg id '.$id_spd.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'ubah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data SPD'));
		redirect(base_url('/spd/detail_spd/'.encrypt_url($id_spt)));
		
	}


	function hapusSppd(){
		$id_spt	= decrypt_url($this->uri->segment(3));
		$id_sppd	= decrypt_url($this->uri->segment(4));

		$data['spd'] = $this->admin_model->get_view_where('sppd', 'id_sppd='.$id_sppd)->result();
		foreach ($data['spd'] as $spt) {
			$no_sppd = $spt->no_sppd;
			$no_spd = $spt->no_spd;
		}

		$this->load->library('user_agent');

		$browser = $this->agent->browser();

		$browser_version = $this->agent->version();

		$os = $this->agent->platform();

		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'hapus data spd dg nomor '.$no_sppd.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'hapus data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);
		$this->admin_model->hapus_data('pegawai_pengikut', 'id_sppd='.$id_sppd);
		$this->admin_model->hapus_data('sppd', 'id_sppd='.$id_sppd);	
		$this->db->query("UPDATE tbl_sppd SET no_spd = no_spd - 1 WHERE id_spt = '{$id_spt}' AND no_spd > {$no_spd}");
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data SPPD'));
		redirect(base_url('/spd/detail_spd/'.encrypt_url($id_spt)),'refresh');
	}


	//cetak laporan
	public function cetak_hal1(){


		$id_spt = decrypt_url($this->uri->segment(3));
		$id_spd = $this->uri->segment(4);
		//tangkap data dengan id tertentu lalu jadikan parameter di view

		$data['kota_brgkt'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_brgkt=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];
		$data['kota_tujuan'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_tujuan=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];
		$data['kepada'] = $this->admin_model->get_empattable('*', 'view_sppd', 'tbl_pegawai', 'tbl_jabatan', 'tbl_golongan', 'tbl_pegawai.id_pegawai=view_sppd.id_pegawai', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'view_sppd.id_sppd='.$id_spd, 'view_sppd.id_sppd')[0];

		$data['spt_where'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_sumber_anggaran', 'tbl_alat_angkut', 'tbl_spt.id_sumber_anggaran = tbl_sumber_anggaran.id', 'tbl_spt.id_alat_angkut=tbl_alat_angkut.id', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		$data['pengikut'] = $this->admin_model->get_limatable('*', 'tbl_pegawai_pengikut', 'view_sppd', 'tbl_pegawai', 'tbl_jabatan', 'tbl_golongan', 'tbl_pegawai_pengikut.id_sppd=view_sppd.id_sppd', 'tbl_pegawai_pengikut.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_pegawai_pengikut.id_sppd='.$id_spd, 'tbl_pegawai_pengikut.id_sppd');

		$data['ttd_sppd'] = $this->admin_model->get_limatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pejabat_ttd.id=tbl_spt.id_pejabat_ttd_sppd', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');


		$mpdf = new \Mpdf\Mpdf(['tempDir' => 'application/cache/']);
		$data = $this->load->view('admin/cetak_hal1', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('SPD_hal1.pdf', 'I');
	}

	public function cetak_hal2(){
		$id_spt = decrypt_url($this->uri->segment(3));
		$id_spd = $this->uri->segment(4);
		$data['ttd_sppd'] = $this->admin_model->get_limatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pejabat_ttd.id=tbl_spt.id_pejabat_ttd_sppd', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];

		//tangkap data dengan id tertentu lalu jadikan parameter di view
		$mpdf = new \Mpdf\Mpdf(['tempDir' => 'application/cache/']);
		$data = $this->load->view('admin/cetak_hal2', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('SPD_hal2.pdf', 'I');
	}


	public function tgl_indo($tanggal){
		$bulan = array (
			1 =>   	'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('/', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[1] . ' ' . $bulan[ (int)$pecahkan[0] ] . ' ' . $pecahkan[2];
	}



}
