<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public $tgl_indo = NULL;
	public $tgl_lengkap = NULL;
	function __construct(){
		parent::__construct();
		$this->tgl_indo = & get_instance();
		$this->tgl_lengkap = & get_instance();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->model('model_surat_keluar');
		$this->load->library('form_validation');
	}

	public function login(){
		$this->load->view('admin/login');
	}

	//halaman depan admin
	public function index()
	{

		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}

		$bulan = date('m');
		
		$data['spt'] = $this->admin_model->get_tbl('spt')->num_rows();
		$data['spd'] = $this->admin_model->get_tbl('sppd')->num_rows();
		$data['surat_masuk'] = $this->admin_model->get_tbl('surat_masuk')->num_rows();
		$data['surat_keluar'] = $this->admin_model->get_tbl('surat_keluar')->num_rows();

		$id_user = $this->session->userdata('id_user');
		$where = [];
		$data['notif'] = $this->admin_model->get_duatable_order_by('*', 'tbl_notifikasi', 'tbl_jenis_surat', 'tbl_notifikasi.id_jenis_surat=tbl_jenis_surat.id', 'tbl_notifikasi.id_user='.$id_user, 'tbl_notifikasi.id_notif', 'DESC');
		
		$data['data'] = $this->admin_model->get_empattablelike('*', 'tbl_spt', 'tbl_sppd', 'tbl_pegawai', 'tbl_kota', 'tbl_sppd.id_spt=tbl_spt.id_spt', 'tbl_pegawai.id_pegawai=tbl_sppd.id_pegawai', 'tbl_kota.id_kota=tbl_spt.id_kota_tujuan', 'tbl_spt.tgl_spt', $bulan, 'tbl_spt.id_spt');
		
		$data['page'] = 'dashboard';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/dashboard', $data);
		$this->load->view('template/footer');


	}

	public function ubah_status_baca_notif(){
		$id_notif = $this->uri->segment(3);
		$id_jenis_surat = $this->uri->segment(4);
		// var_dump($id_notif);
		// exit(0);

		$isi_ubah_status= array(
			'status_dibaca' => 'sudah',
		);
		$this->model_surat_keluar->update_data('notifikasi', 'id_notif='.$id_notif, $isi_ubah_status);

		if($id_jenis_surat == 1){
			redirect('/suratmasuk/','refresh');
		}
		
	}

	public function tgl_lengkap($tgl_lgkp){
		return strftime('%e %B %Y %H:%M:%S', strtotime($tgl_lgkp));
	}

}
