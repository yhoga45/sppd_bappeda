<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
		if(function_exists('check_if_role_is')){
			if (!check_if_role_is('1') && !check_if_role_is('2')){
				echo "<script>alert('Maaf, anda tidak izinkan membuka fitur ini!')</script>";
				redirect(base_url("admin/"));
			}
		}
	}

	function index()
	{
		$where = [];
		$a['golongan'] = $this->admin_model->get_tbl('golongan')->result_object();
		$a['jabatan'] = $this->admin_model->get_tbl('jabatan')->result_object();
		$a['data'] = $this->admin_model->get_tigatable('*', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', $where, 'tbl_pegawai.id_pegawai');

		$a['cek_pegawai_pengikut'] = $this->admin_model->get_tbl('pegawai_pengikut')->result();
		$a['cek_ttd'] = $this->admin_model->get_tbl('pejabat_ttd')->result();
		$a['cek_pegawai_stl'] = $this->admin_model->get_tbl('pegawai_stl')->result();
		$a['cek_pegawai_sppd'] = $this->admin_model->get_tbl('sppd')->result();

		$data['page'] = 'pegawai';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_master/pegawai', $a);
		$this->load->view('template/footer');
	}


	function tambahPegawai(){
		$nama_pegawai = $this->input->post('nama');
		$nip 		  = $this->input->post('nip');
		$id_golongan  = $this->input->post('golongan');
		$id_jabatan	  = $this->input->post('jabatan');
		$ket_jabatan  = $this->input->post('keterangan_jabatan');
;
		$object = array(
			'nama_pegawai' => $nama_pegawai,
			'nip'		   => $nip,
			'id_golongan'  => $id_golongan,
			'id_jabatan'   => $id_jabatan,
			'ket_jabatan'  => $ket_jabatan
		);
		$this->admin_model->tambah('pegawai', $object);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Pegawai'));
		redirect('pegawai','refresh');
	}

	function ubahPegawai(){
		$id_pegawai   = $this->input->post('id');
		$nama_pegawai = $this->input->post('nama');
		$nip 		  = $this->input->post('nip');
		$id_golongan  = $this->input->post('golongan');
		$id_jabatan	  = $this->input->post('jabatan');
		$ket_jabatan  = $this->input->post('keterangan_jabatan');

		$object = array(
			'nama_pegawai' => $nama_pegawai,
			'nip'		   => $nip,
			'id_golongan'  => $id_golongan,
			'id_jabatan'   => $id_jabatan,
			'ket_jabatan'  => $ket_jabatan
		);
		$this->admin_model->update_data('pegawai', 'id_pegawai='.$id_pegawai, $object);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Pegawai'));
		redirect('pegawai','refresh');
	}

	function hapusPegawai($id){
		$id_pegawai	= decrypt_url($id);
		$this->admin_model->hapus_data('pegawai', 'id_pegawai='.$id_pegawai);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data Pegawai'));
		redirect('pegawai','refresh');
	}


	function getById(){
		$id_pegawai	= $this->input->post('id');
		$data=$this->admin_model->get_tigatable('*', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_pegawai.id_pegawai='.$id_pegawai, 'tbl_pegawai.id_pegawai');
        echo json_encode($data);		
	}

	//pegawai
	public function cetak(){
		$where = [];
		$a['golongan'] = $this->admin_model->get_tbl('golongan')->result_object();
		$a['jabatan'] = $this->admin_model->get_tbl('jabatan')->result_object();
		$a['data'] = $this->admin_model->get_tigatable('*', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', $where, 'tbl_pegawai.id_pegawai');
		$nama = 'Cek.pdf';

		$mpdf = new \Mpdf\Mpdf(['tempDir' => 'application/cache/']);
		$data = $this->load->view('admin/data_master/cetak_pegawai', $a, TRUE);
		var_dump($data);
		// $mpdf->WriteHTML($data);
		// $mpdf->Output('./assets/'.$nama, \Mpdf\Output\Destination::FILE);
	}
}
