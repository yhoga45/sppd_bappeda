<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratMasuk extends CI_Controller {
	public $tgl_indo = NULL;
	public $tgl_lengkap = NULL;
	public $cek_cetak_dispo = false;
	public $cek_edit_surat = false;
	public $cek_kirim_surat = false;
	function __construct(){
		parent::__construct();
		$this->tgl_indo = & get_instance();
		$this->tgl_lengkap = & get_instance();
		$this->cek_cetak_dispo = & get_instance();
		$this->cek_edit_surat = & get_instance();
		$this->cek_kirim_surat = & get_instance();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->model('model_surat_masuk');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
	}

	function index()
	{
		// $where = [];
		// $limit = [];
		// $a['data'] = $this->admin_model->get_order_by('*','surat_masuk', $where, 'id_surat_masuk', 'Desc', $limit);

		$id_user = $this->session->userdata('id_user');
		$id_role = $this->session->userdata('id_role');
		$id_instansi = $this->session->userdata('id_instansi');
		$where = array();
		if($id_role == 2 || $id_role == 1){
			$a['data'] = $this->model_surat_masuk->get_order_by('tbl_surat_masuk', 'id_instansi='.$id_instansi, 'id_surat_masuk', 'DESC');
			// $data['data'] = $this->model_surat_masuk->get_limatable('*', 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', $where , 'tbl_surat_masuk.id_surat_masuk');
		} else {
			$a['data'] = $this->model_surat_masuk->get_limatable_order_by('*', 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_kirim_surat_masuk.id_user='.$id_user , 'tbl_surat_masuk.id_surat_masuk', 'tbl_surat_masuk.id_surat_masuk');
		}

		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/surat_masuk', $a);
		$this->load->view('template/footer'); 
	}

	function tambah_surat_masuk(){
		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/tambah_surat_masuk');
		$this->load->view('template/footer');
	}

	function lihat_status($id){
		$id_user = $this->session->userdata('id_user');
		$id_role = $this->session->userdata('id_role');
		$id_surat_masuk = decrypt_url($this->uri->segment(3));
		$where = array();
		if($id_role == 2 || $id_role == 1){
			$data['status_posisi'] = $this->model_surat_masuk->get_delapantable('user_penerima.nama as nama_penerima, tbl_kirim_surat_masuk.tgl_kirim, user_pengirim.nama as nama_pengirim' , 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user user_penerima', 'tbl_role role_penerima', 'tbl_instansi instansi_penerima',  'tbl_user user_pengirim', 'tbl_role role_pengirim', 'tbl_instansi instansi_pengirim', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = user_penerima.id_user', 'user_penerima.id_role=role_penerima.id', 'user_penerima.id_instansi=instansi_penerima.id_instansi', 'tbl_kirim_surat_masuk.user_pengirim = user_pengirim.id_user', 'user_pengirim.id_role=role_pengirim.id', 'user_pengirim.id_instansi=instansi_pengirim.id_instansi', 'tbl_surat_masuk.id_surat_masuk='.$id_surat_masuk , 'tbl_surat_masuk.id_surat_masuk', 'ASC');
			$data['surat_masuk'] = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk)->result();
			// $data['status_posisi'] = $this->model_surat_masuk->get_limatable('*', 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_surat_masuk.id_surat_masuk='.$id_surat_masuk, 'tbl_surat_masuk.id_surat_masuk');
		} else {
			$data['surat_masuk'] = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk)->result();
			$data['status_posisi'] = $this->model_surat_masuk->get_delapantable('user_penerima.nama as nama_penerima, tbl_kirim_surat_masuk.tgl_kirim, user_pengirim.nama as nama_pengirim' , 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user user_penerima', 'tbl_role role_penerima', 'tbl_instansi instansi_penerima',  'tbl_user user_pengirim', 'tbl_role role_pengirim', 'tbl_instansi instansi_pengirim', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = user_penerima.id_user', 'user_penerima.id_role=role_penerima.id', 'user_penerima.id_instansi=instansi_penerima.id_instansi', 'tbl_kirim_surat_masuk.user_pengirim = user_pengirim.id_user', 'user_pengirim.id_role=role_pengirim.id', 'user_pengirim.id_instansi=instansi_pengirim.id_instansi', 'tbl_surat_masuk.id_surat_masuk='.$id_surat_masuk.' AND tbl_kirim_surat_masuk.id_user='.$id_user , 'tbl_surat_masuk.id_surat_masuk', 'ASC');
		}

		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/lihat_surat_masuk', $data);
		$this->load->view('template/footer');
	}

	function ubah_surat_masuk($id){
		$id = decrypt_url($id);
		$a['data'] = $this->admin_model->get_tbl_id('surat_masuk', 'id_surat_masuk=' .$id)->result_object();
		$no_surat = '';
		foreach ($a['data'] as $tampil) {
			$no_surat = $tampil->no_surat;
		}
		$a['pisah'] = explode('/', $no_surat);
		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_surat_masuk', $a);
		$this->load->view('template/footer');
	}

	function tambahSuratMasuk(){
		$no_agenda			= $this->input->post('no_agenda');
		$kode_klasifikasi	= $this->input->post('kode_klasifikasi');
		$isi_ringkas		= $this->input->post('isi_ringkas');
		$file				= $this->uploadFile();
		$asal_surat			= $this->input->post('asal_surat');
		$no_surat			= $this->input->post('no_surat');
		$tgl_surat			= $this->input->post('tanggal_surat');
		$tgl_diterima		= $this->input->post('tanggal_diterima');
		// $id_surat_masuk		= $this->input->post('id_surat_masuk');
		$no_surat_full		= $kode_klasifikasi.'/'.$no_surat;
		$id_instansi 		= $this->session->userdata('id_instansi');
		$id_role 			= $this->session->userdata('id_role');
		$id_user 			= $this->session->userdata('id_user');

		$object = array(
			'no_agenda'			=> $no_agenda,
			'kode_klasifikasi'	=> $kode_klasifikasi,
			'isi_ringkas'  		=> $isi_ringkas,
			'file'   			=> $file,
			'asal_surat'  		=> $asal_surat,
			'no_surat'			=> $no_surat_full,
			'tgl_surat'  		=> $tgl_surat,
			'id_instansi'		=> $id_instansi,
			'tgl_diterima'   	=> $tgl_diterima,
			'insert_date' => date("Y-m-d H:i:s"),
			'author_insert' 	=>	$this->session->userdata('username')
		);

		$cns = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'tbl_surat_masuk.no_surat="'.$no_surat_full.'"')->result();
		if(!empty($cns)){
			$this->session->set_flashdata('message1', array('type'=>'error','text'=>'Gagal Menambah Data Dengan Nomor Surat Sama'));
			redirect('/suratmasuk','refresh');
		} else {
			$this->model_surat_masuk->insert('tbl_surat_masuk', $object);
			
			$data['ambil_id_surat'] = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'isi_ringkas = "'.$isi_ringkas.'" AND asal_surat = "'.$asal_surat. '" AND no_surat = "'.$no_surat_full. '" AND author_insert= "'.$this->session->userdata('username').'"')->result();
			foreach($data['ambil_id_surat'] as $ais){
				$id_surat_masuk = $ais->id_surat_masuk;

			}
			// var_dump($data['ambil_id_surat']);
			// exit(0);

			$isi_user_pembuat = array(
				'id_user' => $id_user,
				'id_surat_masuk' => $id_surat_masuk,
				'tgl_kirim' => date("Y-m-d H:i:s"),
				'user_pengirim' => $this->session->userdata('id_user')
			);
			$this->model_surat_masuk->insert('tbl_kirim_surat_masuk', $isi_user_pembuat);

			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data surat masuk oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			
			$this->model_surat_masuk->insert('tbl_log_user', $isi_data_log);

			// foreach ($data['last_surat_masuk'] as $ls) {
			// 	$last_id_surat = $ls->id_surat_masuk;
			// }

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Surat Masuk'));
			redirect('/suratmasuk','refresh');
		}
	}

	function history_kirim_surat_masuk(){
		$id_user = decrypt_url($this->uri->segment(3));
		$id_role = $this->session->userdata('id_role');
		$where   = [];
		if($id_role == 1 || $id_role == 2){
			$data['history_kirim'] = $this->model_surat_masuk->get_distinct_tigatable('tbl_history_kirim_surat_masuk.id_surat_masuk', 'tbl_surat_masuk.*', 'tbl_history_kirim_surat_masuk', 'tbl_surat_masuk', 'tbl_user', 'tbl_history_kirim_surat_masuk.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_history_kirim_surat_masuk.id_user_pengirim=tbl_user.id_user', $where, 'tbl_history_kirim_surat_masuk.id_surat_masuk');	
		} else {
			$data['history_kirim'] = $this->model_surat_masuk->get_distinct_tigatable('tbl_history_kirim_surat_masuk.id_surat_masuk', 'tbl_surat_masuk.*', 'tbl_history_kirim_surat_masuk', 'tbl_surat_masuk', 'tbl_user', 'tbl_history_kirim_surat_masuk.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_history_kirim_surat_masuk.id_user_pengirim=tbl_user.id_user', 'tbl_history_kirim_surat_masuk.id_user_pengirim ="'. $id_user.'"', 'tbl_history_kirim_surat_masuk.id_surat_masuk');
		}
		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/history_kirim_surat_masuk', $data);
		$this->load->view('template/footer');
	}

	function ubahSuratMasuk(){
		$id_surat_masuk		= $this->input->post('id');
		$no_agenda			= $this->input->post('no_agenda');
		$kode_klasifikasi	= $this->input->post('kode_klasifikasi');
		$isi_ringkas		= $this->input->post('isi_ringkas');
		if (!empty($_FILES['file_surat_masuk']['name'])) {
			$this->deleteImage($id_surat_masuk);
			$file			= $this->uploadFile();
		} else {
			$file			= $this->input->post('file_lama');
		}
		$asal_surat			= $this->input->post('asal_surat');
		$no_surat			= $this->input->post('no_surat');
		$tgl_surat			= $this->input->post('tanggal_surat');
		$tgl_diterima		= $this->input->post('tanggal_diterima');
		$no_surat_full		= $kode_klasifikasi.'/'.$no_surat;

		$object = array(
			'no_agenda' 		=> $no_agenda,
			'kode_klasifikasi'	=> $kode_klasifikasi,
			'isi_ringkas'  		=> $isi_ringkas,
			'file'   			=> $file,
			'asal_surat'  		=> $asal_surat,
			'no_surat'			=> $no_surat_full,
			'tgl_surat'  		=> $tgl_surat,
			'tgl_diterima'   	=> $tgl_diterima,
			'last_update' 		=> date("Y-m-d H:i:s"),
			'author_update' 	=>	$this->session->userdata('username')
		);

		$cns = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'tbl_surat_masuk.no_surat="'.$no_surat_full.'"')->result();
		if(!empty($cns)){
			$id_surat_db = $cns[0]->id_surat_masuk;
			if($id_surat_masuk == $id_surat_db){
				$this->model_surat_masuk->update('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk, $object);
				$this->load->library('user_agent');
				$browser = $this->agent->browser();
				$browser_version = $this->agent->version();
				$os = $this->agent->platform();
				$ip_address = $this->input->ip_address();

				// insert log activity
				$isi_data_log = array(
					'tgl_aktivitas' => date("Y-m-d H:i:s"),
					'keterangan' => 'ubah data surat masuk dg id '.$id_surat_masuk.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
					'jenis_aktivitas' => 'ubah data',
					'username' => $this->session->userdata('username'),
					'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
				);
				$this->admin_model->tambah('log_user', $isi_data_log);
				$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Surat Masuk'));
				redirect('/suratmasuk','refresh');
		
			} else {
				$this->session->set_flashdata('message1', array('type'=>'error','text'=>'Gagal Mengubah Data Dengan Nomor Surat Sama'));
				redirect('/suratmasuk','refresh');
			}
			
		} else {
			$this->model_surat_masuk->update('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk, $object);

			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'ubah data surat masuk dg id '.$id_surat_masuk.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'ubah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			$this->admin_model->tambah('log_user', $isi_data_log);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Surat Masuk'));
			redirect('/suratmasuk','refresh');
		}
	}

	private function uploadFile(){
		$config['upload_path']          = './upload/surat_masuk/';
		$config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx';
		$config['max_size']             = 25600;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('file_surat_masuk')) {
			return $this->upload->data("file_name");
		}
	}

	private function deleteImage($id){
		$product = $this->admin_model->get_tbl_id('surat_masuk', 'id_surat_masuk='.$id)->row();
		if ($product->file != null) {
			$filename = explode(".", $product->file)[0];
			return array_map('unlink', glob(FCPATH."upload/surat_masuk/$filename.*"));
		}
	}


	function hapusSuratMasuk($id){
		$id_surat_masuk = decrypt_url($id);

		$data['suratmasuk'] = $this->admin_model->get_tbl_where('surat_masuk', 'id_surat_masuk='.$id_surat_masuk)->result();
		foreach ($data['suratmasuk'] as $sm) {
			$no_surat = $sm->no_surat;
		}

		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();
		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'hapus data surat masuk dg nomor '.$no_surat.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'hapus data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->deleteImage($id_surat_masuk);
		
		$data['tujuan_disposisi'] = $this->admin_model->get_tbl_where('disposisi', 'id_surat_masuk='.$id_surat_masuk)->result();
		foreach ($data['tujuan_disposisi'] as $td) {
			$id_disposisi = $td->id_disposisi;
		}

		if(!empty($id_disposisi)){
			$this->admin_model->hapus_data('tujuan_disposisi', 'id_disposisi='.$id_disposisi);
			$this->admin_model->hapus_data('disposisi', 'id_surat_masuk='.$id_surat_masuk);
			$this->admin_model->hapus_data('disposisi_perintah_kirim_surat_masuk', 'id_disposisi='.$id_disposisi);
			$this->admin_model->hapus_data('disposisi_uraian_kabid', 'id_disposisi='.$id_disposisi);
			$this->admin_model->hapus_data('history_kirim_surat_masuk', 'id_surat_masuk='.$id_surat_masuk);
			$this->admin_model->hapus_data('kirim_surat_masuk', 'id_surat_masuk='.$id_surat_masuk);
			$this->admin_model->hapus_data('notifikasi', 'id_surat='.$id_surat_masuk.' AND id_jenis_surat=1');
			$this->admin_model->hapus_data('surat_masuk', 'id_surat_masuk='.$id_surat_masuk);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data Surat Masuk'));
			redirect('suratmasuk','refresh');
		} else {
			$this->admin_model->hapus_data('kirim_surat_masuk', 'id_surat_masuk='.$id_surat_masuk);
			$this->admin_model->hapus_data('surat_masuk', 'id_surat_masuk='.$id_surat_masuk);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data Surat Masuk'));
			redirect('suratmasuk','refresh');
		}
		
	}


    public function disposisi($id){
		$id_surat_masuk = decrypt_url($id);
		$count_data = 0;
		$id_role = $this->session->userdata('id_role');
		$id_user = $this->session->userdata('id_user');
		$parent_id_sub_user = $this->session->userdata('parent_id_sub_user');

		$data['cek_disposisi'] = $this->model_surat_masuk->get_where('tbl_disposisi', 'id_surat_masuk='.$id_surat_masuk)->result();
		
		foreach ($data['cek_disposisi'] as $cek) {
			$cek_id_surat = $cek->id_surat_masuk;
			$id_disposisi = $cek->id_disposisi;
			if ($cek_id_surat == $id_surat_masuk) {
				$count_data++;
			}
		}

		// var_dump($count_data);
		// exit(0);

		//MASIH BUG BELUM NEMU SOLUSI
		if ($count_data == 0) {
			$isi_data_disposisi = array (
				'id_surat_masuk' => $id_surat_masuk,
				'author_insert'	=> $this->session->userdata('username'),
				'insert_date' => date("Y-m-d H:i:s"),
			);
			$this->model_surat_masuk->insert('tbl_disposisi', $isi_data_disposisi);	
		}

		// $data['user'] = $this->model_surat_masuk->get_where('tbl_user', 'id != 1 AND id !='. $id_user)->result();
		$data['user'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_role.id != 1 AND tbl_role.id != 7 AND tbl_role.id != 8 AND tbl_user.id_user !='. $id_user, 'tbl_user.id_role');

		$where = [];
		$data['tujuan'] =  $this->model_surat_masuk->get('tbl_tujuan', 'id', 'ASC');
		$data['pejabat_disposisi'] = $this->model_surat_masuk->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where, 'tbl_pejabat_ttd.id');

		$data['disposisi_dipilih'] =  $this->model_surat_masuk->get_duatable('*', 'tbl_disposisi', 'tbl_surat_masuk', 'tbl_disposisi.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_disposisi.id_surat_masuk='.$id_surat_masuk, 'tbl_disposisi.id_disposisi');
		$data['ttd_disposisi_dipilih'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_disposisi', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_disposisi.id_pejabat_ttd_disposisi = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_disposisi.id_surat_masuk='.$id_surat_masuk, 'tbl_disposisi.id_surat_masuk');
		$data['tujuan_disposisi_dipilih'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_tujuan_disposisi', 'tbl_disposisi', 'tbl_tujuan', 'tbl_tujuan_disposisi.id_disposisi = tbl_disposisi.id_disposisi', 'tbl_tujuan_disposisi.id_tujuan=tbl_tujuan.id', 'tbl_disposisi.id_surat_masuk='.$id_surat_masuk, 'tbl_tujuan.id');
		if($id_role != 2){
		$data['user_dipilih'] = $this->model_surat_masuk->get_empattable('*', 'tbl_disposisi_perintah_kirim_surat_masuk', 'tbl_disposisi', 'tbl_user', 'tbl_role', 'tbl_disposisi_perintah_kirim_surat_masuk.id_disposisi = tbl_disposisi.id_disposisi', 'tbl_disposisi_perintah_kirim_surat_masuk.id_user=tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_disposisi_perintah_kirim_surat_masuk.id_disposisi='.$id_disposisi, 'tbl_user.id_role');
		}
		$data['tujuan_disposisi_dipilih_ids'] = [];
		foreach ($data['tujuan_disposisi_dipilih'] as $d) {
			$data['tujuan_disposisi_dipilih_ids'][] = $d->id_tujuan;
		}

		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		if($id_role == '1' || $id_role == '3' || $id_role == '4'){
			$this->load->view('admin/disposisi', $data);
		} else if($id_role == '2'){
			$this->load->view('admin/disposisi_sekretariat', $data);
		} else if($id_role == '5' || $id_role == '6'){
			$data['uraian_kabid'] =  $this->model_surat_masuk->get_where('tbl_disposisi_uraian_kabid', 'id_disposisi= "'.$id_disposisi.'" AND id_user="'.$id_user.'"')->result();
			// $data['uraian_kabid'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi', 'tbl_user', 'tbl_disposisi_uraian_kabid.id_disposisi = tbl_disposisi.id_disposisi', 'tbl_disposisi_uraian_kabid.id_user=tbl_user.id_user', 'tbl_disposisi_uraian_kabid.id_disposisi="'.$id_disposisi.'" AND tbl_disposisi_uraian_kabid.id_user="'.$id_user.'"', 'tbl_disposisi_uraian_kabid.id_disposisi');
			$this->load->view('admin/disposisi_kabid', $data);
		} else if($id_role == '7' || $id_role == '8'){
			$data['uraian_kabid'] =  $this->model_surat_masuk->get_where('tbl_disposisi_uraian_kabid', 'id_disposisi= "'.$id_disposisi.'" AND id_user="'.$parent_id_sub_user.'"')->result()[0];
			$data['kasi_dipilih'] = $this->model_surat_masuk->get_empattable('*', 'tbl_history_kirim_surat_masuk', 'tbl_disposisi', 'tbl_user', 'tbl_role', 'tbl_history_kirim_surat_masuk.id_surat_masuk = tbl_disposisi.id_surat_masuk', 'tbl_history_kirim_surat_masuk.id_user_tujuan=tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_disposisi.id_disposisi='.$id_disposisi.' AND tbl_history_kirim_surat_masuk.id_user_pengirim='.$parent_id_sub_user, 'tbl_user.id_role');
		
			$this->load->view('admin/disposisi_seksi', $data);
		}
	$this->load->view('template/footer');
	}


	function kirim_surat_masuk($id){
		$id_surat_masuk = decrypt_url($id);
		$id_user = $this->session->userdata('id_user');
		$id_role = $this->session->userdata('id_role');
		$id_instansi = $this->session->userdata('id_instansi');
		$parent_id_user = $this->session->userdata('parent_id_user');
		$parent_id_sub_user = $this->session->userdata('parent_id_sub_user');
		// $id_jabatan = $this->session->userdata('id_jabatan');
		$where = array();
		// var_dump($id_jabatan);
		// exit(0);
		$data['data'] = $this->model_surat_masuk->get_duatable('*', 'tbl_surat_masuk', 'tbl_disposisi', 'tbl_surat_masuk.id_surat_masuk=tbl_disposisi.id_surat_masuk', 'tbl_surat_masuk.id_surat_masuk='.$id_surat_masuk, 'tbl_surat_masuk.id_surat_masuk');
		$data['disposisi'] = $this->model_surat_masuk->get_where('tbl_disposisi', 'id_surat_masuk='. $id_surat_masuk)->result();
		$data['ttd_disposisi_dipilih'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_disposisi', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_disposisi.id_pejabat_ttd_disposisi = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_disposisi.id_surat_masuk='.$id_surat_masuk, 'tbl_disposisi.id_surat_masuk');
		$data['tujuan_disposisi_dipilih'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_tujuan_disposisi', 'tbl_disposisi', 'tbl_tujuan', 'tbl_tujuan_disposisi.id_disposisi = tbl_disposisi.id_disposisi', 'tbl_tujuan_disposisi.id_tujuan=tbl_tujuan.id', 'tbl_disposisi.id_surat_masuk='.$id_surat_masuk, 'tbl_tujuan.id');
		$data['perintah_kirim'] = $this->model_surat_masuk->get_limatable('*','tbl_disposisi', 'tbl_disposisi_perintah_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_disposisi.id_disposisi=tbl_disposisi_perintah_kirim_surat_masuk.id_disposisi', 'tbl_disposisi_perintah_kirim_surat_masuk.id_user=tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_disposisi.id_surat_masuk='. $id_surat_masuk, 'tbl_disposisi.id_surat_masuk');
		if($id_role == 2){
			$data['role'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_role.id = 4 AND tbl_user.id_instansi='.$id_instansi, 'tbl_user.id_role');
		} elseif($id_role == 3 || $id_role == 4){
			$data['role'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_role.id != 1 AND tbl_role.id != 2 AND tbl_user.id_role !='. $id_role. ' AND tbl_user.id_instansi='.$id_instansi.' AND tbl_user.parent_id_sub_user ='.NULL, 'tbl_user.id_role');
		} elseif($id_role == 5 || $id_role == 6){
			$data['role'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_role.id != 1 AND tbl_user.id_role !='. $id_role. ' AND tbl_user.id_instansi='.$id_instansi.' AND tbl_user.parent_id_sub_user ='.$id_user, 'tbl_user.id_role');
		}

		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/kirim_surat_masuk', $data);
		$this->load->view('template/footer');
	}

	public function tambahDisposisi(){
		$id_role = $this->session->userdata('id_role');
		$id_user = $this->session->userdata('id_user');

		$this->load->library('upload');
		$id_surat_masuk 			= decrypt_url($this->uri->segment(3));
		$sifat						= $this->input->post('sifat');
		//$perihal					= $this->input->post('perihal');
		$kepada						= $this->input->post('kepada');
		$disposisi					= $this->input->post('disposisi');
		$uraian_kadis				= $this->input->post('uraian');
		$uraian_kabid				= $this->input->post('uraian_kabid');

		$id_pejabat_ttd_disposisi	= $this->input->post('pejabat_disposisi');
		$id_tujuan 					= $this->input->post('tujuan');
		//$diketahui_kadis			= $this->input->post('diketahui');
		$id_user_kirim				= $this->input->post('perintah_kirim_surat');

		$data['disposisi_ini']	= $this->model_surat_masuk->get_where('tbl_disposisi', 'id_surat_masuk='.$id_surat_masuk)->result();
		foreach ($data['disposisi_ini'] as $di) {
			$id_disposisi = $di->id_disposisi;
		}

		//untuk hapus data sebelumnya
		$data['cek_perintah_kirim']	= $this->model_surat_masuk->get_where('tbl_disposisi_perintah_kirim_surat_masuk', 'id_disposisi='.$id_disposisi)->result();

			if($id_role == '1' || $id_role == '3' || $id_role == '4'){

				if($id_user_kirim ==  NULL){
					foreach($data['cek_perintah_kirim'] as $cpk){
						$this->model_surat_masuk->delete('tbl_disposisi_perintah_kirim_surat_masuk', 'id_disposisi='.$id_disposisi);
					}
				} else {
						foreach($data['cek_perintah_kirim'] as $cpk){
							$this->model_surat_masuk->delete('tbl_disposisi_perintah_kirim_surat_masuk', 'id_disposisi='.$id_disposisi);
						}
							$isi_update_disposisi = array(
								'id_surat_masuk' 	=> $id_surat_masuk,
								'sifat' 			=> $sifat,
								//'perihal'			=> $perihal,
								'disposisi' 		=> $disposisi,
								'uraian_kadis' 		=> $uraian_kadis,
								//'diketahui_kadis'	=> $diketahui_kadis,
								'id_pejabat_ttd_disposisi' => $id_pejabat_ttd_disposisi,
								'author_update'		=> $this->session->userdata('username'),
								'last_update' 		=> date("Y-m-d H:i:s")
					
							);
							
							foreach($id_user_kirim as $u => $user){
								$isi_perintah_kirim = array(
									'id_user'	=> $user,
									'id_disposisi' => $id_disposisi
								);
								$this->model_surat_masuk->insert('tbl_disposisi_perintah_kirim_surat_masuk', $isi_perintah_kirim);
							}

							if ($id_tujuan == NULL) {

								$this->model_surat_masuk->delete('tbl_tujuan_disposisi', 'id_disposisi='.$id_disposisi);
					
							} else {
								$i=0;
								$cek_kesamaan = 0;
								$data['cek_tujuan_disposisi'] = $this->model_surat_masuk->get_where('tbl_tujuan_disposisi', 'id_disposisi='.$id_disposisi)->result();
								if(isset($data['cek_tujuan_disposisi'])){
									$this->model_surat_masuk->delete('tbl_tujuan_disposisi', 'id_disposisi='.$id_disposisi);
					
									//perulangan data array dari inputan multiple
									foreach ($id_tujuan as $it => $tujuan) {
										$isi_data_tujuan = array(
											'id_disposisi' => $id_disposisi,
											'id_tujuan' => $tujuan
										);
										$this->model_surat_masuk->insert('tbl_tujuan_disposisi', $isi_data_tujuan);
									}
					
								} else {
					
									//perulangan data array dari inputan multiple
									foreach ($id_tujuan as $it => $tujuan) {
										$isi_data_tujuan = array(
											'id_disposisi' => $id_disposisi,
											'id_tujuan' => $tujuan
										);
										$this->model_surat_masuk->insert('tbl_tujuan_disposisi', $isi_data_tujuan);
									}
								}
							}
						}
						$this->model_surat_masuk->update('tbl_disposisi', 'id_surat_masuk='.$id_surat_masuk, $isi_update_disposisi);
				} elseif($id_role == '2'){
					$isi_update_disposisi = array(
						'id_surat_masuk' 	=> $id_surat_masuk,
						'sifat' 			=> $sifat,
						// 'perihal'			=> $perihal,
						'author_update'		=> $this->session->userdata('username'),
						'last_update'		=> date("Y-m-d H:i:s")
					);
					$this->model_surat_masuk->update('tbl_disposisi', 'id_surat_masuk='.$id_surat_masuk, $isi_update_disposisi);
				} elseif($id_role == '5' || $id_role == '6'){
					$isi_update_disposisi_kabid = array(
						'id_disposisi' => $id_disposisi,
						'id_user' => $id_user,
						'uraian_kabid' => $uraian_kabid,
						'author_insert'	=> $this->session->userdata('username'),
						'insert_date' => date("Y-m-d H:i:s")
					);
					$data['cek_uraian_kabid']	= $this->model_surat_masuk->get_where('tbl_disposisi_uraian_kabid', 'id_disposisi= "'.$id_disposisi.'" AND id_user="'.$id_user.'"')->result();
					// var_dump($data['cek_uraian_kabid']);
					// exit(0);
					if(count($data['cek_uraian_kabid']) > 0){
						$this->model_surat_masuk->update('tbl_disposisi_uraian_kabid', 'id_disposisi= "'.$id_disposisi.'" AND id_user="'.$id_user.'"', $isi_update_disposisi_kabid);
					} else {
						$this->model_surat_masuk->insert('tbl_disposisi_uraian_kabid', $isi_update_disposisi_kabid);
						
					}

				}
	
		// $this->model_surat_masuk->update('tbl_disposisi', 'id_surat_masuk='.$id_surat_masuk, $isi_update_disposisi);

		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'ubah data disposisi surat masuk oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'ubah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->model_surat_masuk->insert('tbl_log_user', $isi_data_log);
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menyimpan Data Disposisi'));
		redirect('/suratmasuk/','refresh');
	}


	function cetakDisposisi($id, $userpilih){
		$id = decrypt_url($id);
		$userpilih = decrypt_url($userpilih);
		$id_user = $this->session->userdata('id_user');
		
		$where = [];
		$data['disposisi'] = $this->model_surat_masuk->get_duatable('*', 'tbl_surat_masuk', 'tbl_disposisi', 'tbl_disposisi.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_surat_masuk.id_surat_masuk='.$id, 'tbl_surat_masuk.id_surat_masuk')[0];
		if(function_exists('check_if_role_is')){
			if (check_if_role_is('1') || check_if_role_is('2') || check_if_role_is('3') || check_if_role_is('4')){
				$data['kepada'] = $this->model_surat_masuk->get_empattable('*', 'tbl_disposisi', 'tbl_disposisi_perintah_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_disposisi.id_disposisi=tbl_disposisi_perintah_kirim_surat_masuk.id_disposisi', 'tbl_disposisi_perintah_kirim_surat_masuk.id_user=tbl_user.id_user','tbl_user.id_role=tbl_role.id', 'tbl_disposisi.id_surat_masuk ='.$id. ' AND tbl_disposisi_perintah_kirim_surat_masuk.id_user ='.$userpilih , 'tbl_disposisi.id_disposisi');
			} else {
				$data['kepada'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_history_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_history_kirim_surat_masuk.id_user_tujuan=tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_history_kirim_surat_masuk.id_surat_masuk ='.$id. ' AND tbl_history_kirim_surat_masuk.id_user_tujuan ='.$userpilih , 'tbl_history_kirim_surat_masuk.id_surat_masuk');
			}
		}
		$data['pejabat_disposisi'] = $this->model_surat_masuk->get_limatable('*', 'tbl_disposisi', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_jabatan', 'tbl_golongan', 'tbl_disposisi.id_pejabat_ttd_disposisi = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_pegawai.id_jabatan=tbl_jabatan.id_jabatan', 'tbl_pegawai.id_golongan=tbl_golongan.id_golongan', 'tbl_disposisi.id_surat_masuk='.$id)[0];
		$data['tujuan'] =  $this->model_surat_masuk->get('tbl_tujuan', 'id', 'ASC');
		$data['tujuan_disposisi_dipilih'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_tujuan_disposisi', 'tbl_disposisi', 'tbl_tujuan', 'tbl_tujuan_disposisi.id_disposisi = tbl_disposisi.id_disposisi', 'tbl_tujuan_disposisi.id_tujuan=tbl_tujuan.id', 'tbl_disposisi.id_surat_masuk='.$id, 'tbl_tujuan.id');
		$data['tujuan_disposisi_dipilih_ids'] = [];
		foreach ($data['tujuan_disposisi_dipilih'] as $d) {
			$data['tujuan_disposisi_dipilih_ids'][] = $d->id_tujuan;
		}

		// if(function_exists('check_if_role_is')){
		// 	if (check_if_role_is('5') || check_if_role_is('6')){
		// 		$data['uraian_kabid'] = $this->model_surat_masuk->get_duatable('*', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi', 'tbl_disposisi_uraian_kabid.id_disposisi=tbl_disposisi.id_disposisi', 'tbl_disposisi.id_surat_masuk="'.$id.'" AND tbl_disposisi_uraian_kabid.id_user="'.$id_user.'"', 'tbl_disposisi.id_surat_masuk')[0];
		// 	} elseif (check_if_role_is('7') || check_if_role_is('8')) {
		// 		$parent_id_sub_user = $this->session->userdata('parent_id_sub_user');
		// 		$data['uraian_kabid'] = $this->model_surat_masuk->get_duatable('*', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi', 'tbl_disposisi_uraian_kabid.id_disposisi=tbl_disposisi.id_disposisi', 'tbl_disposisi.id_surat_masuk="'.$id.'" AND tbl_disposisi_uraian_kabid.id_user="'.$parent_id_sub_user.'"', 'tbl_disposisi.id_surat_masuk')[0];
		// 	} elseif (check_if_role_is('1') || check_if_role_is('2') || check_if_role_is('3')) {
		// 		$id_kabid = decrypt_url($this->uri->segment(4));
		// 		$data['uraian_kabid'] = $this->model_surat_masuk->get_duatable('*', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi', 'tbl_disposisi_uraian_kabid.id_disposisi=tbl_disposisi.id_disposisi', 'tbl_disposisi.id_surat_masuk="'.$id.'" AND tbl_disposisi_uraian_kabid.id_user="'.$id_kabid.'"', 'tbl_disposisi.id_surat_masuk')[0];
		// 	} 
		// }

		$mpdf = new \Mpdf\Mpdf();
		$data = $this->load->view('admin/cetak_disposisi', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('Disposisi.pdf', 'I');

	}

	function cetakDisposisiKasubid($id, $userpilih){
		$id = decrypt_url($id);
		$userpilih = decrypt_url($userpilih);
		$id_user = $this->session->userdata('id_user');
		
		$where = [];
		$data['disposisi'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_surat_masuk', 'tbl_disposisi', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_disposisi.id_disposisi=tbl_disposisi_uraian_kabid.id_disposisi', 'tbl_surat_masuk.id_surat_masuk='.$id.' AND tbl_disposisi_uraian_kabid.id_user='.$userpilih, 'tbl_surat_masuk.id_surat_masuk')[0];
		$data['kepada'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_history_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_history_kirim_surat_masuk.id_user_tujuan=tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_history_kirim_surat_masuk.id_surat_masuk ='.$id. ' AND tbl_history_kirim_surat_masuk.id_user_pengirim ='.$userpilih. ' AND tbl_history_kirim_surat_masuk.id_user_tujuan='.$id_user, 'tbl_history_kirim_surat_masuk.id_surat_masuk');
		$data['pejabat_disposisi'] = $this->model_surat_masuk->get_duatable('*', 'tbl_user', 'tbl_role', 'tbl_user.id_role=tbl_role.id','tbl_user.id_user='.$userpilih, 'tbl_user.id_user')[0];
		$data['tujuan'] =  $this->model_surat_masuk->get('tbl_tujuan', 'id', 'ASC');
		$data['tujuan_disposisi_dipilih'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_tujuan_disposisi', 'tbl_disposisi', 'tbl_tujuan', 'tbl_tujuan_disposisi.id_disposisi = tbl_disposisi.id_disposisi', 'tbl_tujuan_disposisi.id_tujuan=tbl_tujuan.id', 'tbl_disposisi.id_surat_masuk='.$id, 'tbl_tujuan.id');
		$data['tujuan_disposisi_dipilih_ids'] = [];
		foreach ($data['tujuan_disposisi_dipilih'] as $d) {
			$data['tujuan_disposisi_dipilih_ids'][] = $d->id_tujuan;
		}


		$mpdf = new \Mpdf\Mpdf();
		$data = $this->load->view('admin/cetak_disposisi', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('Disposisi.pdf', 'I');

	}



	public function tgl_indo($tanggal){
		$bulan = array (
			1 =>   	'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('/', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun
		return $pecahkan[1] . ' ' . $bulan[ (int)$pecahkan[0] ] . ' ' . $pecahkan[2];
	}

	public function cek_cetak_dispo($id_surat){
		$data['cek_dispo'] = $this->admin_model->get_duatable('*', 'tbl_disposisi', 'tbl_tujuan_disposisi', 'tbl_disposisi.id_disposisi=tbl_tujuan_disposisi.id_disposisi', 'tbl_disposisi.id_surat_masuk='.$id_surat, 'tbl_disposisi.id_disposisi');
		$save = false;
		foreach($data['cek_dispo'] as $cek){
			if((!empty($cek->uraian_kadis)) && (!empty($cek->sifat)) && (!empty($cek->id_pejabat_ttd_disposisi)) && (!empty($cek->disposisi)) && (!empty($cek->id_tujuan))){
				$save = true;
			}
		}
		return $save;		
	}

	public function cek_edit_surat($id_surat){
		$data['cek_es'] = $this->admin_model->get_tbl_where('kirim_surat_masuk', 'tbl_kirim_surat_masuk.id_surat_masuk='.$id_surat)->result();
		$save = false;
		foreach($data['cek_es'] as $cek){
			if($cek->id_user == $cek->user_pengirim){
				$save = true;
			}	
		}
		return $save;		
	}

	public function cek_kirim_surat($id_surat){
		$id_user = $this->session->userdata('id_user');
		$data['cek_s'] = $this->admin_model->get_duatable('*', 'tbl_disposisi', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi_uraian_kabid.id_disposisi=tbl_disposisi.id_disposisi', 'tbl_disposisi.id_surat_masuk='.$id_surat.' AND tbl_disposisi_uraian_kabid.id_user='.$id_user, 'tbl_disposisi_uraian_kabid.id');
		$save = false;
		foreach($data['cek_s'] as $cek){
			if(!empty($cek->uraian_kabid)){
				$save = true;
			}	
		}
		return $save;		
	}

	public function tgl_lengkap($tgl_lgkp){
		return strftime('%e %B %Y %H:%M:%S', strtotime($tgl_lgkp));
	}

	public function simpan_kirim_user_surat_masuk(){
		$id_surat_masuk = decrypt_url($this->uri->segment(3));
		$id_user_before = $this->session->userdata('id_user');
		$id_user = $this->input->post('id_user');
		// var_dump($id_user);
		// exit(0);
		$this->model_surat_masuk->delete('tbl_kirim_surat_masuk', 'id_surat_masuk='.$id_surat_masuk.' AND id_user='.$id_user_before);
		foreach($id_user as $u => $user){
			$isi_kirim_surat= array(
				'id_user' => $user,
				'id_surat_masuk' => $id_surat_masuk,
				'tgl_kirim' => date("Y-m-d H:i:s"),
				'user_pengirim' => $this->session->userdata('id_user')
			);
			// $this->model_surat_masuk->update('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk, $isi_ubah_status);
			$this->model_surat_masuk->insert('tbl_kirim_surat_masuk', $isi_kirim_surat);
			
			$isi_history_kirim_surat= array(
				'id_user_pengirim' => $id_user_before,
				'id_surat_masuk' => $id_surat_masuk,
				'id_user_tujuan' => $user,
				'tgl_kirim' => strftime('%e %B %Y %H:%M:%S', strtotime(date("Y-m-d H:i:s")))
			);
			
			$this->model_surat_masuk->insert('tbl_history_kirim_surat_masuk', $isi_history_kirim_surat);

			$isi_tambah_notif= array(
				'status_dibaca' => 'belum',
				'id_surat' => $id_surat_masuk,
				'id_jenis_surat' => 1,
				'tgl_notif' => date("Y-m-d H:i:s"),
				'id_user' => $user,
				'klik_lonceng' => 'b'
			);
			$this->model_surat_masuk->insert('tbl_notifikasi', $isi_tambah_notif);
			
		}

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengirim Data Surat Masuk'));
		redirect('suratmasuk/','refresh');
	}

	function daftar_disposisi_bidang($id){
		$id_user = $this->session->userdata('id_user');
		$id_role = $this->session->userdata('id_role');
		$id_surat_masuk = decrypt_url($this->uri->segment(3));
		$where = array();
		// if($id_role == 2 || $id_role == 1){
			
		// 	$data['status_posisi'] = $this->model_surat_masuk->get_delapantable('user_penerima.nama as nama_penerima, tbl_kirim_surat_masuk.tgl_kirim, user_penerima.keterangan_jabatan as bidang_penerima' , 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user user_penerima', 'tbl_role role_penerima', 'tbl_instansi instansi_penerima',  'tbl_user user_pengirim', 'tbl_role role_pengirim', 'tbl_instansi instansi_pengirim', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = user_penerima.id_user', 'user_penerima.id_role=role_penerima.id', 'user_penerima.id_instansi=instansi_penerima.id_instansi', 'tbl_kirim_surat_masuk.user_pengirim = user_pengirim.id_user', 'user_pengirim.id_role=role_pengirim.id', 'user_pengirim.id_instansi=instansi_pengirim.id_instansi', 'tbl_surat_masuk.id_surat_masuk="'.$id_surat_masuk.'" AND user_penerima.id_role=5 OR user_penerima.id_role=4 OR user_penerima.id_role=6', 'tbl_surat_masuk.id_surat_masuk');

		// 	// $data['status_posisi'] = $this->model_surat_masuk->get_limatable('*', 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_surat_masuk.id_surat_masuk='.$id_surat_masuk, 'tbl_surat_masuk.id_surat_masuk');
		// } else {

		// 	$data['status_posisi'] = $this->model_surat_masuk->get_delapantable('user_penerima.nama as nama_penerima, tbl_kirim_surat_masuk.tgl_kirim, user_pengirim.nama as nama_pengirim' , 'tbl_surat_masuk', 'tbl_kirim_surat_masuk', 'tbl_user user_penerima', 'tbl_role role_penerima', 'tbl_instansi instansi_penerima',  'tbl_user user_pengirim', 'tbl_role role_pengirim', 'tbl_instansi instansi_pengirim', 'tbl_surat_masuk.id_surat_masuk=tbl_kirim_surat_masuk.id_surat_masuk', 'tbl_kirim_surat_masuk.id_user = user_penerima.id_user', 'user_penerima.id_role=role_penerima.id', 'user_penerima.id_instansi=instansi_penerima.id_instansi', 'tbl_kirim_surat_masuk.user_pengirim = user_pengirim.id_user', 'user_pengirim.id_role=role_pengirim.id', 'user_pengirim.id_instansi=instansi_pengirim.id_instansi', 'tbl_surat_masuk.id_surat_masuk='.$id_surat_masuk.' AND tbl_kirim_surat_masuk.id_user='.$id_user , 'tbl_surat_masuk.id_surat_masuk');
		// }

		$data['surat_masuk'] = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk)->result();
		$data['disposisi_bidang'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_disposisi_perintah_kirim_surat_masuk', 'tbl_disposisi', 'tbl_user', 'tbl_disposisi_perintah_kirim_surat_masuk.id_disposisi=tbl_disposisi.id_disposisi', 'tbl_disposisi_perintah_kirim_surat_masuk.id_user=tbl_user.id_user', 'tbl_disposisi.id_surat_masuk='.$id_surat_masuk, 'tbl_user.id_role');

		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/cetak_disposisi_sekretariat', $data);
		$this->load->view('template/footer');
	}

	function daftar_disposisi_kasubid($id){
		$id_user = $this->session->userdata('id_user');
		$id_role = $this->session->userdata('id_role');
		$parent_id_sub_user = $this->session->userdata('parent_id_sub_user');
		$id_surat_masuk = decrypt_url($this->uri->segment(3));
		$where = array();
		$data['surat_masuk'] = $this->model_surat_masuk->get_where('tbl_surat_masuk', 'id_surat_masuk='.$id_surat_masuk)->result();
		$data['disposisi_kasubid'] = $this->model_surat_masuk->get_tigatable('*', 'tbl_history_kirim_surat_masuk', 'tbl_user', 'tbl_role', 'tbl_history_kirim_surat_masuk.id_user_pengirim=tbl_user.id_user', 'tbl_user.id_role=tbl_role.id', 'tbl_history_kirim_surat_masuk.id_surat_masuk='.$id_surat_masuk. ' AND (tbl_history_kirim_surat_masuk.id_user_tujuan='.$id_user. ' OR tbl_history_kirim_surat_masuk.id_user_tujuan='.$parent_id_sub_user.')', 'tbl_history_kirim_surat_masuk.id_history');

		$data['page'] = 'surat_masuk';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/cetak_disposisi_kasubid', $data);
		$this->load->view('template/footer');
	}

	function cek_count_disposisi(){
		$id_user = $this->session->userdata('id_user');
		$id_surat_masuk = decrypt_url($this->uri->segment(3));
		$where = array();
		$data['count_disposisi'] = $this->model_surat_masuk->get_duatable('*', 'tbl_disposisi_uraian_kabid', 'tbl_disposisi', 'tbl_disposisi_uraian_kabid.id_disposisi=tbl_disposisi.id_disposisi', 'tbl_disposisi.id_surat_masuk = '.$id_surat_masuk.'', 'tbl_disposisi_uraian_kabid.id_user');
		if (check_if_role_is('1') || check_if_role_is('2') || check_if_role_is('3') || check_if_role_is('4')){
			// if(count($data['count_disposisi']) > 1){
				redirect('suratmasuk/daftar_disposisi_bidang/'.encrypt_url($id_surat_masuk),'refresh');
			// } else {
			// 	redirect('suratmasuk/cetakDisposisi/'.encrypt_url($id_surat_masuk),'refresh');
			// }
		} elseif(check_if_role_is('7') || check_if_role_is('8')) {
			redirect('suratmasuk/daftar_disposisi_kasubid/'.encrypt_url($id_surat_masuk),'refresh');
		}
		
		else {
			redirect('suratmasuk/cetakDisposisi/'.encrypt_url($id_surat_masuk).'/'.encrypt_url($id_user),'refresh');
		}
	}

	function getById(){
		$id_user = $this->session->userdata('id_user');
		$id_role = $this->session->userdata('id_role');
		$id_surat_masuk	= $this->input->post('id');
		if($id_role == 2 || $id_role == 1){
			$data=$this->model_surat_masuk->get_delapantable('tbl_history_kirim_surat_masuk.id_history, tbl_history_kirim_surat_masuk.tgl_kirim, user_pengirim.nama as nama_pengirim, role_pengirim.role, user_pengirim.keterangan_jabatan, user_tujuan.nama as nama_tujuan', 'tbl_history_kirim_surat_masuk', 'tbl_surat_masuk', 'tbl_user user_pengirim', 'tbl_role role_pengirim', 'tbl_instansi instansi_pengirim', 'tbl_user user_tujuan', 'tbl_role role_tujuan', 'tbl_instansi instansi_tujuan','tbl_history_kirim_surat_masuk.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_history_kirim_surat_masuk.id_user_pengirim=user_pengirim.id_user', 'user_pengirim.id_role=role_pengirim.id', 'user_pengirim.id_instansi=instansi_pengirim.id_instansi', 'tbl_history_kirim_surat_masuk.id_user_tujuan=user_tujuan.id_user', 'user_tujuan.id_role=role_tujuan.id', 'user_tujuan.id_instansi=instansi_tujuan.id_instansi','tbl_history_kirim_surat_masuk.id_surat_masuk ="'. $id_surat_masuk.'"', 'tbl_history_kirim_surat_masuk.id_history', 'DESC');
		} else {
			$data=$this->model_surat_masuk->get_delapantable('tbl_history_kirim_surat_masuk.tgl_kirim, user_pengirim.nama as nama_pengirim, user_tujuan.nama as nama_tujuan', 'tbl_history_kirim_surat_masuk', 'tbl_surat_masuk', 'tbl_user user_pengirim', 'tbl_role role_pengirim', 'tbl_instansi instansi_pengirim', 'tbl_user user_tujuan', 'tbl_role role_tujuan', 'tbl_instansi instansi_tujuan','tbl_history_kirim_surat_masuk.id_surat_masuk=tbl_surat_masuk.id_surat_masuk', 'tbl_history_kirim_surat_masuk.id_user_pengirim=user_pengirim.id_user', 'user_pengirim.id_role=role_pengirim.id', 'user_pengirim.id_instansi=instansi_pengirim.id_instansi', 'tbl_history_kirim_surat_masuk.id_user_tujuan=user_tujuan.id_user', 'user_tujuan.id_role=role_tujuan.id', 'user_tujuan.id_instansi=instansi_tujuan.id_instansi','tbl_history_kirim_surat_masuk.id_surat_masuk ="'. $id_surat_masuk.'" && tbl_history_kirim_surat_masuk.id_user_pengirim="'.$id_user.'"', 'tbl_history_kirim_surat_masuk.id_history', 'DESC');
		}

		echo json_encode($data);		
	}


}
