<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stl extends CI_Controller {
	public $tgl_indo = NULL;
	public $cek_cetak_stl = false;
	function __construct(){
		parent::__construct();
		$this->tgl_indo = & get_instance();
		$this->cek_cetak_stl = & get_instance();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}

	}


	public function index()
	{
		$where = [];
		$limit = [];
		$data['stl'] = $this->admin_model->get_order_by('*','stl', $where, 'id_stl', 'Desc', $limit);

		$data['page'] = 'stl';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/stl', $data);
		$this->load->view('template/footer');

	}

	public function tambah_stl(){
		$where = array();
		$data['no_stl'] = [];
		$last = $this->admin_model->get_order_by('id_stl','stl', $where, 'id_stl', 'Desc', 1);
		if (!empty($last)) {
			$last_id = $last[0]->id_stl + 1;
			if($last_id <= 9){
				$data['no_stl'] = '00'.$last_id;
			} elseif ($last_id <= 99) {
				$data['no_stl'] = '0'.$last_id;
			} else{
				$data['no_stl'] = $last_id;
			}
		} else{
			$data['no_stl'] = '001';
		}
		$data['pegawai_stl'] = $this->admin_model->get_tbl('pegawai')->result();
		$data['pejabat_ttd_spt'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd.id_pegawai');
		

		$data['page'] = 'stl';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/tambah_stl', $data);
		$this->load->view('template/footer');
		
	}

	public function simpan_data_stl(){
		$no_urut_stl = $this->input->post('no_urut_stl');
		$bulan_romawi  = $this->input->post('bulan_romawi');
		$no_stl_full = '094/'.$no_urut_stl.'/STL/BAPPEDA/'.$bulan_romawi.'/'.date('Y');
		$tgl_stl = $this->input->post('tanggal_stl');
		$tgl_acara = $this->input->post('tgl_serta_waktu_tugas_luar');
		$lokasi = $this->input->post('lokasi');
		$mtl = $this->input->post('mtl');
		$pejabat_stl = $this->input->post('pejabat_stl');
		$nama_pj_tl = $this->input->post('nama_pj_tl');

		$pisah_tgl = explode('-', $tgl_acara);
		$tgl_mulai = $pisah_tgl[0];
		$tgl_selesai = $pisah_tgl[1];
		$id_instansi 		= $this->session->userdata('id_instansi');

		$isi_data_stl = array(
			'no_stl' 	=>	$no_stl_full,
			'tgl_stl'	=>	$tgl_stl,
			'tgl_mulai'	=>	$tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'lokasi'	=>	$lokasi,
			'maksud'	=>	$mtl,
			'id_pejabat_spt' => $pejabat_stl,
			'id_instansi'		=> $id_instansi,
			'insert_date' => date("Y-m-d H:i:s"),
			'author_insert' 	=>	$this->session->userdata('username')
		); 

		$this->admin_model->tambah('stl', $isi_data_stl);

		
		if (isset($nama_pj_tl)) {
			$where = [];
			$data['last_record'] = $this->admin_model->get_order_by('id_stl','stl', $where, 'id_stl', 'Desc', 1);

			foreach ($data['last_record'] as $lr) {
				$last_id_stl = $lr->id_stl;
			}

   			
			$i=0;
			$kaban = false;
			$idk = 0;
			foreach ($nama_pj_tl as $ip => $pegawai) :
				// $pegawai_stl = array(
				// 	'id_stl' => $last_id_stl,
				// 	'id_pegawai' => $pegawai
				// );
				// $this->admin_model->tambah('pegawai_stl', $pegawai_stl);
				// $i++;
				$data['cek_pgw'] = $this->admin_model->get_tbl_where('pegawai', 'id_pegawai='.$pegawai)->result();
				//print_r($data['cek_pgw']);
				foreach($data['cek_pgw'] as $cpg){
					if($cpg->id_jabatan == 1){
						$kaban = true;
						$idk = $cpg->id_pegawai;
					}
				}
				
			endforeach;

			if($kaban == true){
				$pegawai_stl = array(
					'id_stl' => $last_id_stl,
					'id_pegawai' => $idk
				);
				$this->admin_model->tambah('pegawai_stl', $pegawai_stl);
				$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil, Khusus Kaban Hanya Boleh Satu Penugasan'));
				

			} else {
				foreach ($nama_pj_tl as $ip => $pegawai) :
					$pegawai_stl = array(
						'id_stl' => $last_id_stl,
						'id_pegawai' => $pegawai
					);
					$this->admin_model->tambah('pegawai_stl', $pegawai_stl);
					$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data STL'));		
				endforeach;
			}
			
			$data['cek_kaban'] = $this->admin_model->get_tigatable('*', 'tbl_pegawai_stl','tbl_stl', 'tbl_pegawai', 'tbl_pegawai_stl.id_stl=tbl_stl.id_stl','tbl_pegawai_stl.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_pegawai_stl.id_stl='.$last_id_stl, 'tbl_stl.id_stl');
			foreach($data['cek_kaban'] as $ck){
				$id_jabatan = $ck->id_jabatan;
				$no_stl = $ck->no_stl;
			}
			if($id_jabatan == 1){
				$no_stl_br = str_replace("BAPPEDA","SETDA-PB",$no_stl);
				$isi_ubah_stl = array(
					'no_stl' => $no_stl_br,
					'last_update' => date("Y-m-d H:i:s"),
					'author_update' 	=>	$this->session->userdata('username')
		
				);
				$this->admin_model->update_data('stl','id_stl='.$last_id_stl, $isi_ubah_stl);
			} else {
				$no_stl_br = str_replace("SETDA-PB","BAPPEDA",$no_stl);
				$isi_ubah_stl = array(
					'no_stl' => $no_stl_br,
					'last_update' => date("Y-m-d H:i:s"),
					'author_update' 	=>	$this->session->userdata('username')
		
				);
				$this->admin_model->update_data('stl','id_stl='.$last_id_stl, $isi_ubah_stl);
			}
			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

				// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data stl oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			$this->admin_model->tambah('log_user', $isi_data_log);
			redirect(base_url('/stl/'), 'refresh');

		} else {
			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

				// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data stl oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			$this->admin_model->tambah('log_user', $isi_data_log);
			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data STL'));		
			redirect(base_url('/stl/'), 'refresh');


		}
	}


	public function ubah_stl()
	{
		$id_stl	= decrypt_url($this->uri->segment(3));
		$where = array();
		$data['stl'] = $this->admin_model->get_tbl_where('stl', 'id_stl='.$id_stl)->result();
		$data['pegawai_dipilih'] = $this->admin_model->get_duatable('*', 'tbl_pegawai_stl', 'tbl_pegawai', 'tbl_pegawai_stl.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_pegawai_stl.id_stl='.$id_stl, 'tbl_pegawai_stl.id_stl');
		$data['pejabat_ttd_dipilih'] = $this->admin_model->get_tigatable('*', 'tbl_stl', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_stl.id_pejabat_spt = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_stl.id_stl='.$id_stl, 'tbl_stl.id_stl');

		$data['pegawai_stl'] = $this->admin_model->get_tbl('pegawai')->result();
		$data['pejabat_ttd_spt'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd.id_pegawai');

		$data['page'] = 'stl';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_stl', $data);
		$this->load->view('template/footer');

	}


	public function simpan_ubah_stl(){

		$id_stl	= decrypt_url($this->uri->segment(3));	
		$no_urut_stl = $this->input->post('no_urut_stl');
		$bulan_romawi  = $this->input->post('bulan_romawi');
		$no_stl_full = '094/'.$no_urut_stl.'/STL/BAPPEDA/'.$bulan_romawi.'/'.date('Y');
		$tgl_stl = $this->input->post('tanggal_stl');
		$tgl_acara = $this->input->post('tgl_serta_waktu_tugas_luar');
		$lokasi = $this->input->post('lokasi');
		$mtl = $this->input->post('mtl');
		$pejabat_stl = $this->input->post('pejabat_stl');
		$nama_pj_tl = $this->input->post('nama_pj_tl');
		$pisah_tgl = explode('-', $tgl_acara);
		$tgl_mulai = $pisah_tgl[0];
		$tgl_selesai = $pisah_tgl[1];

		$isi_ubah_stl = array(
			'no_stl'	=> 	$no_stl_full,
			'tgl_stl'	=>	$tgl_stl,
			'tgl_mulai'	=>	$tgl_mulai,
			'tgl_selesai' => $tgl_selesai,
			'lokasi'	=>	$lokasi,
			'maksud'	=>	$mtl,
			'id_pejabat_spt' => $pejabat_stl,
			'last_update' => date("Y-m-d H:i:s"),
			'author_update' 	=>	$this->session->userdata('username')
		);


		$this->admin_model->update_data('stl','id_stl='.$id_stl, $isi_ubah_stl);

		if ($nama_pj_tl == NULL) {

			$this->admin_model->hapus_data('pegawai_stl', 'id_stl='.$id_stl);

			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'ubah data stl dg id '.$id_stl.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'ubah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			$this->admin_model->tambah('log_user', $isi_data_log);
			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data STL'));		
			redirect(base_url('/stl/'), 'refresh');

		} else {

			$i=0;
			$cek_kesamaan = 0;
			$data['cek_pegawai'] = $this->admin_model->get_tbl_where('pegawai_stl', 'id_stl='.$id_stl)->result();
			if(isset($data['cek_pegawai'])){
				
				$this->admin_model->hapus_data('pegawai_stl', 'id_stl='.$id_stl);		
				$kaban = false;
				$idk = 0;
				foreach ($nama_pj_tl as $ip => $pegawai) :
					// $pegawai_stl = array(
					// 	'id_stl' => $last_id_stl,
					// 	'id_pegawai' => $pegawai
					// );
					// $this->admin_model->tambah('pegawai_stl', $pegawai_stl);
					// $i++;
					$data['cek_pgw'] = $this->admin_model->get_tbl_where('pegawai', 'id_pegawai='.$pegawai)->result();
					//print_r($data['cek_pgw']);
					foreach($data['cek_pgw'] as $cpg){
						if($cpg->id_jabatan == 1){
							$kaban = true;
							$idk = $cpg->id_pegawai;
						}
					}
					
				endforeach;

				if($kaban == true){
					$pegawai_stl = array(
						'id_stl' => $id_stl,
						'id_pegawai' => $idk
					);
					$this->admin_model->tambah('pegawai_stl', $pegawai_stl);
					$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil, Khusus Kaban Hanya Boleh Satu Penugasan'));
					

				} else {
					foreach ($nama_pj_tl as $ip => $pegawai) :
						$pegawai_stl = array(
							'id_stl' => $id_stl,
							'id_pegawai' => $pegawai
						);
						$this->admin_model->tambah('pegawai_stl', $pegawai_stl);
						$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data STL'));		
					endforeach;
				}

			} else {
				
				$kaban = false;
				$idk = 0;
				foreach ($nama_pj_tl as $ip => $pegawai) :
					// $pegawai_stl = array(
					// 	'id_stl' => $last_id_stl,
					// 	'id_pegawai' => $pegawai
					// );
					// $this->admin_model->tambah('pegawai_stl', $pegawai_stl);
					// $i++;
					$data['cek_pgw'] = $this->admin_model->get_tbl_where('pegawai', 'id_pegawai='.$pegawai)->result();
					//print_r($data['cek_pgw']);
					foreach($data['cek_pgw'] as $cpg){
						if($cpg->id_jabatan == 1){
							$kaban = true;
							$idk = $cpg->id_pegawai;
						}
					}
					
				endforeach;

				if($kaban == true){
					$pegawai_stl = array(
						'id_stl' => $id_stl,
						'id_pegawai' => $idk
					);
					$this->admin_model->tambah('pegawai_stl', $pegawai_stl);
					$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil, Khusus Kaban Hanya Boleh Satu Penugasan'));
					

				} else {
					foreach ($nama_pj_tl as $ip => $pegawai) :
						$pegawai_stl = array(
							'id_stl' => $id_stl,
							'id_pegawai' => $pegawai
						);
						$this->admin_model->tambah('pegawai_stl', $pegawai_stl);
						$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data STL'));		
					endforeach;
				}
			}

			$data['cek_kaban'] = $this->admin_model->get_tigatable('*', 'tbl_pegawai_stl','tbl_stl', 'tbl_pegawai', 'tbl_pegawai_stl.id_stl=tbl_stl.id_stl','tbl_pegawai_stl.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_pegawai_stl.id_stl='.$id_stl, 'tbl_stl.id_stl');
			foreach($data['cek_kaban'] as $ck){
				$id_jabatan = $ck->id_jabatan;
				$no_stl = $ck->no_stl;
			}
			if($id_jabatan == 1){
				$no_stl_br = str_replace("BAPPEDA","SETDA-PB",$no_stl);
				$isi_ubah_stl = array(
					'no_stl' => $no_stl_br,
					'last_update' => date("Y-m-d H:i:s"),
					'author_update' 	=>	$this->session->userdata('username')
		
				);
				$this->admin_model->update_data('stl','id_stl='.$id_stl, $isi_ubah_stl);
			} else {
				$no_stl_br = str_replace("SETDA-PB","BAPPEDA",$no_stl);
				$isi_ubah_stl = array(
					'no_stl' => $no_stl_br,
					'last_update' => date("Y-m-d H:i:s"),
					'author_update' 	=>	$this->session->userdata('username')
		
				);
				$this->admin_model->update_data('stl','id_stl='.$id_stl, $isi_ubah_stl);
			}
			
			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'ubah data stl dg id '.$id_stl.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'ubah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			$this->admin_model->tambah('log_user', $isi_data_log);
			redirect(base_url('/stl/'), 'refresh');
		}
	}



	public function cetak_stl1($id)
	{
		$id_stl	= decrypt_url($id);

		$data['stl'] = $this->admin_model->get_tbl_where('stl', 'id_stl='.$id_stl)->result()[0];
		$data['pegawai_stl'] = $this->admin_model->get_empattable('*', 'tbl_stl', 'tbl_pegawai_stl', 'tbl_pegawai', 'tbl_jabatan', 'tbl_pegawai_stl.id_stl=tbl_stl.id_stl', 'tbl_pegawai.id_pegawai=tbl_pegawai_stl.id_pegawai', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_stl.id_stl='.$id_stl, 'tbl_stl.id_stl');
		$data['ttd_spt'] = $this->admin_model->get_limatable('*', 'tbl_stl', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pejabat_ttd.id=tbl_stl.id_pejabat_spt', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_stl.id_stl='.$id_stl, 'tbl_stl.id_stl')[0];
		

		$mpdf = new \Mpdf\Mpdf(['tempDir' => 'application/cache/']);
		$data = $this->load->view('admin/cetak_stl1', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('STL_1.pdf', 'I');
		
	}

	public function cetak_stl2($id){
		//tangkap data dengan id tertentu lalu jadikan parameter di view

		$id_stl	= decrypt_url($id);
		$data['ttd_spt'] = $this->admin_model->get_limatable('*', 'tbl_stl', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pejabat_ttd.id=tbl_stl.id_pejabat_spt', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_stl.id_stl='.$id_stl, 'tbl_stl.id_stl')[0];
		$mpdf = new \Mpdf\Mpdf(['tempDir' => 'application/cache/']);
		$data = $this->load->view('admin/cetak_hal2', $data, TRUE);
		$mpdf->WriteHTML($data);
		$mpdf->Output('STL_2.pdf', 'I');
	}

	public function tgl_indo($tanggal){
		$bulan = array (
			1 =>   	'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('/', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[1] . ' ' . $bulan[ (int)$pecahkan[0] ] . ' ' . $pecahkan[2];
	}

	public function cek_cetak_stl($id_surat){
		$data['cek_stl'] = $this->admin_model->get_duatable('*', 'tbl_stl', 'tbl_pegawai_stl', 'tbl_stl.id_stl=tbl_pegawai_stl.id_stl', 'tbl_stl.id_stl='.$id_surat, 'tbl_stl.id_stl');
		$save = false;
		foreach($data['cek_stl'] as $cek){
			if((!empty($cek->no_stl)) && (!empty($cek->tgl_stl)) && (!empty($cek->tgl_mulai)) && (!empty($cek->tgl_selesai)) && (!empty($cek->lokasi)) && (!empty($cek->maksud)) && (!empty($cek->id_pejabat_spt)) && (!empty($cek->id_pegawai))){
				$save = true;
			}
		}
		return $save;		
	}


}
