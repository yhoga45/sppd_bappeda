<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spt extends CI_Controller {
	public $tgl_indo = NULL;
	public $cek_cetak_spt = false;
	public $cek_detail_spt = false;

	function __construct(){
		parent::__construct();
		$this->tgl_indo = & get_instance();
		$this->cek_cetak_spt = & get_instance();
		$this->cek_detail_spt = & get_instance();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
		// if(function_exists('check_if_role_is')){
		// 	if (!check_if_role_is('1') && !check_if_role_is('2')){
		// 		echo "<script>alert('Maaf, anda tidak izinkan membuka fitur ini!')</script>";
		// 		redirect(base_url("admin/"));
		// 	}
		// }
	}


	public function index()
	{
		$where = [];
		$limit = [];
		$data['spt'] = $this->admin_model->get_order_by('*','spt', $where, 'id_spt', 'Desc', $limit);

		$data['page'] = 'spt_spd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/spt', $data);
		$this->load->view('template/footer');

	}

	public function simpan_data_spt(){
		$this->load->library('upload');
		$no_spt = $this->input->post('no_spt');
		$no_urut_spt = $this->input->post('no_urut_spt');
		$kode_spt = $this->input->post('kode_spt');
		$bulan_romawi  = $this->input->post('bulan_romawi');
		$no_spt_full = '094/'.$no_urut_spt.'/SPT/BAPPEDA/'.$bulan_romawi.'/'.date('Y');
		$tgl_spt = $this->input->post('tanggal_spt');
		$kota_brgkt = $this->input->post('kota_brgkt');
		$kota_tujuan = $this->input->post('kota_tujuan');
		$alat_angkut = $this->input->post('alat_angkut');
		$mpd = $this->input->post('maksut_pd');
		$pembebanan_anggaran = $this->input->post('sumber_anggaran');
		$pejabat_spt = $this->input->post('pejabat_spt');
		$pejabat_sppd = $this->input->post('pejabat_sppd');
		// $pejabat_pptk = $this->input->post('pejabat_ppk');

		$tgl_brgkt_kembali = $this->input->post('tgl_berangkat_dan_kembali');
		$pisah_tgl = explode("-", $tgl_brgkt_kembali );
		$tgl_berangkat = $pisah_tgl[0];
		$tgl_kembali = $pisah_tgl[1];
		// var_dump($tgl_berangkat);
		// exit(0);

		$tgl_berangkat_new  = new DateTime(trim($tgl_berangkat));
		$tgl_kembali_new = new DateTime(trim($tgl_kembali));
		$days = $tgl_kembali_new->diff($tgl_berangkat_new)->d;
		$id_instansi 		= $this->session->userdata('id_instansi');

		if ($days > 0) {
			$isi_data_spt = array(
				'no_spt' 	=>	$no_spt_full,
				'tgl_berangkat' => $tgl_berangkat,
				'tgl_kembali' => $tgl_kembali,
				'durasi' => $days+1,
				'tgl_spt' => $tgl_spt,
				'id_kota_brgkt' => $kota_brgkt,
				'id_kota_tujuan' => $kota_tujuan,
				'id_alat_angkut' => $alat_angkut,
				'maksut_pd' => $mpd,
				'id_sumber_anggaran' => $pembebanan_anggaran,
				'id_pejabat_ttd_spt' => $pejabat_spt,
				'id_pejabat_ttd_sppd' => $pejabat_sppd,
				'id_instansi'		=> $id_instansi,
				// 'id_pejabat_ttd_pptk' => $pejabat_pptk,
				'insert_date' => date("Y-m-d H:i:s"),
				'author_insert' 	=>	$this->session->userdata('username')
			);
		} else {
			$isi_data_spt = array(
				'no_spt' 	=>	$no_spt_full,
				'tgl_berangkat' => $tgl_berangkat,
				'tgl_kembali' => $tgl_kembali,
				'durasi' => 1,
				'tgl_spt' => $tgl_spt,
				'id_kota_brgkt' => $kota_brgkt,
				'id_kota_tujuan' => $kota_tujuan,
				'id_alat_angkut' => $alat_angkut,
				'maksut_pd' => $mpd,
				'id_sumber_anggaran' => $pembebanan_anggaran,
				'id_pejabat_ttd_spt' => $pejabat_spt,
				'id_pejabat_ttd_sppd' => $pejabat_sppd,
				'id_instansi'		=> $id_instansi,
				// 'id_pejabat_ttd_pptk' => $pejabat_pptk,
				'insert_date' => date("Y-m-d H:i:s"),
				'author_insert' 	=>	$this->session->userdata('username')
			);
		}

		$this->admin_model->tambah('spt', $isi_data_spt);


		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();

			// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'tambah data spt oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'tambah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);
		
		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data SPT'));
		redirect(base_url('/spt/'), 'refresh');
	}

	public function tambah_spt(){
		$where = array();
		$data['no_spt'] = [];
		$last = $this->admin_model->get_order_by('id_spt','spt', $where, 'id_spt', 'Desc', 1);
		if (!empty($last)) {
			$last_id = $last[0]->id_spt + 1;
			if($last_id <= 9){
				$data['no_spt'] = '00'.$last_id;
			} elseif ($last_id <= 99) {
				$data['no_spt'] = '0'.$last_id;
			} else{
				$data['no_spt'] = $last_id;
			}
		} else{
			$data['no_spt'] = '001';
		}
		

		$data['kota'] = $this->admin_model->get_tbl('kota')->result();
		$data['alat_angkut'] = $this->admin_model->get_tbl('alat_angkut')->result();
		$data['sumber_anggaran'] = $this->admin_model->get_tbl('sumber_anggaran')->result();
		$data['pejabat_ttd_spt'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd.id_pegawai');
		$data['pejabat_ttd_sppd'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd.id_pegawai');
		// $data['pejabat_ttd_pptk'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd_pptk', 'tbl_pegawai', 'tbl_pejabat_ttd_pptk.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd_pptk.id_pegawai');


		$data['page'] = 'spt_spd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/tambah_spt', $data);
		$this->load->view('template/footer');
		
	}

	public function ubah_spt()
	{

		$id_spt	= decrypt_url($this->uri->segment(3));
		$where = array();

		//master data
		$data['kota'] = $this->admin_model->get_tbl('kota')->result();
		$data['alat_angkut'] = $this->admin_model->get_tbl('alat_angkut')->result();
		$data['sumber_anggaran'] = $this->admin_model->get_tbl('sumber_anggaran')->result();
		$data['pejabat_ttd_spt'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd.id_pegawai');
		$data['pejabat_ttd_sppd'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd.id_pegawai');

		// $data['pejabat_ttd_pptk'] = $this->admin_model->get_duatable('*', 'tbl_pejabat_ttd_pptk', 'tbl_pegawai', 'tbl_pejabat_ttd_pptk.id=tbl_pegawai.id_pegawai', $where , 'tbl_pejabat_ttd_pptk.id_pegawai');

		//data yang telah di input sebelumnya
		// $data['spt_where'] = $this->admin_model->get_tbl_limit('spt', 'id_spt='.$id_spt, 1)->result();
		$data['kota_brgkt'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_brgkt=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['kota_tujuan'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_tujuan=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['alat_ang'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_alat_angkut', 'tbl_spt.id_alat_angkut=tbl_alat_angkut.id', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['sum_ang'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_sumber_anggaran', 'tbl_spt.id_sumber_anggaran=tbl_sumber_anggaran.id', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['spt_where'] = $this->admin_model->get_tbl_where('spt', 'tbl_spt.id_spt='.$id_spt)->result();
		$data['ttd_spt'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_spt = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');
		$data['ttd_sppd'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_sppd = tbl_pejabat_ttd.id', 'tbl_pejabat_ttd.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		// $data['ttd_pptk'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_pejabat_ttd_pptk', 'tbl_pegawai', 'tbl_spt.id_pejabat_ttd_pptk = tbl_pejabat_ttd_pptk.id', 'tbl_pejabat_ttd_pptk.id_pegawai=tbl_pegawai.id_pegawai', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

			// var_dump($id_spt);
			// exit(0);
		$data['page'] = 'spt_spd';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_spt', $data);
		$this->load->view('template/footer');

	}

	function hapusSpt(){
		$id_spt	= decrypt_url($this->uri->segment(3));
		$data['spt'] = $this->admin_model->get_tbl_where('spt', 'id_spt='.$id_spt)->result();
		foreach ($data['spt'] as $spt) {
			$no_spt = $spt->no_spt;
		}
		// var_dump($no_spt);
		// exit(0);

		$this->load->library('user_agent');

		$browser = $this->agent->browser();

		$browser_version = $this->agent->version();

		$os = $this->agent->platform();

		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'hapus data spt dg nomor '.$no_spt.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'hapus data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->admin_model->hapus_data('spt', 'id_spt='.$id_spt);
		$this->admin_model->hapus_data('sppd', 'id_spt='.$id_spt);
		$this->admin_model->hapus_data('pegawai_pengikut', 'id_spt='.$id_spt);


		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menghapus Data SPT'));
		redirect('/spt/','refresh');
	}


	public function cetak_spt($id)
	{
		$id_spt = decrypt_url($id);
		$where=[];
		$data['kota_brgkt'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_brgkt=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];
		$data['kota_tujuan'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_kota', 'tbl_spt.id_kota_tujuan=tbl_kota.id_kota', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];

		$data['spt'] = $this->admin_model->get_tigatable('*', 'tbl_spt', 'tbl_sumber_anggaran', 'tbl_alat_angkut', 'tbl_spt.id_sumber_anggaran = tbl_sumber_anggaran.id', 'tbl_spt.id_alat_angkut=tbl_alat_angkut.id', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];

		$data['kepada'] = $this->admin_model->get_empattable('*', 'tbl_spt', 'tbl_sppd', 'tbl_pegawai', 'tbl_jabatan', 'tbl_sppd.id_spt=tbl_spt.id_spt', 'tbl_pegawai.id_pegawai=tbl_sppd.id_pegawai', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		$data['pengikut'] = $this->admin_model->get_empattable('*', 'tbl_spt', 'tbl_pegawai_pengikut', 'tbl_pegawai', 'tbl_jabatan', 'tbl_pegawai_pengikut.id_spt=tbl_spt.id_spt', 'tbl_pegawai.id_pegawai=tbl_pegawai_pengikut.id_pegawai', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt');

		$data['ttd_spt'] = $this->admin_model->get_limatable('*', 'tbl_spt', 'tbl_pejabat_ttd', 'tbl_pegawai', 'tbl_golongan', 'tbl_jabatan', 'tbl_pejabat_ttd.id=tbl_spt.id_pejabat_ttd_spt', 'tbl_pegawai.id_pegawai=tbl_pejabat_ttd.id_pegawai', 'tbl_golongan.id_golongan=tbl_pegawai.id_golongan', 'tbl_jabatan.id_jabatan=tbl_pegawai.id_jabatan', 'tbl_spt.id_spt='.$id_spt, 'tbl_spt.id_spt')[0];



		
		$mpdf = new \Mpdf\Mpdf(['tempDir' => 'application/cache/']);
		$data = $this->load->view('admin/cetak_spt', $data, TRUE);
		var_dump($data);
		// $mpdf->WriteHTML($data);
		// $mpdf->Output('Dokumen SPT', 'I');
		
	}

	public function tgl_indo($tanggal){
		$bulan = array (
			1 =>   	'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('/', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[1] . ' ' . $bulan[ (int)$pecahkan[0] ] . ' ' . $pecahkan[2];
	}


	public function simpan_ubah_spt(){
		$this->load->library('upload');
		$id_spt	= decrypt_url($this->uri->segment(3));

		$no_spt = $this->input->post('no_spt');
		$no_urut_spt = $this->input->post('no_urut_spt');
		$kode_spt = $this->input->post('kode_spt');
		$bulan_romawi  = $this->input->post('bulan_romawi');
		$no_spt_full = '094/'.$no_urut_spt.$kode_spt.$bulan_romawi.'/'.date('Y');
		$tgl_spt = $this->input->post('tanggal_spt');
		$kota_brgkt = $this->input->post('kota_brgkt');
		$kota_tujuan = $this->input->post('kota_tujuan');
		$alat_angkut = $this->input->post('alat_angkut');
		$mpd = $this->input->post('maksut_pd');
		$pembebanan_anggaran = $this->input->post('sumber_anggaran');
		$pejabat_spt = $this->input->post('pejabat_spt');
		$pejabat_sppd = $this->input->post('pejabat_sppd');
		// $pejabat_pptk = $this->input->post('pejabat_ppk');

		$tgl_brgkt_kembali = $this->input->post('tgl_berangkat_dan_kembali');
		$pisah_tgl = explode("-", $tgl_brgkt_kembali );
		$tgl_berangkat = $pisah_tgl[0];
		$tgl_kembali = $pisah_tgl[1];
		// var_dump($bulan_romawi);
		// var_dump($kota_brgkt);
		// var_dump($kota_tujuan);
		// var_dump($this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil mengubah data SPT..')));
		// exit(0);

		$tgl_berangkat_new  = new DateTime(trim($tgl_berangkat));
		$tgl_kembali_new = new DateTime(trim($tgl_kembali));
		$days = $tgl_kembali_new->diff($tgl_berangkat_new)->d;

		// if($bulan_romawi == NULL || $kota_brgkt == NULL || $kota_tujuan == NULL || $alat_angkut)

		if ($days > 0) {
			$isi_ubah_spt = array(
				'no_spt' 	=>	$no_spt_full,
				'tgl_berangkat' => $tgl_berangkat,
				'tgl_kembali' => $tgl_kembali,
				'durasi' => $days+1,
				'tgl_spt' => $tgl_spt,
				'id_kota_brgkt' => $kota_brgkt,
				'id_kota_tujuan' => $kota_tujuan,
				'id_alat_angkut' => $alat_angkut,
				'maksut_pd' => $mpd,
				'id_sumber_anggaran' => $pembebanan_anggaran,
				'id_pejabat_ttd_spt' => $pejabat_spt,
				'id_pejabat_ttd_sppd' => $pejabat_sppd,
				// 'id_pejabat_ttd_pptk' => $pejabat_pptk,
				'last_update' => date("Y-m-d H:i:s"),
				'author_update' 	=>	$this->session->userdata('username')
			);
		} else {
			$isi_ubah_spt = array(
				'no_spt' 	=>	$no_spt_full,
				'tgl_berangkat' => $tgl_berangkat,
				'tgl_kembali' => $tgl_kembali,
				'durasi' => 1,
				'tgl_spt' => $tgl_spt,
				'id_kota_brgkt' => $kota_brgkt,
				'id_kota_tujuan' => $kota_tujuan,
				'id_alat_angkut' => $alat_angkut,
				'maksut_pd' => $mpd,
				'id_sumber_anggaran' => $pembebanan_anggaran,
				'id_pejabat_ttd_spt' => $pejabat_spt,
				'id_pejabat_ttd_sppd' => $pejabat_sppd,
				// 'id_pejabat_ttd_pptk' => $pejabat_pptk,
				'last_update' => date("Y-m-d H:i:s"),
				'author_update' 	=>	$this->session->userdata('username')
			);
		}

		$this->admin_model->update_data('spt','id_spt='.$id_spt, $isi_ubah_spt);


		$this->load->library('user_agent');

		$browser = $this->agent->browser();

		$browser_version = $this->agent->version();

		$os = $this->agent->platform();

		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'ubah data spt dg id '.$id_spt.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'ubah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->admin_model->tambah('log_user', $isi_data_log);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data SPT'));
		redirect(base_url('/spt/'), 'refresh');
	}

	public function cek_cetak_spt($id_surat){
		$data['cek_spt'] = $this->admin_model->get_duatable('*', 'tbl_spt', 'tbl_sppd', 'tbl_spt.id_spt=tbl_sppd.id_spt', 'tbl_spt.id_spt='.$id_surat, 'tbl_spt.id_spt');
		$save = false;
		foreach($data['cek_spt'] as $cek){
			if((!empty($cek->no_spt)) && (!empty($cek->tgl_spt)) && (!empty($cek->tgl_berangkat)) && (!empty($cek->tgl_kembali)) && (!empty($cek->durasi)) && (!empty($cek->id_kota_brgkt)) && (!empty($cek->id_kota_tujuan)) && (!empty($cek->id_alat_angkut)) && (!empty($cek->maksut_pd)) && (!empty($cek->id_sumber_anggaran)) && (!empty($cek->id_pejabat_ttd_spt)) && (!empty($cek->id_pejabat_ttd_sppd)) && (!empty($cek->id_pegawai))){
				$save = true;
			}
		}
		return $save;		
	}

	public function cek_detail_spt($id_surat){
		$data['cek_spt'] = $this->admin_model->get_tbl_where('spt', 'tbl_spt.id_spt='.$id_surat)->result();
		$save = false;
		foreach($data['cek_spt'] as $cek){
			if((!empty($cek->no_spt)) && (!empty($cek->tgl_spt)) && (!empty($cek->tgl_berangkat)) && (!empty($cek->tgl_kembali)) && (!empty($cek->durasi)) && (!empty($cek->id_kota_brgkt)) && (!empty($cek->id_kota_tujuan)) && (!empty($cek->id_alat_angkut)) && (!empty($cek->maksut_pd)) && (!empty($cek->id_sumber_anggaran)) && (!empty($cek->id_pejabat_ttd_spt)) && (!empty($cek->id_pejabat_ttd_sppd))){
				$save = true;
			}
		}
		return $save;		
	}
}
