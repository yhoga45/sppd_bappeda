<?php
class Login extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->helper('login');	
		$this->load->model('m_login');
		date_default_timezone_set('Asia/Jayapura');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = encrypt_url($this->input->post('password'));

		$where = array(
			'username' => $username,
			'password' => $password,
			'status_user' => 1
			);
		$cek = $this->m_login->cek_login("tbl_user", $where)->num_rows();
		if($cek > 0){

			// $ambil_data = $this->m_login->ambil("user_admin", $where)->result();
			// foreach ($ambil_data as $a) {
			// 	$role = $a->role;
			// }

			// $data_session = array(
			// 	'username' => $username,
			// 	'status' => "login",
			// 	'role' => $role
			// 	);
			$ambil_data 	= $this->m_login->ambil("tbl_user", $where)->row();
			$data_session  			= (array) $ambil_data; //convert jadi array
			if (isset($data_session['__ci_last_regenerate'])) {
				unset($data_session['__ci_last_regenerate']);
			}

			$data_session['status'] = "login";
			$this->session->set_userdata($data_session);

			if(function_exists('check_if_role_is')){
				// insert log activity
				$isi_data_log = array(
					'tgl_aktivitas' => date("Y-m-d H:i:s"),
					'keterangan' => 'login nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
					'jenis_aktivitas' => 'login',
					'username' => $this->session->userdata('username')
					);
				$this->m_login->insert_data('tbl_log_user', $isi_data_log);
				$role = $this->session->userdata('id_role');
				if (check_if_role_is($role)){

					// $this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Login..'));
					redirect(base_url("admin/"), 'refresh');

				} 
			}

		}else{
		// 	echo "<script> alert('data yang anda masukkan salah!!');
		// 	history.back();
		// </script>";
			$_SESSION["message"] = 'Username/Password yang Anda Masukkan Salah';
			redirect(base_url(""),'refresh');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url(''));
	}
}

