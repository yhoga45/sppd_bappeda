<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileUser extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('admin_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
	}


	public function profile()
	{
		$id_pengguna = decrypt_url($this->uri->segment(3));
		$data['profile'] = $this->admin_model->get_duatable('*','tbl_user', 'tbl_role', 'tbl_user.id_role=tbl_role.id', 'id_user='.$id_pengguna, 'tbl_user.id_user' );
		// var_dump($id_pengguna);
		// exit(0);
		$data['page'] = '';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/profile', $data);
		$this->load->view('template/footer');

	}

	public function ubah_profile()
	{
		$id_pengguna = decrypt_url($this->uri->segment(3));
		$data['profile'] = $this->admin_model->get_duatable('*','tbl_user', 'tbl_role', 'tbl_user.id_role=tbl_role.id', 'id_user='.$id_pengguna, 'tbl_user.id_user' );
		$data['role'] = $this->admin_model->get_tbl('role')->result();

		$data['page'] = '';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_profile', $data);
		$this->load->view('template/footer');

	}


	public function simpan_ubah_profile(){
		$id_pengguna = $this->input->post('id_user');
		$nip 		= $this->input->post('nip');
		$nama 		= $this->input->post('nama');
		$password  	= $this->input->post('password');


		$config['upload_path']          = FCPATH.'assets/img/profiluser/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

		$this->load->library('upload', $config);
		if ( $this->upload->do_upload('img_profil_admin'))
		{
			$gbr1 = $this->upload->data();
			$foto_profil = $gbr1['file_name'];
		}

		$isi_ubah_pengguna= array(
			'nama' 	 => $nama,
			'nip'	 => $nip,
			'nama'   => $nama
		);

		if (strlen($password) > 0) {
			$isi_ubah_pengguna['password'] =  encrypt_url($password);
		}

		// var_dump($foto_profil);
		// exit(0);
		if (isset($foto_profil)) {
			if (strlen($foto_profil) > 0) {
				$isi_ubah_pengguna['foto_profil'] = $foto_profil;
			}
		}

		// var_dump($id_pengguna);
		// exit(0);
		$this->admin_model->update_data('user', 'id_user='.$id_pengguna, $isi_ubah_pengguna);

		$this->load->model('m_login');
		$where = array(
			'id_user' => $id_pengguna
			);
		$ambil_data 	= $this->m_login->ambil("tbl_user", $where)->row();
		$data_session  			= (array) $ambil_data; //convert jadi array
		if (isset($data_session['__ci_last_regenerate'])) {
			unset($data_session['__ci_last_regenerate']);
		}

		$data_session['status'] = "login";
		$this->session->set_userdata($data_session);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Pengguna'));
		redirect('/profileuser/profile/'.encrypt_url($id_pengguna),'refresh');
	}

}