<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('login');	
		$this->load->model('model_pengguna');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		date_default_timezone_set('Asia/Jayapura');
		if($this->session->userdata('status') != "login"){
			echo "<script>alert('Maaf, anda belum login / sesi anda sudah habis. Silahkan untuk login lagi!')</script>";
			redirect(base_url(""));
		}
		if(function_exists('check_if_role_is')){
			if (!check_if_role_is('1') && !check_if_role_is('2')){
				echo "<script>alert('Maaf, anda tidak izinkan membuka fitur ini!')</script>";
				redirect(base_url("spt/"));
			}
		}
	}


	public function index()
	{
		$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_user.id_role=3', 'tbl_instansi.id_instansi');

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna', $data);
		$this->load->view('template/footer');

	}
	
	public function disable_user_kadis(){

		$id_user = decrypt_url($this->uri->segment(3));
		$ubah_status_pengguna= array(
			'status_user' => 0
		);
		$this->model_pengguna->update_data('user', 'id_user='.$id_user, $ubah_status_pengguna);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menon-aktikan Pengguna'));
		redirect('/pengguna','refresh');
	}

	public function activate_user_kadis(){

		$id_user = decrypt_url($this->uri->segment(3));
		$ubah_status_pengguna= array(
			'status_user' => 1
		);
		$this->model_pengguna->update_data('user', 'id_user='.$id_user, $ubah_status_pengguna);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengaktikan Pengguna'));
		redirect('/pengguna','refresh');
	}

	public function tambah_pengguna()
	{
		$data['role'] = $this->model_pengguna->get_duatable('*','tbl_role', 'tbl_instansi', 'tbl_role.id_instansi=tbl_instansi.id_instansi', 'tbl_role.id=3', 'tbl_role.id');
		$data['instansi'] = $this->model_pengguna->get_tbl('instansi')->result();

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/tambah_data_pengguna', $data);
		$this->load->view('template/footer');

	}

	public function ubah_pengguna()
	{
		$id_pengguna = decrypt_url($this->uri->segment(3));
		$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_user.id_user='.$id_pengguna, 'tbl_instansi.id_instansi');
		// $data['user'] = $this->model_pengguna->get_duatable('*','tbl_user', 'tbl_role', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_user='.$id_pengguna, 'tbl_user.id_user' );
		$data['role'] = $this->model_pengguna->get_tbl_where('role', 'id=3')->result();

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/ubah_data_pengguna', $data);
		$this->load->view('template/footer');

	}

	public function simpan_data_pengguna(){
		$role = $this->input->post('role');
		$nip 		  = $this->input->post('nip');
		$username  = $this->input->post('username');
		$nama = $this->input->post('nama');
		$password  = encrypt_url($this->input->post('password'));

		// $data['cek_username'] = $this->model_pengguna->get_tbl('user')->result();
		// $count_kesamaan_username = 0;
		// foreach($data['cek_username'] as $cu){
		// 	$cek_username = $cu->username;
		// 	if($username == $cek_username){
		// 		$count_kesamaan_username ++;
		// 	}
		// }
		// if($count_kesamaan_username == 0) {
			$isi_data_pengguna= array(
				'nama' => $nama,
				'nip'		   => $nip,
				'username'  => $username,
				'nama'   => $nama,
				'password'  => $password,
				'id_role'  => $role,
				'parent_id_user' => $parent_id_user,
				'parent_id_sub_user' => NULL,
				'author_insert'	=> $this->session->userdata('username'),
				'insert_date' => date("Y-m-d H:i:s")
			);
			$this->model_pengguna->tambah('user', $isi_data_pengguna);

			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data user oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			
			$this->model_pengguna->tambah('log_user', $isi_data_log);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Pengguna'));
		redirect('/pengguna/','refresh');
	}

	public function simpan_ubah_pengguna(){
		$id_pengguna = decrypt_url($this->uri->segment(3));
		$id_role = decrypt_url($this->uri->segment(4));
		$nip  = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$ket_jabatan = $this->input->post('keterangan_jabatan');
		$password  =$this->input->post('password');
		// var_dump($id_role);
		// exit(0);
		$isi_ubah_pengguna= array(
			'nama' => $nama,
			'nip'  => $nip,
			'keterangan_jabatan' => $ket_jabatan,
			'last_update' 		=> date("Y-m-d H:i:s"),
			'author_update'		=> $this->session->userdata('username')
		);
		if (strlen($password) > 0) {
			$isi_ubah_pengguna['password'] =  encrypt_url($password);
		}
		$this->model_pengguna->update_data('user', 'id_user='.$id_pengguna, $isi_ubah_pengguna);

		$this->load->library('user_agent');
		$browser = $this->agent->browser();
		$browser_version = $this->agent->version();
		$os = $this->agent->platform();
		$ip_address = $this->input->ip_address();

		// insert log activity
		$isi_data_log = array(
			'tgl_aktivitas' => date("Y-m-d H:i:s"),
			'keterangan' => 'ubah data pengguna dg id '.$id_pengguna.' oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
			'jenis_aktivitas' => 'ubah data',
			'username' => $this->session->userdata('username'),
			'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
		);
		$this->model_pengguna->tambah('log_user', $isi_data_log);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengubah Data Pengguna'));
		if($id_role == 3) {
			redirect('/pengguna/','refresh');
		} elseif($id_role == 4 || $id_role == 5 || $id_role == 6){
			$parent_id_user = $this->uri->segment(5);
			redirect('/pengguna/kabid/'.$parent_id_user,'refresh');
		} elseif($id_role == 7 || $id_role == 8){
			$parent_id_user = $this->uri->segment(5);
			$parent_id_sub_user = $this->uri->segment(6);
			// var_dump($parent_id_user);
			// exit(0);
			redirect('/pengguna/kasubid/'.$parent_id_user.'/'.$parent_id_sub_user,'refresh');
		}
	}

	public function kabid(){
		$id_instansi = $this->session->userdata('id_instansi');
		$id_user = decrypt_url($this->uri->segment(3));
		$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'id_role=6 OR id_role=5 OR id_role=4 AND parent_id_user="'.$id_user.'" AND tbl_user.id_instansi='.$id_instansi, ' tbl_instansi.id_instansi');
		// $data['user'] = $this->model_pengguna->get_tbl_where('user', 'id_role=5 OR id_role=4 AND parent_id_user='.$id_user.' AND id_instansi='.$id_instansi)->result();

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna_kabid/pengguna_kabid', $data);
		$this->load->view('template/footer');
	}

	public function tambah_kabid()
	{
		$data['role'] = $this->model_pengguna->get_tbl_where('role', 'id=4 OR id=5 OR id=6')->result();
		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna_kabid/tambah_kabid', $data);
		$this->load->view('template/footer');
	}

	public function simpan_data_kabid(){
		$role 		= $this->input->post('role');
		$nip 		= $this->input->post('nip');
		$username  	= $this->input->post('username');
		$nama		= $this->input->post('nama');
		$password 	= encrypt_url($this->input->post('password'));
		$parent_id_user = decrypt_url($this->uri->segment(3));
		$ket_jabatan = $this->input->post('keterangan_jabatan');

		$cun = $this->model_pengguna->get_tbl_where('user', 'tbl_user.username="'.$username.'"')->result();
		if(empty($cun)){
			$isi_data_pengguna= array(
				'nama' 		=> $nama,
				'nip'		=> $nip,
				'username'  => $username,
				'nama'   	=> $nama,
				'password'  => $password,
				'id_role'  	=> $role,
				'parent_id_user' => $parent_id_user,
				'keterangan_jabatan' => $ket_jabatan,
				'status_user' => 0,
				'id_instansi' => $this->session->userdata('id_instansi'),
				'parent_id_sub_user' => NULL,
				'author_insert'	=> $this->session->userdata('username'),
				'insert_date' => date("Y-m-d H:i:s")
			);
			$this->model_pengguna->tambah('user', $isi_data_pengguna);
	
			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();
			
	
			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data user oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			
			$this->model_pengguna->tambah('log_user', $isi_data_log);
	
			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Pengguna'));
			redirect('/pengguna/kabid/'.encrypt_url($parent_id_user),'refresh');
		} else {
			$this->session->set_flashdata('message1', array('type'=>'error','text'=>'Username tidak boleh sama!'));
			redirect('/pengguna/kabid/'.encrypt_url($parent_id_user),'refresh');
		}
	}

	public function ubah_kabid()
	{
		$id_pengguna = decrypt_url($this->uri->segment(3));
		$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'tbl_user.id_user='.$id_pengguna, 'tbl_instansi.id_instansi');
		// $data['user'] = $this->model_pengguna->get_duatable('*','tbl_user', 'tbl_role', 'tbl_user.id_role=tbl_role.id', 'tbl_user.id_user='.$id_pengguna, 'tbl_user.id_user' );
		$data['role'] = $this->model_pengguna->get_tbl_where('role',  'id=4 OR id=5 OR id=6')->result();

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna_kabid/ubah_kabid', $data);
		$this->load->view('template/footer');

	}

	public function disable_user_kabid(){

		$id_parent_user = decrypt_url($this->uri->segment(3));
		$id_parent_sub_user = decrypt_url($this->uri->segment(4));
		$ubah_status_pengguna= array(
			'status_user' => 0
		);
		$this->model_pengguna->update_data('user', 'id_user='.$id_parent_sub_user, $ubah_status_pengguna);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menon-aktikan Pengguna'));
		redirect('/pengguna/kabid/'. encrypt_url($id_parent_user),'refresh');
	}

	public function activate_user_kabid(){

		$id_parent_user = decrypt_url($this->uri->segment(3));
		$id_parent_sub_user = decrypt_url($this->uri->segment(4));
		$ubah_status_pengguna= array(
			'status_user' => 1
		);
		$this->model_pengguna->update_data('user', 'id_user='.$id_parent_sub_user, $ubah_status_pengguna);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengaktikan Pengguna'));
		redirect('/pengguna/kabid/'. encrypt_url($id_parent_user),'refresh');
	}

	public function kasubid(){
		$id_instansi = $this->session->userdata('id_instansi');
		$id_user = decrypt_url($this->uri->segment(4));

		$data['cek_role'] = $this->model_pengguna->get_tbl_where('user', 'tbl_user.id_user='.$id_user)->result();
		foreach($data['cek_role'] as $cr){
			$id_role = $cr->id_role;
		}
		if($id_role == 5){
			$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'id_role=7 AND parent_id_sub_user='.$id_user.' AND tbl_user.id_instansi='.$id_instansi, ' tbl_instansi.id_instansi');
		} elseif($id_role == 4 || $id_role == 6){
			$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'id_role=8 AND parent_id_sub_user='.$id_user.' AND tbl_user.id_instansi='.$id_instansi, ' tbl_instansi.id_instansi');
		}
		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna_kasubid/pengguna_kasubid', $data);
		$this->load->view('template/footer');
	}

	public function tambah_kasubid()
	{
		$id_user_kabid = decrypt_url($this->uri->segment(4));
		$kabid = $this->model_pengguna->get_tbl_where('user', 'id_user='.$id_user_kabid)->result()[0];

		if($kabid->id_role == 4 || $kabid->id_role == 6){		
			$data['role'] = $this->model_pengguna->get_tbl_where('role', 'id=8')->result();
		} else {
			print_r("Kabid");
			$data['role'] = $this->model_pengguna->get_tbl_where('role', 'id=7')->result();
		}

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna_kasubid/tambah_kasubid', $data);
		$this->load->view('template/footer');
	}

	public function simpan_data_kasubid(){
		$role 		= $this->input->post('role');
		$nip 		= $this->input->post('nip');
		$username  	= $this->input->post('username');
		$nama		= $this->input->post('nama');
		$password 	= encrypt_url($this->input->post('password'));
		$parent_id_user = decrypt_url($this->uri->segment(3));
		$parent_id_sub_user = decrypt_url($this->uri->segment(4));
		// var_dump($parent_id_sub_user);
		// exit(0);
		$ket_jabatan = $this->input->post('keterangan_jabatan');

		$cun = $this->model_pengguna->get_tbl_where('user', 'tbl_user.username="'.$username.'"')->result();
		if(empty($cun)){
			$isi_data_pengguna= array(
				'nama' 		=> $nama,
				'nip'		=> $nip,
				'username'  => $username,
				'nama'   	=> $nama,
				'password'  => $password,
				'id_role'  	=> $role,
				'parent_id_user' => $parent_id_user,
				'parent_id_sub_user' => $parent_id_sub_user,
				'keterangan_jabatan' => $ket_jabatan,
				'status_user' => 0,
				'id_instansi' => $this->session->userdata('id_instansi'),
				'author_insert'	=> $this->session->userdata('username'),
				'insert_date' => date("Y-m-d H:i:s")
			);
			$this->model_pengguna->tambah('user', $isi_data_pengguna);

			$this->load->library('user_agent');
			$browser = $this->agent->browser();
			$browser_version = $this->agent->version();
			$os = $this->agent->platform();
			$ip_address = $this->input->ip_address();

			// insert log activity
			$isi_data_log = array(
				'tgl_aktivitas' => date("Y-m-d H:i:s"),
				'keterangan' => 'tambah data user oleh user dengan nama asli '.$this->session->userdata('nama'). ' dan kode role '.$this->session->userdata('id_role'),
				'jenis_aktivitas' => 'tambah data',
				'username' => $this->session->userdata('username'),
				'perangkat' =>  $browser.'-'.$browser_version.', '.$os.', '.$ip_address
			);
			
			$this->model_pengguna->tambah('log_user', $isi_data_log);

			$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menambah Data Pengguna'));
			redirect('/pengguna/kasubid/'.encrypt_url($parent_id_user).'/'.encrypt_url($parent_id_sub_user),'refresh');
		} else {
			$this->session->set_flashdata('message1', array('type'=>'error','text'=>'Username tidak boleh sama!'));
			redirect('/pengguna/kasubid/'.encrypt_url($parent_id_user).'/'.encrypt_url($parent_id_sub_user),'refresh');
		}
	}

	public function ubah_kasubid()
	{
		$id_instansi = $this->session->userdata('id_instansi');
		$id_user = decrypt_url($this->uri->segment(3));
		$data['user'] = $this->model_pengguna->get_tigatable('*','tbl_user', 'tbl_role', 'tbl_instansi', 'tbl_user.id_role = tbl_role.id', 'tbl_user.id_instansi=tbl_instansi.id_instansi', 'id_user="'.$id_user.'" AND tbl_user.id_instansi='.$id_instansi, ' tbl_instansi.id_instansi');
		// $data['user'] = $this->model_pengguna->get_tbl_where('user', 'id_role=5 OR id_role=4 AND parent_id_user='.$id_user.' AND id_instansi='.$id_instansi)->result();

		$data['role'] = $this->model_pengguna->get_tbl_where('role', 'id=7 OR id=8')->result();

		$data['page'] = 'data_pengguna';

		$this->load->view('template/header');
		$this->load->view('template/sidebar', $data);
		$this->load->view('admin/data_pengguna_kasubid/ubah_kasubid', $data);
		$this->load->view('template/footer');

	}

	public function disable_user_kasubid(){

		$parent_id_user = decrypt_url($this->uri->segment(3));
		$parent_id_sub_user = decrypt_url($this->uri->segment(4));
		$id_user = decrypt_url($this->uri->segment(5));
		// var_dump($id_parent_user);
		// exit(0);
		$ubah_status_pengguna= array(
			'status_user' => 0
		);
		$this->model_pengguna->update_data('user', 'id_user='.$id_user, $ubah_status_pengguna);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Menon-aktikan Pengguna'));
		redirect('/pengguna/kasubid/'.encrypt_url($parent_id_user).'/'.encrypt_url($parent_id_sub_user),'refresh');
	}

	public function activate_user_kasubid(){

		$parent_id_user = decrypt_url($this->uri->segment(3));
		$parent_id_sub_user = decrypt_url($this->uri->segment(4));
		$id_user = decrypt_url($this->uri->segment(5));
		$ubah_status_pengguna= array(
			'status_user' => 1
		);
		$this->model_pengguna->update_data('user', 'id_user='.$id_user, $ubah_status_pengguna);

		$this->session->set_flashdata('message', array('type'=>'success','text'=>'Berhasil Mengaktikan Pengguna'));
		redirect('/pengguna/kasubid/'.encrypt_url($parent_id_user).'/'.encrypt_url($parent_id_sub_user),'refresh');
	}

}