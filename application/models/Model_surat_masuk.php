<?php 
    class model_surat_masuk extends CI_Model{	

        function __construct() {
            parent::__construct();
        }

        function get($table, $kolom, $order)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->order_by($kolom, $order);
            $query = $this->db->get();
            return $query->result();
        }

        function get_where($table, $where)
        {
            return $this->db->get_where($table, $where);
             
        }

        function get_order_by($table, $where, $column, $order)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where);
            $this->db->order_by($column, $order);
            $query = $this->db->get();
            return $query->result();
             
        }

        public function get_duatable($select, $table1, $table2, $join, $where, $order)
        {
            $this->db->select($select);
            $this->db->from($table1);
            $this->db->join($table2, $join);
            $this->db->where($where);
            $this->db->order_by($order, "asc");
            $query = $this->db->get();
            return $query->result();
        }

        function get_tigatable($select, $table1, $table2, $table3, $join1, $join2, $where, $order){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->where($where);
            $this->db->order_by($order, "asc");
            $query = $this->db->get();
            return $query->result();
        }

        function get_distinct_tigatable($distinct, $select, $table1, $table2, $table3, $join1, $join2, $where, $order){
            $this->db->select("DISTINCT $distinct, $select", FALSE);
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->where($where);
            $this->db->order_by($order, "asc");
            $query = $this->db->get();
            return $query->result();
        }

        public function get_surat_masuk($select, $table1, $table2, $table3, $join1, $join2, $where, $order){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->where($where);
            $this->db->order_by($order, "desc");
            $query = $this->db->get();
            return $query->result();
        }
    
        function get_empattable($select, $table1, $table2, $table3, $table4, $join1, $join2, $join3, $where, $order){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->join($table4, $join3);
            $this->db->where($where);
            $this->db->order_by($order, "asc");
            $query = $this->db->get();
            return $query->result();
        }

        public function get_limatable($select, $table1, $table2, $table3, $table4, $table5, $join1, $join2, $join3, $join4, $where){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->join($table4, $join3);
            $this->db->join($table5, $join4);
            $this->db->where($where);
            $query = $this->db->get();
            return $query->result();
        }

        public function get_limatable_order_by($select, $table1, $table2, $table3, $table4, $table5, $join1, $join2, $join3, $join4, $where, $order){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->join($table4, $join3);
            $this->db->join($table5, $join4);
            $this->db->where($where);
            $this->db->order_by($order, "Desc");
            $query = $this->db->get();
            return $query->result();
        }

        public function get_enamtable($select, $table1, $table2, $table3, $table4, $table5, $table6, $join1, $join2, $join3, $join4, $join5, $where){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->join($table4, $join3);
            $this->db->join($table5, $join4);
            $this->db->join($table6, $join5);
            $this->db->where($where);
            $query = $this->db->get();
            return $query->result();
        }

        public function get_delapantable($select, $table1, $table2, $table3, $table4, $table5, $table6, $table7, $table8, $join1, $join2, $join3, $join4, $join5, $join6, $join7, $where, $column, $order){
            $this->db->select($select);    
            $this->db->from($table1);
            $this->db->join($table2, $join1);
            $this->db->join($table3, $join2);
            $this->db->join($table4, $join3);
            $this->db->join($table5, $join4);
            $this->db->join($table6, $join5);
            $this->db->join($table7, $join6);
            $this->db->join($table8, $join7);
            $this->db->where($where);
            $this->db->order_by($column, $order);
            $query = $this->db->get();
            return $query->result();
        }

        function insert($table, $object){
            $this->db->insert($table, $object);
        }

        function update($table, $where, $object){
            $this->db->where($where);
            $this->db->update($table, $object);
        }

        function delete($table, $where){
            $this->db->where($where);
            $this->db->delete($table);
        }
        
    }
